import _ from 'lodash';
import google from 'googleapis';
import googleAuth from 'google-auth-library';
import fs from 'fs';
import logger from './logger';
import config from './config/enviroment';

export function sendMail(message, callback) {
  logger.log('debug', 'Authorize a client with the loaded credentials, then call the Gmail API.');
  const credentials = config.google;
  GmailAuthorize(credentials, ($err, auth) => {
    if ($err) {
      logger.log('debug',$err);
      return callback($err);
    }
    const gmail = google.gmail('v1');
    const raw = makeBody(message.to, message.from, message.subject, message.body, message.files);

    logger.log('debug', 'send email with Gmail API.');
    gmail.users.messages.send({
      auth: auth,
      userId: 'me',
      resource: { raw }
    }, (error) => callback(error, raw));
  });
}

function GmailAuthorize(credentials, callback) {
  const auth = new googleAuth();
  const oauth2Client = new auth.OAuth2(credentials.clientID, credentials.clientSecret, credentials.callbackURL);
  let tokens = {
    access_token: credentials.access_token,
    refresh_token: credentials.refresh_token,
    token_type: credentials.token_type,
    expiry_date: credentials.expiry_date
  }
  oauth2Client.credentials = tokens;
  oauth2Client.getRequestMetadata(null, (error, headers, response) => {
    if (error) {
      callback(error);
    } else {
      if (credentials.access_token !== oauth2Client.credentials.access_token){
        storeToken(oauth2Client.credentials);
      }
      callback(null, oauth2Client);
    }
  });
}

// store new access_token to file for using future
function storeToken(tokens) {
  const TOKEN_DIR = __dirname + '/../.credentials/';
  const env = process.env.NODE_ENV || '';
  const TOKEN_PATH = TOKEN_DIR + `client_secret${_.isEmpty(env) ? '' : ('.' + env)}.google.token`;
  try {
    config.google.access_token = tokens.access_token;
    config.google.expiry_date = tokens.expiry_date;
    fs.mkdirSync(TOKEN_DIR);
  } catch (err) {
    if (err.code != 'EXIST') {
      throw err;
    }
  }
  fs.writeFile(TOKEN_PATH, JSON.stringify(tokens));
}

function makeBody(to, from, subject, message, attachments) {
  const encode = (subject) => {
    var enc_subject = new Buffer(subject).toString("base64");
    return '=?utf-8?B?' + enc_subject + '?=';
  };
  const nl = "\n";
  let str = [
    'MIME-Version: 1.0',
    'Content-Type: text/html; charset="UTF-8"',
    'Content-Transfer-Encoding: 7bit',
    'to: ' + to,
    'from: ' + from,
    'subject: ' + subject + nl,
    message
  ].join(nl);

  if (!_.isEmpty(attachments)) {
    const boundary = "IndiaOnlineEVisaApp";
    const $attachments = [];
    _.each(attachments, (attachment) => {
      const $attachment = ([
        '--' + boundary,
        'Content-Type: ' + attachment.mimeType + 'name="' + attachment.fileName + '"',
        'Content-Disposition: attachment; filename="' + attachment.fileName + '"',
        'Content-Transfer-Encoding: base64' + nl,
        attachment.bytes.toString("base64")
      ]).join(nl);
      $attachments.push($attachment);
    });
    str = ([
      'MIME-Version: 1.0',
      'to: ' + to,
      'from: ' + from,
      'subject: ' + subject,
      `Content-Type: multipart/mixed; boundary="${boundary}"${nl}`,
      '--' + boundary,
      `Content-Type: multipart/alternative; boundary="html"${nl}`,

      '--html',
      'Content-Type: text/plain; charset=UTF-8',
      'Content-Transfer-Encoding: base64' + nl,
      new Buffer('').toString("base64") + nl,

      '--html',
      'Content-Type: text/html; charset=UTF-8',
      'Content-Transfer-Encoding: base64' + nl,
      new Buffer(message).toString("base64") + nl,

      "--html--",

      $attachments.join(nl),
      "--" + boundary + "--"
    ]).join(nl);
  }
  return new Buffer(str).toString("base64").replace(/\+/g, '-').replace(/\//g, '_').replace(/\=/g, '');
}
