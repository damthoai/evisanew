import fs from 'fs';
import pdf from 'html-pdf';
const html = fs.readFileSync('./Invoiced_UnpaidCopy.html', 'utf8');
const options = {
  "height": "11.69in",
  "width": "8.27in",
  "format": "A4",
  "orientation": "portrait",
  "header": {
    "height": "45mm"
  },
  "footer": {
    "height": "45mm"
  }
};

pdf.create(html, options).toFile("./a.pdf",($err, $sucess) => {
  if ($err) {
    return $err ;
  } else {
    return $sucess;
  }
});