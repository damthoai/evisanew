import _ from 'lodash';

let config = {
  appName: 'IndiaOnlineVisa-API',
  namespace: '/api/v1',
  db: {
    mysql: {
      host: 'localhost',
      user: 'root',
      password: 'tinhviet321#',
      database: 'indiaonlineevisa'
    }
  },
  domain: 'http://localhost:4200',
  ip: process.env.IP || '127.0.0.1',
  port: process.env.PORT || '3000',
  secrets: {
    session: 'secret',
    sessionExpiresIn: 60 * 60 * 5
  },
  facebook: {
    clientID: '468858830115945',
    clientSecret: '7b454c877ed88935b55a3e9ea75f1555',
    callbackURL: 'http://localhost/api/v1/auth/facebook/callback'
  },
  google: {
    projectId: 'onlineindiaevisa',
    clientID: '880133518781-bdlm6tkojes34t1s17q1t6fv01hitdu7.apps.googleusercontent.com',
    clientSecret: 'SBBD2jYxnaQYIKxfSpMpcrf5',
    callbackURL: 'http://localhost:4200/api/v1/auth/google/callback',
    access_token: "ya29.GlvwBPD7hDYXgb3yOj8eLOjuxX1iR5Dq_9x5olhAsrFnOr32_riBBGWnzhfOI5Cd6riMEmjyBUz4y5yzXJeEDEEDKRr0vJcAcZy8oX5PWPUfy_e4Gy2P699C_duX",
    refresh_token: "1/m3zNqBxxLQIHf1rgC0fx4YAr9nINS2xdZQM_X1BjH2o",
    token_type: "Bearer",
    expiry_date: 1509002226158
  },
  payment: {
    active: 'authorizeNet',
    eWay: {
      API_KEY: '44DD7C6Tu5SRFRwZCOB/7QCNRUnbLMVFFIO4yaQnJ491OLUCxi1H+Zi80bBwMqFIEEjehy',
      Password: 'rVapIYU4',
      END_POINT: 'https://api.sandbox.ewaypayments.com/Transaction'
    },
    authorizeNet: {
      API_LOGIN_ID: "3sEN68be",
      TRANSACTION_KEY: "9MX2k6s7S3rw555S",
      SECRET_KEY: "Simon",
      END_POINT: 'https://apitest.authorize.net/xml/v1/request.api'
    }
  },
  emailFrom: 'Evisa4u Pty Ltd trading as onlineindiaevisa.com <visa@onlineindiaevisa.com>',
  infofile: './logs/info-file.log',
  errorfile: './logs/error-file.log',
  timeToResend: 1000
};
if (process.env.NODE_ENV) {
  config = _.merge(config, require(`./${process.env.NODE_ENV}`));
}
export default config;