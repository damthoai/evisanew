const mysql = {
  host: 'localhost',
  user: 'indiaonlineevisa',
  password: 'tinhviet321#',
  database: 'indiaonlineevisa'
};
module.exports = {
	appName: 'IndiaOnlineVisa-API',
  namespace: '/api/v1',
  domain: 'https://onlineindiaevisa.com',
  db: { mysql },
  ip: process.env.IP || '127.0.0.1',
  port: process.env.PORT || '3000',
  payment: {
    eWay: {
      API_KEY: '44DD7C6Tu5SRFRwZCOB/7QCNRUnbLMVFFIO4yaQnJ491OLUCxi1H+Zi80bBwMqFIEEjehy',
      Password: 'aNMQybmY',
      END_POINT: 'https://api.ewaypayments.com/Transaction'
    },
    authorizeNet: {
      API_LOGIN_ID: "9u2z5UHPbXMh",
      TRANSACTION_KEY: "66ryKk8n5c8E7LuV",
      END_POINT: 'https://api.authorize.net/xml/v1/request.api'
    }
  },
  facebook: {
    clientID: '468858830115945',
    clientSecret: '7b454c877ed88935b55a3e9ea75f1555',
    callbackURL: 'http://onlineindiaevisa.com/api/v1/auth/facebook/callback'
  },
  google: {
    projectId: 'onlineindiaevisa',
    clientID: '880133518781-dvnoond6ru5qofe123c8lkaqpd331v9c.apps.googleusercontent.com',
    clientSecret: 'FIG3bvPUPSgQkrBcniwBpwPy',
    callbackURL: 'https://onlineindiaevisa.com/api/v1/auth/google/callback',
    access_token: "",
    refresh_token: "",
    token_type: "Bearer",
    expiry_date: 1507519895267
  },
  emailFrom: 'Evisa4u Pty Ltd trading as onlineindiaevisa.com <visa@onlineindiaevisa.com>',
  logfile: '/root/indiaonlineevisa/IndiaOnlineVisa-BE/logs/all-logs.log',
  timeToResend: 15*60*1000
}