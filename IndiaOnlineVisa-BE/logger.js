import winston from 'winston';
import config from './config/enviroment';

let transports = [
  new (winston.transports.Console)({
    level: 'debug'
  })
];
if (process.env.NODE_ENV === 'production') {
  transports = [
    new (winston.transports.File)({
      name: 'all-log',
      filename: config.logfile,
      level: 'debug'
    })
  ];
}
const logger = new (winston.Logger)({
  transports
});
export default logger;