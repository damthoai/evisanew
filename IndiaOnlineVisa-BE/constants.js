export const GOVERMENT_FEE_25 = ("Japan, Singapore, Sri Lanka").split(", ").map(nationality => ({ key: nationality, value: nationality, fee: 25 }));
export const GOVERMENT_FEE_50 = ("Albania, Andorra, Angola, Anguilla, Antigua & Barbuda, Armenia, Australia, Austria, Azerbaijan, Bahamas, Barbados, Belgium, Belize, Bolivia, Bosnia & Herzegovina, Botswana, Brazil, Brunei, Bulgaria, Burundi, Cambodia, Cameroon, Canada, Cape Verde, Cayman Island, Chile, China, China- Sar Hongkong, China - Macau, Colombia, Comoros, Costa Rica, Croatia, Cuba, Cyprus, Czech Republic, Denmark, Djibouti, Dominica, Dominican Republic, East Timor, Ecuador, El Salvador, Eritrea, Estonia, Finland, France, Gabon, Gambia, Georgia, Germany, Ghana, Greece, Grenada, Guatemala, Guinea, Guyana, Haiti, Vatican City - Holy See, Honduras, Hungary, Iceland, Indonesia, Ireland, Israel, Italy, Cote D'Ivoire, Jordan, Kenya, Republic Of Korea, Laos, Latvia, Lesotho, Liberia, Liechtenstein, Lithuania, Luxembourg, Madagascar, Malawi, Malaysia, Mali,Malta, Mexico, Moldova, Monaco, Mongolia, Montenegro, Montserrat, Myanmar, Namibia, Netherlands, New Zealand, Nicaragua, Niger Republic, Norway, Oman, Palestine, Panama, Paraguay, Peru, Philippines, Poland, Portugal, Macedonia, Romania, Rwanda, Saint Lucia, Saint Vincent And The Grenadines, San Marino, Senegal, Serbia, Sierra Leone, Slovakia, Slovenia, Spain, Saint Christopher And Nevis, Suriname, Swaziland, Sweden, Switzerland, Taiwan, Tajikistan, Tanzania, Thailand, Trinidad And Tobago , Turks And Caicos Isl, United Arab Emirates, Uzbekistan, Venezuela, Vietnam, Zambia, Zimbabwe").split(", ").map(nationality => ({ key: nationality, value: nationality, fee: 50 }));
export const GOVERMENT_FEE_75 = ("Mozambique, Russia, Ukraine, United Kingdom, USA").split(", ").map(nationality => ({ key: nationality, value: nationality, fee: 75 }));
export const GOVERMENT_FEES = [...GOVERMENT_FEE_25, ...GOVERMENT_FEE_50, ...GOVERMENT_FEE_75];

export const ORDER_STATUS = {
  PENDING: "PENDING",
  PAID: "PAID",
  CANCELED_PAYMENT: "CANCELED_PAYMENT",
  PROCESSING: "PROCESSING",
  FINISH: "FINISH"
};

export const PAYMENT_STATUS = {
  PENDING: "PENDING",
  PAID: "PAID",
  CANCELED_PAYMENT: "CANCELED_PAYMENT"
};

export const STEP_STATUS = {
  INFO: 1,
  DETAIL_APPLICANT: 2,
  PAYMENT: 3
}
export const GENDER = {
  NONE: '',
  MALE: 'MALE',
  FEMALE: 'FEMALE'
}
export const TYPE_OF_CARDS = {
  eTourist: {
    key: '241',
    value: 'Indian e-Tourist visa - 60 days double entries'
  },
  eBusiness: {
    key: '243',
    value: 'Indian e-Business visa - 60 days double entries'
  },
  eMedical: {
    key: '244',
    value: 'Indian e-Medical visa - 60 days triple entries'
  }
};
export const PROCESS_TYPES = {
  NORMAL: {
    key: '1',
    value: 'Normal (Guaranteed within 3 working days)',
    fee: 0
  },
  URGENT: {
    key: '2',
    value: 'Urgent (Guaranteed within 24 hours)',
    fee: 79
  },
  SUPERURGENT: {
    key: '3',
    value: 'Super Urgent (Within 5 business hours)',
    fee: 99
  }
};
export const ARRIVAL_PORT_TYPE = {
  Airport: {
    key: '1',
    portOfArrivals: [
      { key: '1', value: 'Chennai - MAA - Chennai - Tamil Nadu' },
      { key: '2', value: 'Indira Gandhi - DEL - Delhi - Delhi' },
      { key: '3', value: 'Netaji Subhash Chandra Bose - CCU - Kolkata - West Bengal' },
      { key: '4', value: 'Chhatrapati Shivaji - BOM - Mumbai - Maharashtra' },
      { key: '5', value: 'Cochin – COK - Kochi - Kerala' },
      { key: '6', value: 'Rajiv Gandhi – HYD - Hyderabad - Telangana' },
      { key: '7', value: 'Kempegowda – BLR - Bangalore - Karnataka' },
      { key: '8', value: 'Goa – GOI – Dabolim – Goa' },
      { key: '9', value: 'Thiruvananthapuram – TRV - Thiruvananthapuram - Kerala' },
      { key: '10', value: 'Sardar Patel - AMD - Ahmedabad - Gujarat' },
      { key: '11', value: 'Sri Guru Ram Dass Jee - ATQ - Amritsar - Punjab' },
      { key: '12', value: 'Gaya - GAY - Gaya - Bihar' },
      { key: '13', value: 'Jaipur - JAI - Jaipur - Rajasthan' },
      { key: '14', value: 'Chaudhary Charan Singh - LKO - Lucknow - Uttar Pradesh' },
      { key: '15', value: 'Varanasi - VNS - Varanasi - Uttar Pradesh' },
      { key: '16', value: 'Tiruchirappalli - TRZ - Tiruchirappalli - Tamil Nadu' },
      { key: '17', value: 'Bagdogra - IXB - Bagdogra - West Bengal' },
      { key: '18', value: 'Calicut - CCJ - Kozhikode  - Kerala' },
      { key: '19', value: 'Chandigarh- IXC - Chandigarh - Chandigarh' },
      { key: '20', value: 'Coimbatore - CJB - Coimbatore - Tamil Nadu' },
      { key: '21', value: 'Lokpriya Gopinath Bordoloi  - GAU - Guwahati - Assam' },
      { key: '22', value: 'Mangalore - IXE - Mangalore  - Karnataka' },
      { key: '23', value: 'Dr. Babasaheb Ambedkar - NAG - Nagpur - Maharashtra' },
      { key: '24', value: 'Pune - PNQ - Pune -Maharashtra' }
    ]
  },
  Sea_Port: {
    key: '2',
    portOfArrivals: [
      { key: '25', value: 'Cochin Seaport'},
      { key: '26', value: 'Mangalore Seaport'},
      { key: '27', value: 'Goa Seaport'}
    ]
  }
};
export const PORT_ARRIVAL = ARRIVAL_PORT_TYPE.Airport.portOfArrivals.concat( ARRIVAL_PORT_TYPE.Sea_Port.portOfArrivals);


export const RELIOUS = [
  'Bahai', 'Buddhism', 'Chirstian', 'Hindu', 'Islam', 'Jainism', 'Judaism', 'Parsi', 'Sikh', 'Zoroastrian', 'Other'
];
export const EDUCATIONS = [
  'High school', 'Graduated - bachelor', 'Post graduated', 'Below matriculation', 'Others'
];

export const KEYS = {
  SOCIAL: 'SOCIAL',
  ORDER_STATUS: 'ORDER_STATUS',
  GENDER: 'GENDER',
  TYPE_OF_CARDS: 'TYPE_OF_CARDS',
  ARRIVAL_PORT_TYPE: 'ARRIVAL_PORT_TYPE',
  PORT_OF_AIRPORT: 'PORT_OF_AIRPORT',
  PORT_OF_SEAPORT: 'PORT_OF_SEAPORT',
  GOVERNMENT_TYPE: 'GOVERNMENT_TYPE',
  PROCESS_TYPES: 'PROCESS_TYPE',
  VISA_TYPE: 'VISA_TYPE',
  CHECK_VISA_FEE: 'CHECK_VISA_FEE',
  SUPPORT: 'SUPPORT'
}

export const RESPONSE_CODE = {
  LOGIN_FAIL: 'LOGIN_FAIL',
  SIGNUP_FAIL: 'SIGNUP_FAIL'
}

export const ROLES = {
  ADMIN: 'ADMIN',
  MEMBER: 'MEMBER',
  GUEST: 'GUEST'
}

export const ADMIN_ID = 1;