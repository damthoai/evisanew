import config from '../config/enviroment';
import mysql from 'mysql';
import bluebird from 'bluebird';

const connection = bluebird.promisifyAll(mysql.createConnection(config.db.mysql));

export default connection;
export function createConnection(dbconfig) {
    return bluebird.promisifyAll(mysql.createConnection(dbconfig));
}