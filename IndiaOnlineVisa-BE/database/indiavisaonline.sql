DROP TABLE IF EXISTS `t_content_page`;
CREATE TABLE `t_content_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` text,
  `title` text NOT NULL,
  `enumKey` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `t_email`;
CREATE TABLE `t_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `userEmail` text NOT NULL,
  `dateSend` datetime DEFAULT NULL,
  `orderId` int(11) DEFAULT NULL,
  `paymentId` int(11) DEFAULT NULL,
  `html` text NOT NULL,
  `emailFile` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `t_master_list`;
CREATE TABLE `t_master_list` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `key` text,
  `detail` text,
  `value` float DEFAULT NULL,
  `enumKey` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `t_master_list` WRITE;
INSERT INTO `t_master_list` VALUES (1,'1','Normal (Guaranteed within 3 working days)',0,'PROCESS_TYPE'),(2,'2','Urgent (Guaranteed within 24 hours)',79,'PROCESS_TYPE'),(3,'3','Super Urgent (Emergency Visa)',99,'PROCESS_TYPE'),(4,'Japan','Japan',25,'GOVERNMENT_TYPE'),(5,'Singapore','Singapore',25,'GOVERNMENT_TYPE'),(6,'Sri Lanka','Sri Lanka',25,'GOVERNMENT_TYPE'),(7,'Mozambique','Mozambique',75,'GOVERNMENT_TYPE'),(8,'Russia','Russia',75,'GOVERNMENT_TYPE'),(9,'Ukraine','Ukraine',75,'GOVERNMENT_TYPE'),(10,'United Kingdom','United Kingdom',75,'GOVERNMENT_TYPE'),(11,'USA','USA',75,'GOVERNMENT_TYPE'),(12,'Albania','Albania',50,'GOVERNMENT_TYPE'),(13,'Andorra','Andorra',50,'GOVERNMENT_TYPE'),(14,'Angola','Angola',50,'GOVERNMENT_TYPE'),(15,'Antigua & Barbuda','Antigua & Barbuda',50,'GOVERNMENT_TYPE'),(16,'Armenia','Armenia',50,'GOVERNMENT_TYPE'),(17,'Australia','Australia',50,'GOVERNMENT_TYPE'),(18,'Austria','Austria',50,'GOVERNMENT_TYPE'),(19,'Azerbaijan','Azerbaijan',50,'GOVERNMENT_TYPE'),(20,'Bahamas','Bahamas',50,'GOVERNMENT_TYPE'),(21,'Barbados','Barbados',50,'GOVERNMENT_TYPE'),(22,'Belgium','Belgium',50,'GOVERNMENT_TYPE'),(23,'Belize','Belize',50,'GOVERNMENT_TYPE'),(24,'Bolivia','Bolivia',50,'GOVERNMENT_TYPE'),(25,'Bosnia & Herzegovina','Bosnia & Herzegovina',50,'GOVERNMENT_TYPE'),(26,'Botswana','Botswana',50,'GOVERNMENT_TYPE'),(27,'Brazil','Brazil',50,'GOVERNMENT_TYPE'),(28,'Brunei','Brunei',50,'GOVERNMENT_TYPE'),(29,'Bulgaria','Bulgaria',50,'GOVERNMENT_TYPE'),(30,'Burundi','Burundi',50,'GOVERNMENT_TYPE'),(31,'Cambodia','Cambodia',50,'GOVERNMENT_TYPE'),(32,'Cameroon','Cameroon',50,'GOVERNMENT_TYPE'),(33,'Cape Verde','Cape Verde',50,'GOVERNMENT_TYPE'),(34,'Cayman Island','Cayman Island',50,'GOVERNMENT_TYPE'),(35,'Chile','Chile',50,'GOVERNMENT_TYPE'),(36,'China','China',50,'GOVERNMENT_TYPE'),(37,'China -Taiwan','China -Taiwan',50,'GOVERNMENT_TYPE'),(38,'China- Sar Hongkong','China- Sar Hongkong',50,'GOVERNMENT_TYPE'),(39,'China - Macau','China - Macau',50,'GOVERNMENT_TYPE'),(40,'Colombia','Colombia',50,'GOVERNMENT_TYPE'),(41,'Comoros','Comoros',50,'GOVERNMENT_TYPE'),(42,'Costa Rica','Costa Rica',50,'GOVERNMENT_TYPE'),(43,'Croatia','Croatia',50,'GOVERNMENT_TYPE'),(44,'Cuba','Cuba',50,'GOVERNMENT_TYPE'),(45,'Cyprus','Cyprus',50,'GOVERNMENT_TYPE'),(46,'Czech Republic','Czech Republic',50,'GOVERNMENT_TYPE'),(47,'Denmark','Denmark',50,'GOVERNMENT_TYPE'),(48,'Djibouti','Djibouti',50,'GOVERNMENT_TYPE'),(49,'Dominica','Dominica',50,'GOVERNMENT_TYPE'),(50,'Dominican Republic','Dominican Republic',50,'GOVERNMENT_TYPE'),(51,' East Timor',' East Timor',50,'GOVERNMENT_TYPE'),(52,'Ecuador','Ecuador',50,'GOVERNMENT_TYPE'),(53,'El Salvador','El Salvador',50,'GOVERNMENT_TYPE'),(54,'Eritrea','Eritrea',50,'GOVERNMENT_TYPE'),(55,'Estonia','Estonia',50,'GOVERNMENT_TYPE'),(56,'Finland','Finland',50,'GOVERNMENT_TYPE'),(57,'France','France',50,'GOVERNMENT_TYPE'),(58,'Gabon','Gabon',50,'GOVERNMENT_TYPE'),(59,'Gambia','Gambia',50,'GOVERNMENT_TYPE'),(60,'Georgia','Georgia',50,'GOVERNMENT_TYPE'),(61,'Germany','Germany',50,'GOVERNMENT_TYPE'),(62,'Ghana','Ghana',50,'GOVERNMENT_TYPE'),(63,'Greece','Greece',50,'GOVERNMENT_TYPE'),(64,'Grenada','Grenada',50,'GOVERNMENT_TYPE'),(65,'Guatemala','Guatemala',50,'GOVERNMENT_TYPE'),(66,'Guinea','Guinea',50,'GOVERNMENT_TYPE'),(67,'Guyana','Guyana',50,'GOVERNMENT_TYPE'),(68,'Haiti','Haiti',50,'GOVERNMENT_TYPE'),(69,' Vatican City - Holy See',' Vatican City - Holy See',50,'GOVERNMENT_TYPE'),(70,'Honduras','Honduras',50,'GOVERNMENT_TYPE'),(71,'Hungary','Hungary',50,'GOVERNMENT_TYPE'),(72,'Iceland','Iceland',50,'GOVERNMENT_TYPE'),(73,'Indonesia','Indonesia',50,'GOVERNMENT_TYPE'),(74,'Ireland','Ireland',50,'GOVERNMENT_TYPE'),(75,'Israel','Israel',50,'GOVERNMENT_TYPE'),(76,'Italy','Italy',50,'GOVERNMENT_TYPE'),(77,'Cote D Ivoire','Cote D Ivoire',50,'GOVERNMENT_TYPE'),(78,'Jordan','Jordan',50,'GOVERNMENT_TYPE'),(79,'Kenya','Kenya',50,'GOVERNMENT_TYPE'),(80,'Republic Of Korea','Republic Of Korea',50,'GOVERNMENT_TYPE'),(81,'Laos','Laos',50,'GOVERNMENT_TYPE'),(82,'Latvia','Latvia',50,'GOVERNMENT_TYPE'),(83,'Lesotho','Lesotho',50,'GOVERNMENT_TYPE'),(84,'Liberia','Liberia',50,'GOVERNMENT_TYPE'),(85,'Liechtenstein','Liechtenstein',50,'GOVERNMENT_TYPE'),(86,'Lithuania','Lithuania',50,'GOVERNMENT_TYPE'),(87,'Luxembourg','Luxembourg',50,'GOVERNMENT_TYPE'),(88,'Madagascar','Madagascar',50,'GOVERNMENT_TYPE'),(89,'Malawi','Malawi',50,'GOVERNMENT_TYPE'),(90,'Malaysia','Malaysia',50,'GOVERNMENT_TYPE'),(91,'Mali','Mali',50,'GOVERNMENT_TYPE'),(92,'Malta','Malta',50,'GOVERNMENT_TYPE'),(93,'Mexico','Mexico',50,'GOVERNMENT_TYPE'),(94,'Moldova','Moldova',50,'GOVERNMENT_TYPE'),(95,'Monaco','Monaco',50,'GOVERNMENT_TYPE'),(96,'Mongolia','Mongolia',50,'GOVERNMENT_TYPE'),(97,'Montenegro','Montenegro',50,'GOVERNMENT_TYPE'),(98,'Montserrat','Montserrat',50,'GOVERNMENT_TYPE'),(99,'Myanmar','Myanmar',50,'GOVERNMENT_TYPE'),(100,'Namibia','Namibia',50,'GOVERNMENT_TYPE'),(101,'Netherlands','Netherlands',50,'GOVERNMENT_TYPE'),(102,'New Zealand','New Zealand',50,'GOVERNMENT_TYPE'),(103,'Nicaragua','Nicaragua',50,'GOVERNMENT_TYPE'),(104,'Niger Republic','Niger Republic',50,'GOVERNMENT_TYPE'),(105,'Norway','Norway',50,'GOVERNMENT_TYPE'),(106,'Oman','Oman',50,'GOVERNMENT_TYPE'),(107,'Palestine','Palestine',50,'GOVERNMENT_TYPE'),(108,'Panama','Panama',50,'GOVERNMENT_TYPE'),(109,'Paraguay','Paraguay',50,'GOVERNMENT_TYPE'),(110,'Peru','Peru',50,'GOVERNMENT_TYPE'),(111,'Philippines','Philippines',50,'GOVERNMENT_TYPE'),(112,'Poland','Poland',50,'GOVERNMENT_TYPE'),(113,'Portugal','Portugal',50,'GOVERNMENT_TYPE'),(114,'Macedonia','Macedonia',50,'GOVERNMENT_TYPE'),(115,'Romania','Romania',50,'GOVERNMENT_TYPE'),(116,'Rwanda','Rwanda',50,'GOVERNMENT_TYPE'),(117,'Saint Lucia','Saint Lucia',50,'GOVERNMENT_TYPE'),(118,'Saint Vincent And The Grenadines','Saint Vincent And The Grenadines',50,'GOVERNMENT_TYPE'),(119,'San Marino','San Marino',50,'GOVERNMENT_TYPE'),(120,'Senegal','Senegal',50,'GOVERNMENT_TYPE'),(121,'Serbia','Serbia',50,'GOVERNMENT_TYPE'),(122,'Sierra Leone','Sierra Leone',50,'GOVERNMENT_TYPE'),(123,'Slovakia','Slovakia',50,'GOVERNMENT_TYPE'),(124,'Slovenia','Slovenia',50,'GOVERNMENT_TYPE'),(125,'United Arab Emirates','United Arab Emirates',50,'GOVERNMENT_TYPE'),(126,'Uzbekistan','Uzbekistan',50,'GOVERNMENT_TYPE'),(127,'Venezuela','Venezuela',50,'GOVERNMENT_TYPE'),(128,'Vietnam','Vietnam',50,'GOVERNMENT_TYPE'),(129,'Zambia','Zambia',50,'GOVERNMENT_TYPE'),(130,'Zimbabwe','Zimbabwe',50,'GOVERNMENT_TYPE'),(131,'Slovenia','Slovenia',50,'GOVERNMENT_TYPE'),(132,'Facebook','www.facebook.com',0,'SOCIAL'),(133,'Google+','www.google.com',0,'SOCIAL'),(134,'PENDING','PENDING',0,'ORDER_STATUS'),(135,'PAID','PAID',0,'ORDER_STATUS'),(136,'CANCELED_PAYMENT','CANCELED_PAYMENT',0,'ORDER_STATUS'),(137,'PROCESSING','PROCESSING',0,'ORDER_STATUS'),(138,'FINISH','FINISH',0,'ORDER_STATUS'),(139,'NONE','',0,'GENDER'),(140,'MALE','MALE',0,'GENDER'),(141,'FEMALE','FEMALE',0,'GENDER'),(142,'241','Indian e-Tourist visa - 60 days double entries',0,'TYPE_OF_CARDS'),(143,'243','Indian e-Business visa - 60 days double entries',0,'TYPE_OF_CARDS'),(144,'244','Indian e-Medical visa - 60 days triple entries',0,'TYPE_OF_CARDS'),(145,'1','Airport',0,'ARRIVAL_PORT_TYPE'),(146,'2','Sea Port',0,'ARRIVAL_PORT_TYPE'),(147,'1','Chennai - MAA - Chennai - Tamil Nadu',0,'PORT_OF_AIRPORT'),(148,'2','Indira Gandhi - DEL - Delhi - Delhi',0,'PORT_OF_AIRPORT'),(149,'3','Netaji Subhash Chandra Bose - CCU - Kolkata - West Bengal',0,'PORT_OF_AIRPORT'),(150,'4','Chhatrapati Shivaji - BOM - Mumbai - Maharashtra',0,'PORT_OF_AIRPORT'),(151,'5','Cochin – COK - Kochi - Kerala',0,'PORT_OF_AIRPORT'),(152,'6','Rajiv Gandhi – HYD - Hyderabad - Telangana',0,'PORT_OF_AIRPORT'),(153,'7','Kempegowda – BLR - Bangalore - Karnataka',0,'PORT_OF_AIRPORT'),(154,'8','Goa – GOI – Dabolim – Goa',0,'PORT_OF_AIRPORT'),(155,'9','Thiruvananthapuram – TRV - Thiruvananthapuram - Kerala',0,'PORT_OF_AIRPORT'),(156,'10','Sardar Patel - AMD - Ahmedabad - Gujarat',0,'PORT_OF_AIRPORT'),(157,'11','Sri Guru Ram Dass Jee - ATQ - Amritsar - Punjab',0,'PORT_OF_AIRPORT'),(158,'12','Gaya - GAY - Gaya - Bihar',0,'PORT_OF_AIRPORT'),(159,'13','Jaipur - JAI - Jaipur - Rajasthan',0,'PORT_OF_AIRPORT'),(160,'14','Chaudhary Charan Singh - LKO - Lucknow - Uttar Pradesh',0,'PORT_OF_AIRPORT'),(161,'15','Varanasi - VNS - Varanasi - Uttar Pradesh',0,'PORT_OF_AIRPORT'),(162,'16','Tiruchirappalli - TRZ - Tiruchirappalli - Tamil Nadu',0,'PORT_OF_AIRPORT'),(163,'17','Bagdogra - IXB - Bagdogra - West Bengal',0,'PORT_OF_AIRPORT'),(164,'18','Calicut - CCJ - Kozhikode  - Kerala',0,'PORT_OF_AIRPORT'),(165,'19','Chandigarh- IXC - Chandigarh - Chandigarh',0,'PORT_OF_AIRPORT'),(166,'20','Coimbatore - CJB - Coimbatore - Tamil Nadu',0,'PORT_OF_AIRPORT'),(167,'21','Lokpriya Gopinath Bordoloi  - GAU - Guwahati - Assam',0,'PORT_OF_AIRPORT'),(168,'22','Mangalore - IXE - Mangalore  - Karnataka',0,'PORT_OF_AIRPORT'),(169,'23','Dr. Babasaheb Ambedkar - NAG - Nagpur - Maharashtra',0,'PORT_OF_AIRPORT'),(170,'24','Pune - PNQ - Pune -Maharashtra',0,'PORT_OF_AIRPORT'),(171,'25','Cochin Seaport',0,'PORT_OF_SEAPORT'),(172,'26','Mangalore Seaport',0,'PORT_OF_SEAPORT'),(173,'27','Goa Seaport',0,'PORT_OF_SEAPORT'),(174,'visafee','Visa Fee',69,'VISA_TYPE'),(175,'checkvisafee','Check Visa Fee',39,'CHECK_VISA_FEE'),(176,'support','',0,'SUPPORT'),(177,'supportCC','',0,'SUPPORT');
UNLOCK TABLES;


DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order` (
  `orderId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `transactionId` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `transactionDate` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `orderDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `lastUpdateDate` datetime DEFAULT NULL,
  `orderStatus` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `numverOfTravelInYear` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `numberOfVisa` int(2) DEFAULT NULL,
  `typeOfVisa` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `arrivalPortType` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `portOfArrival` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `processType` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `applicants` json DEFAULT NULL,
  `dateOfArrival` datetime DEFAULT NULL,
  `dateOfDeparture` datetime DEFAULT NULL,
  `flightNumber` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `arrivalTime` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `contactEmail` varchar(150) CHARACTER SET utf8 NOT NULL,
  `contactName` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `contactPhone` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `contactAddress` text,
  `contactCountry` text,
  `contactNote` text,
  `firstName` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `lastName` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `country` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `address` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `city` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `state` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `postCode` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `cardNumber` varchar(16) CHARACTER SET utf8 DEFAULT NULL,
  `paymentType` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `userAgent` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `ipClient` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `visaFee` float DEFAULT '0',
  `paymentFor` float DEFAULT '0',
  `govermentVisaFee` float DEFAULT '0',
  `processFee` float DEFAULT '0',
  `promoteDetail` json DEFAULT NULL,
  `version` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=70000000 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `t_payment`;
CREATE TABLE `t_payment` (
  `paymentId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `paymentStatus` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `transactionId` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `transactionDate` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `contactEmail` varchar(150) CHARACTER SET utf8 NOT NULL,
  `firstName` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `lastName` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `country` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `address` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `city` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `state` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `postCode` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `cardNumber` varchar(16) CHARACTER SET utf8 DEFAULT NULL,
  `paymentType` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `userAgent` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `ipClient` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `amount` float DEFAULT '0',
  `paymentFor` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `note` text DEFAULT NULL,
  `version` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`paymentId`)
) ENGINE=InnoDB AUTO_INCREMENT=40000000 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `t_transaction_log`;
CREATE TABLE `t_transaction_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderId` int(11) DEFAULT '0',
  `paymentId` int(11) DEFAULT '0',
  `transactionId` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `transactionDate` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `request` json DEFAULT NULL,
  `response` json DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `password` varchar(300) CHARACTER SET utf8 NOT NULL,
  `role` varchar(100) CHARACTER SET utf8 NOT NULL,
  `fullname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `phone` varchar(15) CHARACTER SET utf8 NOT NULL,
  `country` varchar(50) CHARACTER SET utf8 NOT NULL,
  `address` varchar(80) CHARACTER SET utf8 NOT NULL,
  `salt` text NOT NULL,
  `provider` varchar(20) CHARACTER SET utf8 DEFAULT 'local',
  `profile` json DEFAULT NULL,
  `isDeleted` tinyint(1) DEFAULT '0',
  `isActived` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
LOCK TABLES `t_user` WRITE;
INSERT INTO `t_user` VALUES (1, 'admin', 'admin@indiaonlineevisa.com', 'Hwo5/ie/GMHY3m8fdTJeX75TGVAidxT8HgBr6Q5A9wUfuWcFjxQjhscEZeTEYWf2W0mNSG1zCrsQtGoaoZyIkA==', 'ADMIN', 'ADMIN', '', '', '', 'oraTnyL9BUzrfYNQYdf4sw==','local',null,0,1);
UNLOCK TABLES;
