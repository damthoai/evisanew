import ejs from 'ejs';
import fs from 'fs';
import pdf from 'html-pdf';

const TEMPLATE_DIR = __dirname;
const pdfOptions = {
  "height": "297mm",
  "width": "210mm",
  "format": "A4",
  "orientation": "portrait",
  "header": {
    "height": "45mm"
  },
  "footer": {
    "height": "45mm"
  }
};
export function render(templateName, data, callback) {
  const TEMPLATE_PATH = TEMPLATE_DIR + `/${templateName}.ejs`;
  fs.readFile(TEMPLATE_PATH, 'utf8', function (err, $template) {
    if (err) {
      callback(err);
    } else {
      try {
        const html = ejs.render($template, data);
        callback(null, html);
      } catch (error) {
        callback(error);
      }
    }
  });
}
export function exportPdf(templateName, data, callback) {
  render(templateName, data, (err, content) => {
    if (err) {
      callback(err);
    } else {
      try {
        pdf.create(content, pdfOptions).toBuffer(($err, $buffer) => {
          if ($err) {
            return callback($err);
          } else {
            callback(null, $buffer);
          }
        });
      } catch (error) {
        callback(error);
      }
    }
  });
}