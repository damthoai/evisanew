import jwt from 'jsonwebtoken';
import config from '../config/enviroment';
import logger from '../logger';

export function handleError(error, next) {
  logger.log('error', error.stack || error.toString());
  next(error);
}

export function maskCardNumber(cardNumber) {
  return (cardNumber || '').replace(/\d(?=\d{4})/g, "*");
}

export function noop(){}
