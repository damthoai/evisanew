import db from '../../database';
import _ from 'lodash';
import moment from 'moment';
import * as MasterDataModel from '../masterList/model';

export function create(validvisa){
    const validvisadetail = {
        email: validvisa.Email,
        id_number: validvisa.idNumber,
        passport_number: validvisa.passportNumber,
        payment_box: validvisa.payment_box,
        date_created:  moment.utc().format('YYYY-MM-DD HH:mm:ss'),
        type: validvisa.type
    }
    return db.queryAsync("INSERT INTO t_valid_visa set ? ", validvisadetail).then((result) => {
        validvisadetail.id = result.insertId;
        return validvisadetail;
      });
}
export function update_payment(id, payment_transaction){
    const sql = `
    UPDATE t_valid_visa set 
      is_payment = 1,
      payment_transaction = ?
    WHERE id = ?`;
  const values = [
    payment_transaction,
    id
  ];
  return db.queryAsync(sql, values);
}
export function deleted(id, userId){
    const sql = `
    UPDATE t_valid_visa set 
    is_deleted = 1, 
    last_modifier = ?
    WHERE id = ?`;
  const values = [
    userId,
    id
  ];
  return db.queryAsync(sql, values);
}


export function getById(id) {
    return db.queryAsync("SELECT * FROM t_valid_visa WHERE id = ?", id).then((validvisa) => _.first(validvisa));
  }