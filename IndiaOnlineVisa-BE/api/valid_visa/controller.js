import path from 'path';
import _ from 'lodash';
import fs from 'fs';
import moment from 'moment';
import db from '../../database';
import * as validVisaModel from './model';
import logger from '../../logger';
import { render as renderTemplate, exportPdf } from '../../templates';
import { sendMail } from '../../Mailer';
import * as MasterDataModel from '../masterList/model';
import * as Utils from '../../utils';
import { ORDER_STATUS, KEYS, ROLES, ADMIN_ID } from '../../constants';
import config from '../../config/enviroment';
export function CreateValidVisa(req, res, next) {
    try {    
        validVisaModel.create(req.body)
          .then((validvisa) => {
            res.send(200, normalizeValidVisa(validvisa));          
            setTimeout(sendCheckStatusEmail, 0, validvisa);
          })
          .catch(error => handleError(error, next));
      } catch (error) {
        handleError(error, next);
      }
      
}
export function UpdateValidVisa(req, res, next) {
    try {
        const userId = (req.dbUser || {}).id;
        const id = req.body.id;
        const transaction = res.body.transaction;
        validVisaModel.getById(id)
          .then((validvisa) => {
            if (!_.isEmpty(validvisa.id)) {
              return next(new restify.UnprocessableEntityError('order doest not exist'));
            }          
            validVisaModel.update_payment(id, transaction)
              .then((validvisa) => {
                res.send(200, validvisa);
              })
              .catch((error) => handleError(error, next));
          })
          .catch((error) => handleError(error, next));
      } catch (error) {
        handleError(error, next);
      }
}



function normalizeValidVisa(validvisa){
    return _.omit(validvisa);
  }


function handleError(error, next) {
    logger.log('error', error.stack || error.toString());
    next(error);
  }
// ================================================
function sendCheckStatusEmail(mailModel) {
  
  try {
    const template = 'valid-pickup-visa';
    mailModel.request =  mailModel.type === 'check-visa'? 'Check visa':'Pickup visa';
    renderTemplate(template, mailModel, (err, htmlBody) => {
      const message = {
        to: mailModel.email,
        from: config.emailFrom,
        body: htmlBody,
        subject: mailModel.type === 'check-visa'? `Check visa of ID Number #${mailModel.id}`: `Pickup visa of ID Number #${mailModel.id}`,
        files: []
      };
      logger.log(message);
      sendMail(message, (err, rawEmail) => {
        if (err) {
          logger.log('error', '%s', err.stack || err.toString());
        }
      });
    });
  } catch (err) {
    logger.log('error', '%s', err.stack || err.toString());
  }
}
// ================================================