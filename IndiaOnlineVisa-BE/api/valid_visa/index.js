import * as controller from './controller';
import { Router } from 'restify-router';
import {getUserInfo} from '../auth/authService';

const router = new Router();

router.post({path: '/', version: '0.0.1'}, getUserInfo, controller.CreateValidVisa);
router.put({path: '/', version: '0.0.1'}, getUserInfo, controller.UpdateValidVisa);


export default router;