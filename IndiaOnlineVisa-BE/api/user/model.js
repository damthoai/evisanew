import db from '../../database';
import crypto from 'crypto';
import _ from 'lodash';
import { ROLES } from '../../constants'

export function login(email, password) {
  return db.queryAsync('SELECT * FROM t_user WHERE email = ? and isActived = 1 and provider = ? and isDeleted = 0', [email, 'local'])
    .then((users) => {
      const user = _.first(users);
      if(!user) {
        return user;
      }
      return encryptPassword(password, user.salt).then((encryptedpassword) => {
        if(user.password === encryptedpassword) {
          return user;
        }
        return null;
      });
    });
}
export function findUser(userId, email) {
  return db.queryAsync('SELECT * FROM t_user WHERE email = ? and isActived = 1 and id = ? and isDeleted = 0', [email, _.isEmpty(userId) ? null : Number(userId)])
    .then((users) => {
      const user = _.first(users);
      return user;
    });
}
export function findUserByEmail(email) {
  return db.queryAsync('SELECT * FROM t_user WHERE email = ? and provider = ? and isActived = 1 and isDeleted = 0', [email, 'local'])
    .then((users) => {
      const user = _.first(users);
      return user;
    });
}
export function getAll(limit, page) {
  return db.queryAsync('SELECT * FROM t_user where isDeleted = 0 LIMIT ? OFFSET ?', [limit, page]);
}
export function getUserById(id) {
  return db.queryAsync('SELECT * FROM t_user where id = ?', id).then(users => _.first(users));
}
export function updatePassword(id, currentPassword, newPassword) {
  return getUserById(id).then((user) => {
    if(user) {
      return encryptPassword(currentPassword, user.salt).then((password) => {
        if (user.password !== password){
          throw new Error('Current password is incorrect');
        }
        return encryptPassword(newPassword, user.salt).then(($password) => {
          const data = { password: $password };
          return db.queryAsync("UPDATE t_user set ? WHERE id = ?", [data, user.id]);
        });
      });
    }
    throw new Error('Cannot find user');
  });
}
export function updateRecoverPassword(id, newPassword, salt) {
  return encryptPassword(newPassword, salt).then((password) => {
    const data = { password };
    return db.queryAsync("UPDATE t_user set ? WHERE id = ?", [data, id]);
  });
}
export function update(id, user) {
  const data = {
    fullname: user.fullname,
    phone: user.phone,
    country: user.country,
    address: user.address
  };
  return db.queryAsync("UPDATE t_user set ? WHERE id = ?", [data, id]);
}
export function insert(user) {
  const data = {
    username: user.email,
    email: user.email,
    password: user.password,
    salt: '',
    role: user.role || ROLES.MEMBER,
    fullname: user.fullname,
    phone: user.phone,
    country: user.country,
    address: user.address,
    isDeleted: 0
  };
  return genSalt().then((salt) => {
    data.salt = salt;
    return encryptPassword(data.password, salt).then((password) => {
      data.password = password;
      return db.queryAsync("INSERT INTO t_user set ? ", data).then((result) => {
        data.id = result.insertId;
        return _.omit(data, ['salt', 'isDeleted', 'password', 'role']);
      });
    });
  });
}
export function remove(id, isDeleted) {
  const data = {
    isDeleted: (isDeleted === 0 ? 1 : 0)
  };
  return db.queryAsync("UPDATE t_user set ? WHERE id = ? ", [data, id]);
}
export function active(id, isActived) {
  const data = {
    isActived: (isActived === 0 ? 1 : 0)
  };
  return db.queryAsync("UPDATE t_user set ? WHERE id = ? ", [data, id]);
}
export function findGoogleAccount(googleAccountEmail) {
  return db.queryAsync("SELECT * FROM t_user where provider = ? and email = ?", ['google', googleAccountEmail]);
}
export function insertGoogleAccount(accessToken, refreshToken, profile) {
  const data = {
    username: profile.emails[0].value,
    email: profile.emails[0].value,
    password: '',
    salt: '',
    role: ROLES.MEMBER,
    fullname: profile.displayName,
    provider: 'google',
    profile: JSON.stringify({
      accessToken,
      refreshToken,
      id: profile.id,
      displayName: profile.displayName,
      name: profile.name,
      emails: profile.emails,
      photos: profile.photos,
      gender: profile.gender,
      provider: profile.provider
    }),
    phone: '',
    country: '',
    address: '',
    isDeleted: 0
  };
  return db.queryAsync("INSERT INTO t_user set ? ", data).then((user) => {
    data.id = user.insertId;
    return _.omit(data, ['salt', 'isDeleted', 'password', 'role', 'provider', 'profile']);
  });
}
export function findFacebookAccount(facebookAccountEmail) {
  return db.queryAsync("SELECT * FROM t_user where provider = ? and email = ?", ['facebook', facebookAccountEmail]);
}
export function insertFacebookAccount(accessToken, refreshToken, profile) {
  const data = {
    username: profile.emails[0].value,
    email: profile.emails[0].value,
    password: '',
    salt: '',
    role: ROLES.MEMBER,
    fullname: profile.displayName,
    provider: 'facebook',
    profile: JSON.stringify({
      accessToken,
      refreshToken,
      id: profile.id,
      displayName: profile.displayName,
      emails: profile.emails,
      provider: 'facebook'
    }),
    phone: '',
    country: '',
    address: '',
    isDeleted: 0
  };
  return db.queryAsync("INSERT INTO t_user set ? ", data).then((user) => {
    data.id = user.insertId;
    return _.omit(data, ['salt', 'isDeleted', 'password', 'role', 'provider', 'profile']);
  });
}

// private
function encryptPassword(password, salt) {
  return new Promise((resolve, reject) => {
    const defaultIterations = 10000;
    const defaultKeyLength = 64;

    crypto.pbkdf2(password, new Buffer(salt, 'base64'), defaultIterations, defaultKeyLength, 'sha512', (error, key) => {
      if (error) {
        reject(error);
      } else {
        resolve(key.toString('base64'));
      }
    });
  });
};
function genSalt(byteSize) {
  return new Promise((resolve, reject) => {
    const defaultByteSize = 16;

    if (!byteSize) {
      byteSize = defaultByteSize;
    }

    return crypto.randomBytes(byteSize, (error, salt) => {
      if (error) {
        reject(error);
      } else {
        resolve(salt.toString('base64'));
      }
    });
  });
};