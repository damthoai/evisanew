import { sendMail } from '../../Mailer';
import { render as renderTemplate } from '../../templates';
import config from '../../config/enviroment';
import { handleError, noop } from '../../utils';
import * as UserModel from './model';
import _ from 'lodash';
import {ROLES} from '../../constants';
import restify from 'restify';

export function UserList(req, res, next) {
  const { limit, page } = req.query;
  const user = req.dbUser || {};
  const userRole = user.role;
  if (userRole === ROLES.ADMIN) {
    UserModel.getAll(+limit, +page).then((result) => res.send(200, result)).catch(error => handleError(error, next));
  } else {
    next(new restify.ForbiddenError('you dont have permission to access.'));
  }
}
export function UserDetail(req, res, next) {
  if (!req.dbUser) {
    next(new restify.UnauthorizedError('your session is expired.'));
  } else {
    UserModel.getUserById(req.dbUser.id).then((result) => res.send(200, _.omit(result, ['isActived', 'isDeleted']))).catch(error => handleError(error, next));
  }
}
export function RecoverPassword(req, res, next) {
  const email = req.body.email;
  UserModel.findUserByEmail(email).then((user)=>{
    if (!user) {
      return next(new restify.ResourceNotFoundError('Cannot find user with this email.'));
    }
    const newPassword = Math.random().toString(36).slice(-8);
    return UserModel.updateRecoverPassword(user.id, newPassword, user.salt).then(() => {
      res.send(204);
      user.newPassword = newPassword;
      setTimeout(sendRecoverPassword, 0, user);
    });
  }).catch(error => handleError(error, next));
}
export function ChangePassword(req, res, next) {
  const id = req.dbUser.id;
  const newPassword = req.body.newPassword;
  const currentPassword = req.body.currentPassword;
  UserModel.updatePassword(id, currentPassword, newPassword)
    .then(()=>res.send(204))
    .catch(error => handleError(error, next));
}
export function EditUser(req, res, next) {
  const id = req.params.id;
  const data = {
    fullname: req.body.fullname,
    phone: req.body.phone,
    country: req.body.country,
    address: req.body.address
  };
  UserModel.update(id, data).then(()=>res.send(204)).catch(error => handleError(error, next));
}
export function RemoveUser(req, res, next) {
  UserModel.remove(req.params.id, req.body.isDeleted).then(() => res.send(204)).catch(error => handleError(error, next));
}
export function ActiveUser(req, res, next) {
  UserModel.active(req.params.id, req.body.isActived).then(() => res.send(204)).catch(error => handleError(error, next));
}
export function AddUser(req, res, next) {
  UserModel.insert(_.omit(req.body, 'role')).then(user => {
      sendRegistrationEmail(user);
      res.send(201, user);
  }).catch(error => handleError(error, next));
}

function sendRegistrationEmail(userModel) {
  try {
    const template = 'registration';
    renderTemplate(template, userModel, (err, htmlBody) => {
      if (err) {
        handleError(err, noop);
      } else {
        const message = {
          to: userModel.email,
          from: config.emailFrom,
          body: htmlBody,
          subject: 'Congratulation for successful registration',
          files: []
        };
        sendMail(message, ($error) => {
          if ($error) {
            handleError($error, noop);
          }
        });
      }
    });
  } catch (error) {
    handleError(error, noop);
  }
}

function sendRecoverPassword(userModel) {
  try {
    const template = 'recover-password';
    renderTemplate(template, userModel, (err, htmlBody) => {
      if (err) {
        handleError(err, noop);
      } else {
        const message = {
          to: userModel.email,
          from: config.emailFrom,
          body: htmlBody,
          subject: 'Notification of password recovery',
          files: []
        };
        sendMail(message, ($error) => {
          if ($error) {
            handleError($error, noop);
          }
        });
      }
    });
  } catch (error) {
    handleError(error, noop);
  }
}