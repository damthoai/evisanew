import * as controller from './controller';
import { Router } from 'restify-router';
import {validateAuthentication} from '../auth/authService';

const router = new Router();

router.get({path: '/', version: '0.0.1'}, validateAuthentication, controller.UserList);
router.get({path: '/detail', version: '0.0.1'}, validateAuthentication, controller.UserDetail);
router.get({path: '/:id', version: '0.0.1'}, controller.UserDetail);
router.post({path: '/', version: '0.0.1'}, controller.AddUser);
router.put({path: '/recoverPassword', version: '0.0.1'}, controller.RecoverPassword);
router.put({path: '/changePWD', version: '0.0.1'}, validateAuthentication, controller.ChangePassword);
router.put({path: '/:id', version: '0.0.1'}, validateAuthentication, controller.EditUser);
router.post({path: '/:id/removeUser', version: '0.0.1'}, validateAuthentication, controller.RemoveUser);
router.post({path: '/:id/activeUser', version: '0.0.1'}, validateAuthentication, controller.ActiveUser);

export default router;