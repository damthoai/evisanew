import * as controller from './controller';
import { Router } from 'restify-router';
import {getUserInfo, validateAuthentication} from '../auth/authService';

const router = new Router();

router.post({path: '/', version: '0.0.1'}, getUserInfo, controller.CreateOrder);
router.put({path: '/', version: '0.0.1'}, getUserInfo, controller.UpdateOrder);
router.put({path: '/paynow', version: '0.0.1'}, getUserInfo, controller.payOrder);
router.put({path: '/order-status', version: '0.0.1'}, validateAuthentication, controller.updateOrderStatus);
router.get({path: '/', version: '0.0.1'}, getUserInfo, controller.OrderList);
router.get({path: '/:id', version: '0.0.1'}, getUserInfo, controller.OrderDetail);
router.post({path: '/checkStatus', version: '0.0.1'}, controller.CheckStatus);

export default router;