import _ from 'lodash';
import moment from 'moment';
import db from '../../database';
import { ORDER_STATUS, KEYS } from '../../constants';
import * as MasterDataModel from '../masterList/model';

export function getAllByUserId(userId, limit, page) {
  return db.queryAsync('SELECT * FROM t_order WHERE userId = ? LIMIT ? OFFSET ?',
    [Number(userId || Number.MIN_SAFE_INTEGER), Number(limit), Number(page)]);
}

export function getAllByAdmin(limit, page) {
  return db.queryAsync('SELECT * FROM t_order LIMIT ? OFFSET ?',
    [Number(limit), Number(page)]);
}

export function getByEmail(model) {
  return db.queryAsync('SELECT * FROM t_order where contactEmail = ?', model.contactEmail);
}

export function getById(orderId) {
  return db.queryAsync("SELECT * FROM t_order WHERE orderId = ?", orderId).then((orders) => _.first(orders));
}

export function insert(order, userId, userAgent, ipClient) {
  const orderData = {
    userId, //if guest is null
    orderDate: moment.utc().format('YYYY-MM-DD HH:mm:ss'),
    orderStatus: ORDER_STATUS.PENDING,
    version: Date.now().toString(),

    // step 1
    numberOfVisa: order.numberOfVisa,
    typeOfVisa: order.typeOfVisa,
    arrivalPortType: order.arrivalPortType,
    portOfArrival: order.portOfArrival,
    processType: order.processType,
    // step 2
    numverOfTravelInYear: _.isEmpty(order.numverOfTravelInYear) ? '0' : order.numverOfTravelInYear,
    question2: order.question2,
    question3: order.question3,
    question4: order.question4,
    question5: order.question5,
    applicants: order.applicants,
    dateOfArrival: order.dateOfArrival,
    dateOfDeparture: order.dateOfDeparture,
    contactEmail: order.contactEmail,
    contactName: order.contactName,
    contactPhone: order.contactPhone,
    contactAddress: order.contactAddress,
    contactCountry: order.contactCountry,
    contactNote: order.contactNote,
    flightNumber: order.flightNumber,
    arrivalTime: order.arrivalTime,

    rate: order.rate,

    userAgent,
    ipClient
  };
  return MasterDataModel.getMasterList().then((masterData) => {
    return MasterDataModel.getExchangeRate().then(rate => {
      orderData.rate = (rate.rates || {}).AUD || 1;

      const fees = calculateFees(orderData, masterData);
      orderData.applicants = JSON.stringify(fees.applicants);
      orderData.visaFee = fees.visaFee;
      orderData.govermentVisaFee = fees.govermentVisaFee;
      orderData.processFee = fees.processFee;

      return db.queryAsync("INSERT INTO t_order set ? ", orderData).then((result) => {
        orderData.applicants = fees.applicants;
        orderData.totalVisaFee = fees.totalVisaFee;
        orderData.totalFees = fees.totalFees;
        orderData.orderId = result.insertId;
        return orderData;
      });
    });
  });
}
export function updateStatus(order) {
  const sql = `
      UPDATE t_order set 
        orderStatus = ?
      WHERE orderId = ?`;
    const values = [
      order.orderStatus,
      order.orderId
    ];
    return db.queryAsync(sql, values);
}
export function update(order, userId, userAgent, ipClient) {
  const orderData = {
    lastUpdateDate: moment.utc().format('YYYY-MM-DD HH:mm:ss'),
    orderStatus: order.orderStatus,
    version: Date.now().toString(),
    // step 1
    numberOfVisa: order.numberOfVisa,
    typeOfVisa: order.typeOfVisa,
    arrivalPortType: order.arrivalPortType,
    portOfArrival: order.portOfArrival,
    processType: order.processType,
    // step 2
    numverOfTravelInYear: _.isEmpty(order.numverOfTravelInYear) ? '0' : order.numverOfTravelInYear,
    question2: order.question2,
    question3: order.question3,
    question4: order.question4,
    question5: order.question5,
    applicants: order.applicants,
    dateOfArrival: order.dateOfArrival,
    dateOfDeparture: order.dateOfDeparture,
    contactEmail: order.contactEmail,
    contactName: order.contactName,
    contactPhone: order.contactPhone,
    contactAddress: order.contactAddress,
    contactCountry: order.contactCountry,
    contactNote: order.contactNote,
    flightNumber: order.flightNumber,
    arrivalTime: order.arrivalTime,

    rate: order.rate,

    userAgent,
    ipClient
  };
  return MasterDataModel.getMasterList().then((masterData) => {
    return MasterDataModel.getExchangeRate().then(rate => {
      orderData.rate = (rate.rates || {}).AUD || 1;

      const fees = calculateFees(orderData, masterData);
      orderData.applicants = JSON.stringify(fees.applicants);
      orderData.visaFee = fees.visaFee;
      orderData.govermentVisaFee = fees.govermentVisaFee;
      orderData.processFee = fees.processFee;
      const sql = `
        UPDATE t_order set 
          lastUpdateDate = ?, orderStatus = ?, version = ?,
          numberOfVisa = ?, typeOfVisa = ?, arrivalPortType = ?, portOfArrival = ?, processType = ?,
          numverOfTravelInYear = ?, question2 = ?, question3 = ?, question4 = ?, question5 = ?, applicants = ?, dateOfArrival = ?, dateOfDeparture = ?, contactEmail = ?, contactName = ?, contactPhone = ?, contactAddress = ?, contactCountry = ?, contactNote = ?, flightNumber = ?, arrivalTime = ?,
          userAgent = ?, ipClient = ?,
          visaFee = ?, govermentVisaFee = ?, processFee = ?,
          rate = ?
        WHERE orderId = ? and userId = ?`;
      const values = [
        orderData.lastUpdateDate, orderData.orderStatus, orderData.version,
        orderData.numberOfVisa, orderData.typeOfVisa, orderData.arrivalPortType, orderData.portOfArrival, orderData.processType,
        orderData.numverOfTravelInYear, orderData.question2, orderData.question3, orderData.question4, orderData.question5, orderData.applicants, orderData.dateOfArrival, orderData.dateOfDeparture, orderData.contactEmail, orderData.contactName, orderData.contactPhone, orderData.contactAddress,
        orderData.contactCountry, orderData.contactNote, orderData.flightNumber, orderData.arrivalTime,
        orderData.userAgent, orderData.ipClient,
        orderData.visaFee, orderData.govermentVisaFee, orderData.processFee,
        orderData.rate,
        order.orderId, userId
      ];
      return db.queryAsync(sql, values).then((result) => {
        orderData.applicants = fees.applicants;
        orderData.totalVisaFee = fees.totalVisaFee;
        orderData.totalFees = fees.totalFees;
        orderData.orderId = order.orderId;
        orderData.userId = userId;
        return orderData;
      });
    });
  });
};

export function updatePayment(order, userId, userAgent, ipClient) {
  const orderData = {
    transactionId: order.transactionId,
    transactionDate: order.transactionDate,
    lastUpdateDate: moment.utc().format('YYYY-MM-DD HH:mm:ss'),
    orderStatus: order.orderStatus,
    version: Date.now().toString(),
    // step 1
    numverOfTravelInYear: _.isEmpty(order.numverOfTravelInYear) ? '0' : order.numverOfTravelInYear,
    question2: order.question2,
    question3: order.question3,
    question4: order.question4,
    question5: order.question5,
    numberOfVisa: order.numberOfVisa,
    typeOfVisa: order.typeOfVisa,
    arrivalPortType: order.arrivalPortType,
    portOfArrival: order.portOfArrival,
    processType: order.processType,
    // step 2
    applicants: JSON.stringify(order.applicants),
    dateOfArrival: order.dateOfArrival,
    dateOfDeparture: order.dateOfDeparture,
    contactEmail: order.contactEmail,
    contactName: order.contactName,
    contactPhone: order.contactPhone,
    contactAddress: order.contactAddress,
    contactCountry: order.contactCountry,
    contactNote: order.contactNote,
    flightNumber: order.flightNumber,
    arrivalTime: order.arrivalTime,
    // step 3
    firstName: order.firstName,
    lastName: order.lastName,
    country: order.country,
    address: order.address,
    city: order.city,
    state: order.state,
    postCode: order.postCode,

    cardNumber: order.cardNumber,
    paymentType: order.paymentType,
    visaFee: order.visaFee,
    govermentVisaFee: order.govermentVisaFee,
    processFee: order.processFee,

    rate: order.rate,

    userAgent,
    ipClient
  };
  return MasterDataModel.getMasterList().then((masterData) => {
    return MasterDataModel.getExchangeRate().then(rate => {
      orderData.rate = (rate.rates || {}).AUD || 1;

      const sql = `
        UPDATE t_order set 
          lastUpdateDate = ?, orderStatus = ?, version = ?,
          numberOfVisa = ?, typeOfVisa = ?, arrivalPortType = ?, portOfArrival = ?, processType = ?,
          numverOfTravelInYear = ?, question2 = ?, question3 = ?, question4 = ?, question5 = ?, applicants = ?, dateOfArrival = ?, dateOfDeparture = ?, contactEmail = ?, contactName = ?, contactPhone = ?, contactAddress = ?, contactCountry = ?, contactNote = ?, flightNumber = ?, arrivalTime = ?,
          firstName = ?, lastName = ?, country = ?, address = ?, city = ?, state = ?, postCode = ?,
          cardNumber = ?, paymentType = ?,
          userAgent = ?, ipClient = ?,
          visaFee = ?, govermentVisaFee = ?, processFee = ?,
          rate = ?,
          transactionId = ?, transactionDate = ?
        WHERE orderId = ? and userId = ?`;
      const values = [
        orderData.lastUpdateDate, orderData.orderStatus, orderData.version,
        orderData.numberOfVisa, orderData.typeOfVisa, orderData.arrivalPortType, orderData.portOfArrival, orderData.processType,
        orderData.numverOfTravelInYear, orderData.question2, orderData.question3, orderData.question4, orderData.question5, orderData.applicants, orderData.dateOfArrival, orderData.dateOfDeparture, orderData.contactEmail, orderData.contactName, orderData.contactPhone, orderData.contactAddress,
        orderData.contactCountry, orderData.contactNote, orderData.flightNumber, orderData.arrivalTime,
        orderData.firstName, orderData.lastName, orderData.country, orderData.address, orderData.city, orderData.state, orderData.postCode,
        orderData.cardNumber, orderData.paymentType,
        orderData.userAgent, orderData.ipClient,
        orderData.visaFee, orderData.govermentVisaFee, orderData.processFee,
        orderData.rate,
        orderData.transactionId, orderData.transactionDate,
        order.orderId, userId
      ];
      return db.queryAsync(sql, values).then((result) => {
        orderData.applicants = order.applicants;
        orderData.orderId = order.orderId;
        orderData.userId = userId;
        return orderData;
      });
    });
  });
};
export function logTransaction(orderId, transactionId, transactionDate, request, response) {
  return db.queryAsync("INSERT INTO t_transaction_log set ? ", {
    transactionId,
    transactionDate,
    orderId,
    request: JSON.stringify(request),
    response: JSON.stringify(response)
  });
}
export function calculateFees(visaInfo, masterData) {
  //calculate fee for applicants
  const applicants = _.cloneDeep(visaInfo.applicants) || [];
  _.forEach(applicants, applicant => {
    applicant.govermentFee = calculateGovernmentFee(applicant.nationality, masterData);
  });
  //calculate fee for other
  let visaFee = getVisaFee(masterData);

  if (visaInfo.numberOfVisa > 10) {
    visaFee = 59; // hard code in here, will replace by apply rule dynamic
  }

  const fees = {
    applicants,
    visaFee,
    totalVisaFee: calculateTotalVisaFee(visaFee, visaInfo.numberOfVisa),
    govermentVisaFee: calculateTotalGovermentFee(applicants),
    processFee: calculateProcessFee(visaInfo.processType, masterData)
  };
  fees.totalFees = fees.totalVisaFee + fees.govermentVisaFee + fees.processFee;
  return fees;
}

//private methods
function validate(order) {
  return false;
}
function getVisaFee(masterData) {
  const visaFee = masterData.find(item => item.enumKey === KEYS.VISA_TYPE);
  return visaFee.value;
}
export function calculateTotalVisaFee(visaFee, members) {
  return visaFee * members;
}
function calculateGovernmentFee(country, masterData) {
  const GOVERMENT_FEES = masterData.filter(item => item.enumKey === KEYS.GOVERNMENT_TYPE);
  const coutryFee = _.find(GOVERMENT_FEES, fee => fee.key === country) || {};
  return coutryFee.value || 0;
}
function calculateTotalGovermentFee(applicants) {
  return _.sumBy(applicants, 'govermentFee')
}
function calculateProcessFee(processType, masterData) {
  const processFee = masterData.find(item => item.enumKey === KEYS.PROCESS_TYPES && item.key === processType);
  if (!processFee){
    return 0;
  }
  return processFee.value;
}
