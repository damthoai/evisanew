import path from 'path';
import _ from 'lodash';
import fs from 'fs';
import moment from 'moment';
import db from '../../database';
import { ORDER_STATUS, KEYS, ROLES, ADMIN_ID } from '../../constants';
import { render as renderTemplate, exportPdf } from '../../templates';
import { sendMail } from '../../Mailer';
import config from '../../config/enviroment';
import logger from '../../logger';
import * as OrderModel from './model';
import * as EmailModel from '../email/model';
import rapid from 'eway-rapid';
import eWayProvider from '../payment/providers/eWay';
import restify from 'restify';
import * as MasterDataModel from '../masterList/model';
import * as Utils from '../../utils';
import { APIContracts, APIControllers, Constants } from 'authorizenet';
import authorizenetProvider from '../payment/providers/authorizenet';

// ===============OrderList=======================
export function OrderList(req, res, next) {
  const { limit, page } = req.query;
  const user = req.dbUser || {};
  const userId = user.id;
  const userRole = user.role;
  if(userRole === ROLES.ADMIN){
    return OrderModel.getAllByAdmin(limit, page)
      .then((orders) => {
        const result = normalizeOrders(orders);
        res.send(200, result);
      })
      .catch((error) => handleError(error, next));
  }
  return OrderModel.getAllByUserId(userId, limit, page)
    .then((orders) => {
      const result = normalizeOrders(orders);
      res.send(200, result);
    })
    .catch((error) => handleError(error, next));
}

function normalizeOrders(orders){
  return _.map(orders, order => normalizeOrder(order));
}
function normalizeOrder(order){
  if (!_.isObjectLike(order.applicants)){
    order.applicants = JSON.parse(order.applicants);
  }
  if (_.isEmpty(order.question2)) {
    order.question2 = 'No';
  }
  if (_.isEmpty(order.question3)) {
    order.question3 = 'No';
  }
  if (_.isEmpty(order.question4)) {
    order.question4 = 'No';
  }
  if (_.isEmpty(order.question5)) {
    order.question5 = 'No';
  }
  return _.omit(order, ['userAgent', 'ipClient']);
}
// ===============CheckStatus======================
export function CheckStatus(req, res, next) {
  const model = req.body;
  OrderModel.getByEmail(model)
    .then(result => res.send(200, normalizeOrders(result)))
    .catch((error) => handleError(error, next));
  setTimeout(sendCheckStatusEmail, 0, model);
}
// ===============OrderDetail======================
export function OrderDetail(req, res, next) {
  const order_id = req.params.id;
  let userId = (req.dbUser || {}).id || null; // set null to consistent with DB data
  if (!req.dbUser) {
    userId = req.query.userId || null; // set null to consistent with DB data
  }
  OrderModel.getById(order_id)
    .then((order) => {
      if(userId !== ADMIN_ID) {
        if (_.isEmpty(order) || (order || {}).userId !== userId) {
          return next(new restify.UnprocessableEntityError("order does not exist"));
        }
      }
      res.send(200, normalizeOrder(order));
    })
    .catch((error) => handleError(error, next));
}
// ================CreateOrder=====================
export function CreateOrder(req, res, next) {
  try {
    const { id } = req.dbUser || {};
    OrderModel.insert(req.body, id, req.headers['user-agent'], req.headers['x-real-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress)
      .then((order) => {
        res.send(200, normalizeOrder(order));

        logger.log('debug', 'wait sendOrderEmail');
        setTimeout(sendOrderEmail, 0, order);
      })
      .catch(error => handleError(error, next));
  } catch (error) {
    handleError(error, next);
  }
}
// ================UpdateOrder======================
export function updateOrderStatus(req, res, next) {
  const user = req.dbUser;
  if (!user || (user && user.role !== ROLES.ADMIN)) {
    return next(new restify.ForbiddenError('You dont have permission.'));
  }
  const orderId = req.body.orderId;
  OrderModel.getById(orderId)
    .then((order) => {
      if (!order) {
        return next(new restify.UnprocessableEntityError('order doest not exist'));
      }
      const orderStatus = req.body.orderStatus;
      if(orderStatus === ORDER_STATUS.PROCESSING || orderStatus === ORDER_STATUS.FINISH){
        order.orderStatus = orderStatus;
        return OrderModel.updateStatus(order).then(()=>{
          res.send(200, normalizeOrder(order));
        }).catch(error => handleError(error, next));
      } else {
        return next(new restify.UnprocessableEntityError('order update un-successful'));
      }
    }).catch(error => handleError(error, next));
}
export function UpdateOrder(req, res, next) {
  try {
    const userId = (req.dbUser || {}).id;
    const orderId = req.body.orderId;
    OrderModel.getById(orderId)
      .then((order) => {
        if (!_.isEmpty(order.userId) && userId !== order.userId) {
          return next(new restify.UnprocessableEntityError('order doest not exist'));
        }
        if (order.orderStatus !== ORDER_STATUS.PENDING) {
          return next(new restify.UnprocessableEntityError('Cannot update order as it had complete or processing'));
        }
        OrderModel.update(req.body, order.userId, req.headers['user-agent'], req.headers['x-real-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress)
          .then((order) => {
            res.send(200, normalizeOrder(order));

            logger.log('debug', 'wait sendOrderEmail');
            setTimeout(sendOrderEmail, 0, order);
          })
          .catch((error) => handleError(error, next));
      })
      .catch((error) => handleError(error, next));
  } catch (error) {
    handleError(error, next);
  }
}
// ================payOrder=========================
export function payOrder(req, res, next) {
  try {
    const userId = (req.dbUser || {}).id;
    const orderId = req.body.orderId;
    OrderModel.getById(orderId)
      .then((order) => {
        if (!_.isEmpty(order.userId) && userId !== order.userId) {
          return next(new restify.UnprocessableEntityError('order doest not exist'));
        }
        if (order.orderStatus !== ORDER_STATUS.PENDING) {
          return next(new restify.UnprocessableEntityError('Cannot update order as it had complete or processing'));
        }

        MasterDataModel.getMasterList()
          .then(masterData => {
            order.firstName = req.body.firstName;
            order.lastName = req.body.lastName;
            order.country = req.body.country;
            order.address = req.body.address;
            order.city = req.body.city;
            order.state = req.body.state;
            order.postCode = req.body.postCode;
            order.paymentType = req.body.paymentType;
            order.applicants = req.body.applicants;

            // calculate fees
            calculateFees(order, masterData);
            const { cardNumber, expiryDate, cvv2 } = req.body;

            if (!validate(order, cardNumber, expiryDate, cvv2)){
              return next(new restify.UnprocessableEntityError('order data is invalid'));
            }
            sendPayment(order, cardNumber, expiryDate, cvv2, masterData)
              .then((response) => {
                order.cardNumber = Utils.maskCardNumber(cardNumber);
                order.transactionId = response.transactionId;
                order.transactionDate = response.transactionDate;
                order.orderStatus = ORDER_STATUS.PAID;
                order.paymentErrors = response.errors;
                if (order.paymentErrors) {
                  order.orderStatus = ORDER_STATUS.CANCELED_PAYMENT;
                }
                // log transaction
                OrderModel.logTransaction(order.paymentId, order.transactionId, order.transactionDate, response.rawRequest, response.rawResponse);
                // update order billing info, card info and transaction status
                OrderModel.updatePayment(order, order.userId, req.headers['user-agent'], req.headers['x-real-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress)
                  .then(() => res.send(200, normalizeOrder(order)))
                  .catch(error => handleError(error, next));
                // send email
                sendPaymentEmail(order, masterData);
              });
          })
          .catch(error => handleError(error, next));
      })
      .catch((error) => handleError(error, next));
  } catch (error) {
    handleError(error, next);
  }
}

// private methods
function validate(order, cardNumber, expiryDate, cvv2){
  if (!_.isNumber(+order.visaFee) || !_.isNumber(+order.numberOfVisa) || !_.isNumber(+order.processingFee) || !_.isNumber(+order.govermentVisaFee)){
    return false;
  }
  if (_.isEmpty(cardNumber) || _.isEmpty(expiryDate) || _.isEmpty(cvv2)){
    return false;
  }
  return true;
}

function storeBackupMail(order, type, rawEmail, html, callback) {
  const data = {
    type,
    userEmail: order.contactEmail,
    dateSend: order.sendDate,
    orderId: order.orderId,
    html,
    emailFile: `${order.orderId}-${Date.now()}.email`
  };
  const MAIL_DIR = `${__dirname}/../../mails/`;
  if (!fs.existsSync(MAIL_DIR)) {
    fs.mkdirSync(MAIL_DIR);
  }
  fs.writeFile(path.resolve(`${MAIL_DIR}${data.emailFile}`), rawEmail, (err) => {
    if (err) {
      if (callback) {
        callback(err)
      } else {
        logger.log('error', '%s', err.stack || err.toString());
      }
    } else {
      EmailModel.insert(data, (error) => {
        if (error) {
          if (callback) {
            callback(error)
          } else {
            logger.log('error', '%s', error.stack || error.toString());
          }
        } else if (callback) {
          callback();
        }
      });
    }
  });
}
// ================================================
function preparePaymenteWayModel(order, cardNumber, expiryDate, cvv2, masterData) {
  const processTypeText = getProcessTypeText(masterData, order.processType);
  const model = {
    Customer: {
      FirstName: order.firstName,
      LastName: order.lastName,
      Street1: order.address,
      City: order.city,
      State: order.state,
      PostalCode: order.postCode,
      Country: order.country,
      Phone: order.contactPhone,
      Email: order.contactEmail,
      CardDetails: {
        Number: order.cardNumber,
        ExpiryMonth: order.expiryDate.substr(0, 2),
        ExpiryYear: order.expiryDate.substr(2),
        CVN: order.cvv2
      }
    },
    Items: [
      {
        SKU: '1',
        Description: "Visa Service Fee",
        Quantity: order.numberOfVisa,
        UnitCost: order.visaFee,
        Tax: 0,
        Total: order.visaFee * order.numberOfVisa
      },
      {
        SKU: '2',
        Description: _.truncate(`Processing: ${processTypeText}`, 26),
        Quantity: 1,
        UnitCost: order.processFee,
        Tax: 0,
        Total: order.processFee
      },
      {
        SKU: '3',
        Description: _.truncate(`Government fee`, 26),
        Quantity: 1,
        UnitCost: order.govermentVisaFee,
        Tax: 0,
        Total: order.govermentFee
      }
    ],
    Payment: {
      TotalAmount: order.totalFees,
      InvoiceNumber: String(order.orderId),
      InvoiceDescription: `Payment for Order ID ${order.orderId}`,
      CurrencyCode: "USD"
    },
    Method: "ProcessPayment",
    TransactionType: "Purchase",
    CustomerIP: order.ipClient,
    Capture: true
  }
  return model;
}
function preparePaymentAuthorizeNetModel(order, cardNumber, expiryDate, cvv2, masterData){
    const processTypeText = getProcessTypeText(masterData, order.processType);
    // card info
    const creditCard = new APIContracts.CreditCardType();
    creditCard.setCardNumber(cardNumber);
    creditCard.setExpirationDate(expiryDate);
    creditCard.setCardCode(cvv2);

    // card type
    const paymentType = new APIContracts.PaymentType();
    paymentType.setCreditCard(creditCard);

    // billing info
    const billTo = new APIContracts.CustomerAddressType();
    billTo.setFirstName(order.firstName);
    billTo.setLastName(order.lastName);
    billTo.setCompany('');
    billTo.setAddress(order.address);
    billTo.setCity(order.city);
    billTo.setState(order.state);
    billTo.setZip(order.postCode);
    billTo.setCountry(order.country);

    // order detail
    const orderDetails = new APIContracts.OrderType();
    orderDetails.setInvoiceNumber(order.orderId);
    orderDetails.setDescription(`Payment for Order ID ${order.orderId}`);

    const visaFee = new APIContracts.LineItemType();
    visaFee.setItemId('1');
    visaFee.setName('Visa Service Fee');
    visaFee.setDescription('Visa Service Fee');
    visaFee.setQuantity(String(order.numberOfVisa));
    visaFee.setUnitPrice(Number(order.visaFee).toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 }));

    const processingFee = new APIContracts.LineItemType();
    processingFee.setItemId('2');
    processingFee.setName(_.truncate(`Processing: ${processTypeText}`, 31));
    processingFee.setDescription(`Processing: ${processTypeText}`);
    processingFee.setQuantity('1');
    processingFee.setUnitPrice(Number(order.processFee).toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 }));

    const govermentFee = new APIContracts.LineItemType();
    govermentFee.setItemId('3');
    govermentFee.setName('Government fee');
    govermentFee.setDescription('Government fee');
    govermentFee.setQuantity('1');
    govermentFee.setUnitPrice(Number(order.govermentVisaFee).toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 }));

    const lineItemList = [];
    lineItemList.push(visaFee);
    lineItemList.push(processingFee);
    lineItemList.push(govermentFee);

    const lineItems = new APIContracts.ArrayOfLineItem();
    lineItems.setLineItem(lineItemList);

    // transaction request type
    const transactionRequestType = new APIContracts.TransactionRequestType();
    transactionRequestType.setTransactionType(APIContracts.TransactionTypeEnum.AUTHCAPTURETRANSACTION);
    transactionRequestType.setPayment(paymentType);
    transactionRequestType.setOrder(orderDetails);
    transactionRequestType.setLineItems(lineItems);
    transactionRequestType.setAmount(Number(order.totalFees).toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 }));
    transactionRequestType.setBillTo(billTo);
    return transactionRequestType;
}
function sendPayment(order, cardNumber, expiryDate, cvv2, masterData) {
  if (config.payment[config.payment.active] === config.payment.eWay){
    const orderModel = preparePaymenteWayModel(order, cardNumber, expiryDate, cvv2, masterData);
    const client = new eWayProvider(config.payment.eWay);
    return client.send(orderModel);
  } else {
    const orderModel = preparePaymentAuthorizeNetModel(order, cardNumber, expiryDate, cvv2, masterData);
    const client = new authorizenetProvider(config.payment.authorizeNet);
    return client.send(orderModel);
  }
}
function sendPaymentEmail(order, masterData) {
  const mailModel = buildMailModel(order, masterData);
  mailModel.transactionDateText = moment(new Date(mailModel.transactionDate)).format('LLL');
  mailModel.cvv2 = 'xxx';
  mailModel.cardExpireDate = 'xx/xx';
  const template = mailModel.orderStatus === ORDER_STATUS.PAID ? 'payment-success' : 'payment-fail';
  // html email body
  renderTemplate(template, mailModel, (err, htmlBody) => {
    if (err) {
      return handleError(err, Utils.noop);
    }

    if (mailModel.orderStatus === ORDER_STATUS.PAID) {
      // attach invoice pdf
      const pdfTemplate = 'Invoiced_Paid';
      exportPdf(pdfTemplate, mailModel, ($err, $buffer) => {
        if ($err) {
          handleError($err, Utils.noop);
        } else {
          // prepare message to send
          const message = {
            to: mailModel.contactEmail,
            from: "Evisa4u Pty Ltd trading as indiaonlineevisa.com",
            body: htmlBody,
            subject: `Order Number #${mailModel.orderId} Payment Successfully.`,
            files: [{
              mimeType: 'Application/pdf',
              fileName: `Invoice_Order_${mailModel.orderId}.pdf`,
              bytes: $buffer
            }]
          };
          sendMail(message, (err, rawEmail) => {
            if (err) {
              handleError(err, Utils.noop);
            } else {
              const action = 'performPaymentOrder';
              storeBackupMail(mailModel, action, rawEmail, htmlBody);
            }
          });
        }
      });
    } else {
      const message = {
        to: order.contactEmail,
        from: "Evisa4u Pty Ltd trading as indiaonlineevisa.com",
        body: htmlBody,
        subject: `Order Number #${order.orderId} Failed Payment`,
        files: []
      };
      sendMail(message, (err, rawEmail) => {
        if (err) {
          handleError(err, Utils.noop);
        } else {
          const action = 'performPayment';
          storeBackupMail(mailModel, action, rawEmail, htmlBody);
        }
      });
    }
  });
}
// ================================================
function buildMailModel(order, masterData) {
  const $order = _.cloneDeep(order);
  $order.website = _.replace(config.domain, new RegExp('https://|http://'), '');
  $order.visaType = getVisaTypeText(masterData, $order.typeOfVisa);
  $order.transportationMethod = getArrivalPortType($order.arrivalPortType, masterData);
  $order.portOfArrivalText = getPortOfArrivalText(masterData, $order.portOfArrival);
  $order.processTypeText = getProcessTypeText(masterData, $order.processType);
  $order.sendDate = $order.orderDate;
  $order.orderDate = moment(new Date($order.orderDate)).format('LLL');
  $order.dateOfArrival = moment(new Date($order.dateOfArrival)).format('LL');
  $order.dateOfDeparture = moment(new Date($order.dateOfDeparture)).format('LL');

  const applicants = $order.applicants;
  _.forEach(applicants, applicant => {
    applicant.dateOfBirth = moment(new Date($order.dateOfBirth)).format('LL');
  });
  const paymentToken = new Buffer(JSON.stringify({ orderId: $order.orderId, userId: $order.userId })).toString('base64');
  $order.paymentLink = `${config.domain}/apply-visa/step/3?token=${paymentToken}`;
  return $order;
}
function sendOrderEmail(order) {
  try {
    MasterDataModel.getMasterList().then(masterData => {
      const mailModel = buildMailModel(order, masterData);
      const template = 'createOrder';
      // build html body
      renderTemplate(template, mailModel, (err, htmlBody) => {
        if (err) {
          handleError(err, Utils.noop);
        } else {
          const message = {
            to: mailModel.contactEmail,
            from: config.emailFrom,
            body: htmlBody,
            subject: `Notification of Order Number #${mailModel.orderId}`,
            files: []
          };
          // send an email
          logger.log('debug', 'send order email');
          sendMail(message, ($error, rawEmail) => {
            if ($error) {
              handleError($error, Utils.noop);
            } else {
              const action = _.isEmpty(mailModel.orderId) ? 'createOrder' : 'updateOrder';
              // store backup email
              logger.log('debug', 'store backup email');
              storeBackupMail(mailModel, action, rawEmail, htmlBody, (error) => {
                if (error) {
                  handleError(error, Utils.noop);
                }
              });
              logger.log('debug', `wait sendRemindOrderEmail`);
              setTimeout(sendRemindOrderEmail, config.timeToResend, mailModel);
            }
          });
        }
      });
    }).catch(error => handleError(error, Utils.noop));
  } catch (error) {
    handleError(error, Utils.noop);
  }
}
function sendRemindOrderEmail(mailModel) {
  try {
    OrderModel.getById(mailModel.orderId)
      .then((order) => {
        if (order.version === mailModel.version && order.orderStatus === ORDER_STATUS.PENDING) {
          // send remind
          const template = 'remindOrder';
          // build html body
          renderTemplate(template, mailModel, (err, htmlBody) => {
            if (err) {
              handleError(err, Utils.noop);
            } else {
              const pdfTemplate = 'Invoiced_Unpaid';
              exportPdf(pdfTemplate, mailModel, ($err, $buffer) => {
                if ($err) {
                  handleError($err, Utils.noop);
                } else {
                  const message = {
                    to: mailModel.contactEmail,
                    from: config.emailFrom,
                    body: htmlBody,
                    subject: 'India Visa Application Order Reminder.',
                    files: [{
                      mimeType: 'Application/pdf',
                      fileName: `Invoice_Order_${mailModel.orderId}.pdf`,
                      bytes: $buffer
                    }]
                  };
                  // send an email
                  sendMail(message, ($error, rawEmail) => {
                    if ($error) {
                      handleError($error, Utils.noop);
                    } else {
                      const action = 'remindOrder';
                      // store backup email
                      storeBackupMail(mailModel, action, rawEmail, htmlBody, (error) => {
                        if (error) {
                          handleError(error, Utils.noop);
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      })
      .catch((error) => handleError(error, Utils.noop));
  } catch (error) {
    handleError(error, Utils.noop);
  }
}
// ================================================
function sendCheckStatusEmail(mailModel) {
  try {
    const template = 'checkStatusOrder';
    renderTemplate(template, mailModel, (err, htmlBody) => {
      const message = {
        to: config.emailFrom,
        from: config.emailFrom,
        body: htmlBody,
        subject: `Check status of ID Number #${mailModel.contactName}`,
        files: []
      };
      sendMail(message, (err, rawEmail) => {
        if (err) {
          logger.log('error', '%s', err.stack || err.toString());
        }
      });
    });
  } catch (err) {
    logger.log('error', '%s', err.stack || err.toString());
  }
}
// ================================================
function calculateFees(order, masterData) {
  const fees = OrderModel.calculateFees(order, masterData);
  order.visaFee = fees.visaFee;
  order.processFee = fees.processFee;
  order.govermentVisaFee = fees.govermentVisaFee;
  order.totalVisaFee = fees.totalVisaFee;
  order.totalFees = fees.totalFees;
}
function getVisaTypeText(masterData, typeOfVisa) {
  const TYPE_OF_CARDS = masterData.filter(item => item.enumKey === KEYS.TYPE_OF_CARDS);
  return (TYPE_OF_CARDS.find(item => item.key === typeOfVisa) || {}).detail;
}
function getProcessTypeText(masterData, processType) {
  const PROCESS_TYPES = masterData.filter(item => item.enumKey === KEYS.PROCESS_TYPES);
  return (PROCESS_TYPES.find(item => item.key === processType) || {}).detail;
}
function getArrivalPortType(portType, masterData) {
  const ARRIVAL_PORT_TYPE = masterData.filter(item => item.enumKey === KEYS.ARRIVAL_PORT_TYPE);
  return (ARRIVAL_PORT_TYPE.find(item => item.key === portType) || {}).detail;
}
function getPortOfArrivalText(masterData, portOfArrival) {
  const PORT_OF_AIRPORT = masterData.filter(item => item.enumKey === KEYS.PORT_OF_AIRPORT);
  const PORT_OF_SEAPORT = masterData.filter(item => item.enumKey === KEYS.PORT_OF_SEAPORT);
  const PORT_ARRIVAL = [...PORT_OF_AIRPORT, ...PORT_OF_SEAPORT];
  return (PORT_ARRIVAL.find(item => item.key === portOfArrival) || {}).detail;
}
function handleError(error, next) {
  logger.log('error', error.stack || error.toString());
  next(error);
}