import db from '../../database';

export function getAll(callback) {
  db.query('SELECT * FROM t_email' , callback);
}

export function filterLike(name,value,callback) {
  db.query('SELECT * FROM t_email where ? like ?', [name,`%${value}%`] ,callback);
}

export function insert(email, callback) {
  db.query('INSERT INTO t_email set ?' , email, callback);
}