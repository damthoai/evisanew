import * as controller from './controller';
import { Router } from 'restify-router';

const router = new Router();

router.get({path: '/', version: '0.0.1'}, controller.EmailList);
router.get({path: '/filter', version: '0.0.1'}, controller.EmailFilter);

export default router;