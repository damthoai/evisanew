import * as EmailModel from './model';

export function EmailList(req, res, next) {
  EmailModel.getAll(function (err, result) {
    if (err) {
      return next(err);
    } else {
      res.send(200, result);
    }
  });
}
export function EmailFilter(req, res, next) {
  const name = req.query.name;
  const value = req.query.value;
  EmailModel.filterLike(name,value,function (err, result) {
    if (err) {
      return next(err);
    } else {
      res.send(200, result);
    }
  });
}
