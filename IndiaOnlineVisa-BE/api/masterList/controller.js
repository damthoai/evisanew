import * as MasterListModel from './model';
import request from 'request';

export function MasterList(req, res, next) {
  MasterListModel.getMasterList().then(result => res.send(200, result)).catch(next);
}

export function GetExchangeRate(req, res, next) {
  MasterListModel.getExchangeRate().then(rate => res.send(200, rate)).catch(next);
}

export function EditMasterItem(req, res, next) {
  MasterListModel.updateItem(req.body.value, req.body.detail, req.params.id).then(() => res.send(204)).catch(next);
}
