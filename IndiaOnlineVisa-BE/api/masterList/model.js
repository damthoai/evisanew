import db from '../../database';
import _ from 'lodash';
import request from 'request';
import moment from 'moment';

let masterList = [];
let rate = {};

export function getMasterList() {
  if (!_.isEmpty(masterList)) {
    return Promise.resolve(_.cloneDeep(masterList));
  } else {
    return db.queryAsync('SELECT * FROM t_master_list').then((result) => {
      masterList = result;
      return _.cloneDeep(masterList);
    });
  }
}
export function getMasterListByEnumKey(enumKey, callback) {
  getMasterList((err, items) => {
    if (err) {
      return callback(err);
    }
    const $items = _.filter(items, item => { return item.enumKey === enumKey; });
    callback(null, $items);
  });
}
export function updateItem(value, detail, id, callback) {
  const $item = _.find(masterList, item => { return item.id == id; });
  if (_.isEmpty($item)) {
    return Promise.reject(new Error('item is null'));
  }
  $item.value = value;
  $item.detail = detail;
  return db.queryAsync("UPDATE t_master_list set ? WHERE id = ?", [_.omit($item, 'id'), id]);
}
export function getExchangeRate() {
  return new Promise((resolve, reject)=>{
    if (!rate.base || rate.date !== moment.utc().format('YYYY-MM-DD')) {
      request.get('http://api.fixer.io/latest?base=USD&symbols=AUD', (error, response, body) => {
        if (error) {
          reject(error);
        } else {
          rate = JSON.parse(body);
          resolve(rate);
        }
      });
    } else {
      resolve(rate);
    }
  });
}