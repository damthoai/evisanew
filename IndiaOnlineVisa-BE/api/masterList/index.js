import * as controller from './controller';
import { Router } from 'restify-router';

const router = new Router();

router.get({path: '/', version: '0.0.1'}, controller.MasterList);
router.get({path: '/exchange-rate', version: '0.0.1'}, controller.GetExchangeRate);
router.put({path: '/:id', version: '0.0.1'}, controller.EditMasterItem);

export default router;