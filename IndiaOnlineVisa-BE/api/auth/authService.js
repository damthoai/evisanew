import * as Utils from '../../utils';
import * as UserModel from '../user/model';
import _ from 'lodash';
import restify from 'restify';
import jwt from 'jsonwebtoken';
import config from '../../config/enviroment';
import logger from '../../logger';

function extractAccessCode(req) {
  let access_token = req.body && req.body.access_token || req.query && req.query.access_token || req.headers.authorization;
  if (!_.isEmpty(access_token)) {
    access_token = _.replace(access_token, 'Bearer ', '');
  }
  return access_token;
}
export function signToken(userId, email, role) {
  return jwt.sign({
    userId: String(userId),
    email: email,
    role: role
  }, config.secrets.session, { expiresIn: config.secrets.sessionExpiresIn });
};

export function verifyToken(access_token, callback){
  jwt.verify(access_token, config.secrets.session, callback);
}

export function getUserInfo(req, res, next) {
  const access_token = extractAccessCode(req);
  const hasAccessToken = !_.isEmpty(access_token);
  if (!hasAccessToken) {
    return next();
  }
  verifyToken(access_token, (error, decodedToken) => {
    if (error) {
      logger.log('error', error.stack || error.toString());
      return next(new restify.UnauthorizedError('you are not authorized'));
    }
    if (_.isEmpty(decodedToken)) {
      return next();
    }

    UserModel.findUser(decodedToken.userId, decodedToken.email).then(user => {
      req.dbUser = user;
      next();
    }).catch(error => next());
  });
}
export function validateAuthentication(req, res, next) {
  const access_token = extractAccessCode(req);
  verifyToken(access_token, (error, decodedToken) => {
    if (error) {
      logger.log('error', error.stack || error.toString());
      return next(new restify.UnauthorizedError('you are not authorized'));
    }
    if (_.isEmpty(decodedToken)) {
      return next(new restify.UnauthorizedError('you are not authorized'));
    }
    UserModel.findUser(decodedToken.userId, decodedToken.email).then(user => {
      if (_.isEmpty(user)) {
        return next(new restify.UnprocessableEntityError('user does not exist.'));
      }
      req.dbUser = user;
      return next();
    }).catch(next);
  });
}
