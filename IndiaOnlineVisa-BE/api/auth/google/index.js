import * as controller from './controller';
import { Router } from 'restify-router';

const router = new Router();
router.get({path: '/', version: '0.0.1'}, controller.saveUrlReturn , controller.login);
router.get({path: '/callback', version: '0.0.1'}, controller.login, controller.GoogleCallBack);

export default router;