import passport from 'passport';
import GoogleStrategy from 'passport-google-oauth20';
import config from '../../../config/enviroment';
import logger from '../../../logger';
import {signToken} from '../authService';
import * as UserModel from '../../user/model';
import _ from 'lodash';
import { ftruncateSync } from 'fs';

passport.use(new GoogleStrategy({
  clientID: config.google.clientID,
  clientSecret: config.google.clientSecret,
  callbackURL: config.google.callbackURL
}, (accessToken, refreshToken, profile, done) => {
  UserModel.findGoogleAccount(profile.emails[0].value).then((googleUsers) => {
    const googleUser = _.first(googleUsers);
    if (googleUser) {
      done(null, googleUser);
    } else {
      UserModel.insertGoogleAccount(accessToken, refreshToken, profile).then(user => done(null, user)).catch(done);
    }
  }).catch(done);
}));

export function GoogleCallBack(req, res) {
  logger.log(req);
  if (!req.user) {
    return req.send(422, { code: RESPONSE_CODE.LOGIN_FAIL, message: 'It looks like you aren\'t logged in, please try again.' });
  }
  const access_token = signToken(req.user.id, req.user.email, req.user.role);
  var body = `<html><body><script>
      const CURRENT_REQUEST_KEY = '__torii_request';
      const pendingRequestKey = window.localStorage.getItem(CURRENT_REQUEST_KEY);
      if (pendingRequestKey) {
        window.localStorage.removeItem(CURRENT_REQUEST_KEY);
        var url = window.location.toString() + '&access_token=${access_token}&expireIn=${config.secrets.sessionExpiresIn}&role=${req.user.role}';
        window.localStorage.setItem(pendingRequestKey, url);
      }
      window.close();
  </script></body></html>`;
  res.writeHead(200, {
    'Content-Length': Buffer.byteLength(body),
    'Content-Type': 'text/html'
  });
  res.write(body);
  res.end();
};
export function login(req, res, next) {
  logger.log('login with google');
  passport.authenticate('google', { scope: ['email', 'profile'], session: false })(req, res, next);
};
export function saveUrlReturn(req, res, next){
  if (req.query.return) {
    req.session.oauth2return = req.query.return;
  }
  next();
};
export function finishOauth(){
  passport.authenticate('google')
};
