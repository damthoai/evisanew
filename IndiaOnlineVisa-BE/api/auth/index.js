import { Router } from 'restify-router';
import GoogleRouter from './google';
import FacebookRouter from './facebook';
import LocalRouter from './local';

const router = new Router();

router.add('/', LocalRouter);
router.add('/google', GoogleRouter);
router.add('/facebook', FacebookRouter);

export default router;