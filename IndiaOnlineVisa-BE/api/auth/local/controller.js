import restify from 'restify';
import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import {signToken} from '../authService';
import config from '../../../config/enviroment';
import * as UserModel from '../../user/model';
import { RESPONSE_CODE } from '../../../constants';
import _ from 'lodash';

passport.use(new LocalStrategy((email, password, done) => localAuthenticate(email, password, done)));

//private
function localAuthenticate(email, password, done) {
  UserModel.login(email, password).then((user)=> {
    if (_.isEmpty(user)) {
      return done(new restify.UnprocessableEntityError({ statusCode: 422 , restCode: RESPONSE_CODE.LOGIN_FAIL, message: 'email or password is incorrect' }));
    }
    return done(null, user);
  }).catch(done);
};

// public
export function login(req, res, next) {
  passport.authenticate('local', function (error, user, info) {
    if (error) {
      return next(error);
    }
    if (info) {
      return res.send(422, info);
    }
    const access_token = signToken(user.id, user.email, user.role);
    res.send(200, { access_token, expiresInSecs: config.secrets.sessionExpiresIn, role: user.role });
  })(req, res, next);
};