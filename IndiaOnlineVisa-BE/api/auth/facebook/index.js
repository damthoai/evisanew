import * as controller from './controller';
import { Router } from 'restify-router';

const router = new Router();

// router.post({path: '/', version: '0.0.1'}, controller.login);
router.get({path: '/callback', version: '0.0.1'}, controller.login, controller.FbCallBack);

export default router;