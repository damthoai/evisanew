import passport from 'passport';
import FacebookStrategy from 'passport-facebook';
import config from '../../../config/enviroment';
import logger from '../../../logger';
import {signToken} from '../authService';
import * as UserModel from '../../user/model';
import _ from 'lodash';

passport.use(new FacebookStrategy({
  clientID: config.facebook.clientID,
  clientSecret: config.facebook.clientSecret,
  callbackURL: config.facebook.callbackURL,
  profileFields: ['id', 'displayName', 'photos', 'email']
}, (accessToken, refreshToken, profile, done) => {
  console.log('accessToken: '+ accessToken);
  console.log('refreshToken: '+ refreshToken);
  console.log('profile: '+ JSON.stringify(profile));
  UserModel.findFacebookAccount(profile.emails[0].value).then((facebookUsers) => {
    const facebookUser = _.first(facebookUsers);
    if (facebookUser) {
      done(null, facebookUser);
    } else {
      UserModel.insertFacebookAccount(accessToken, refreshToken, profile).then(user => done(null, user)).catch(done);
    }
  }).catch(done);
}));

export function FbCallBack(req, res) {
  if (!req.user) {
    return req.send(422, { code: RESPONSE_CODE.LOGIN_FAIL, message: 'It looks like you aren\'t logged in, please try again.' });
  }
  const access_token = signToken(req.user.id, req.user.email, req.user.role);
  var body = `<html><body><script>
      const CURRENT_REQUEST_KEY = '__torii_request';
      const pendingRequestKey = window.localStorage.getItem(CURRENT_REQUEST_KEY);
      if (pendingRequestKey) {
        window.localStorage.removeItem(CURRENT_REQUEST_KEY);
        var url = window.location.toString() + '&access_token=${access_token}&expireIn=${config.secrets.sessionExpiresIn}&role=${req.user.role}';
        window.localStorage.setItem(pendingRequestKey, url);
      }
      window.close();
  </script></body></html>`;
  res.writeHead(200, {
    'Content-Length': Buffer.byteLength(body),
    'Content-Type': 'text/html'
  });
  res.write(body);
  res.end();
}
export function login(req, res, next) {
  passport.authenticate('facebook', { session: false })(req, res, next);
};