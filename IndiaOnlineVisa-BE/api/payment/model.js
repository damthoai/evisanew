import db from '../../database';
import _ from 'lodash';
import * as MasterDataModel from '../masterList/model';

export function getAllByUser(userId, limit, page) {
  return db.queryAsync('SELECT * FROM t_payment WHERE userId = ? LIMIT ? OFFSET ?',
    [Number(userId || Number.MIN_SAFE_INTEGER), Number(limit), Number(page)]);
}
export function getAllByAdmin(limit, page) {
  return db.queryAsync('SELECT * FROM t_payment LIMIT ? OFFSET ?', [limit, page]);
}
export function getById(paymentId) {
  return db.queryAsync('SELECT * FROM t_payment WHERE paymentId = ?', [paymentId]).then((payments) => _.first(payments));
}
export function create(payment) {
  payment.version = Date.now().toString();
  return MasterDataModel.getExchangeRate(rate => {
    payment.rate = (rate.rates || {}).AUD || 1;
    return db.queryAsync("INSERT INTO t_payment set ?", payment).then(result => {
      payment.paymentId = result.insertId;
      return payment;
    });
  });
}
export function updateTransaction(payment) {
  payment.version = Date.now().toString();
  const sql = `
    UPDATE t_payment set
      paymentStatus = ?,
      transactionId = ?,
      transactionDate = ?
    WHERE paymentId = ?`;
  const values = [
    payment.paymentStatus,
    payment.transactionId,
    payment.transactionDate,
    payment.paymentId
  ];
  return db.queryAsync(sql, values);
}
export function logTransaction(paymentId, transactionId, transactionDate, request, response) {
  return db.queryAsync("INSERT INTO t_transaction_log set ? ", {
    transactionId,
    transactionDate,
    paymentId,
    request: JSON.stringify(request),
    response: JSON.stringify(response)
  });
}
