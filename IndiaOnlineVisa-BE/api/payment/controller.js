import * as Model from './model';
import { sendMail } from '../../Mailer';
import rapid from 'eway-rapid';
import eWayProvider from './providers/eWay';
import authorizenetProvider from './providers/authorizenet';
import { APIContracts, APIControllers, Constants } from 'authorizenet';
import config from '../../config/enviroment';
import logger from '../../logger';
import _ from 'lodash';
import * as Utils from '../../utils';
import {ROLES, PAYMENT_STATUS} from '../../constants';
import restify from 'restify';
import moment from 'moment';
import { render as renderTemplate } from '../../templates';
import fs from 'fs';
import path from 'path';
import * as EmailModel from '../email/model';

export function getAllPayments(req, res, next) {
  const { limit, page } = req.query;
  const user = req.dbUser || {};
  const userId = user.id;
  const userRole = user.role;
  if(userRole === ROLES.ADMIN){
    return Model.getAllByAdmin(+(limit || 10), +(page || 0))
      .then((payments) => {
        const result = normalizePayments(payments);
        res.send(200, result);
      })
      .catch((error) => handleError(error, next));
  }
  return Model.getAllByUser(userId, +(limit || 10), +(page || 0))
    .then((payments) => {
      const result = normalizePayments(payments);
      res.send(200, result);
    })
    .catch((error) => handleError(error, next));
}
function normalizePayments(payments){
  return _.map(payments, payment => normalizePayment(payment));
}
function normalizePayment(payment){
  return _.omit(payment, ['userAgent', 'ipClient']);
}

export function getPaymentDetail(req, res, next) {
  const paymentId = req.params.id;
  const userId = (req.dbUser || {}).id;
  const role = (req.dbUser || {}).role;
  Model.getById(paymentId)
    .then((payment) => {
      if (_.isEmpty(payment)) {
        return next(new restify.UnprocessableEntityError("payment does not exist"));
      }
      if (payment.userId !== userId && role !== ROLES.ADMIN) {
        return next(new restify.UnprocessableEntityError("payment does not exist"));
      }
      res.send(200, normalizePayment(payment));
    })
    .catch((error) => handleError(error, next));
}

export function createPayment(req, res, next) {
  const userId = (req.dbUser || {}).id;
  const { cardNumber, expiryDate, cvv2 } = req.body;
  const paymentData = {
    userId,
    paymentStatus: PAYMENT_STATUS.PENDING,
    contactEmail: req.body.contactEmail,
    paymentType: req.body.paymentType,
    userAgent: req.headers['user-agent'],
    ipClient: req.connection.remoteAddress,
    amount: req.body.amount,
    rate: req.body.rate,
    paymentFor: req.body.paymentFor,
    note: req.body.note
  };
  if (!validate(paymentData, cardNumber, expiryDate, cvv2)){
    return next(new restify.UnprocessableEntityError('payment data is invalid'));
  }
  Model.create(paymentData).then(payment => {
    logger.log('debug', 'perform payment');
    sendPayment(payment, cardNumber, expiryDate, cvv2).then((response) => {
      // encrypt cardNumber
      payment.cardNumber = Utils.maskCardNumber(cardNumber);
      payment.transactionId = response.transactionId;
      payment.transactionDate = response.transactionDate;
      payment.paymentStatus = PAYMENT_STATUS.PAID;
      payment.paymentErrors = response.errors;
      if (payment.paymentErrors) {
        payment.paymentStatus = PAYMENT_STATUS.CANCELED_PAYMENT;
      }
      // log transaction
      Model.logTransaction(payment.paymentId, payment.transactionId, payment.transactionDate, response.rawRequest, response.rawResponse);
      // update transaction
      Model.updateTransaction(payment).then(() => {
        res.send(200, normalizePayment(payment));

        logger.log('debug', 'send payment email');
        setTimeout(sendPaymentEmail, 0, payment);
      });
    });
  }).catch(next);
}

// privates
function validate(payment, cardNumber, expiryDate, cvv2){
  if (!_.isNumber(+payment.amount)){
    return false;
  }
  if (_.isEmpty(cardNumber) || _.isEmpty(expiryDate) || _.isEmpty(cvv2)){
    return false;
  }
  return true;
}
function sendPayment(payment, cardNumber, expiryDate, cvv2) {
  if (config.payment[config.payment.active] === config.payment.eWay){
    const paymentModel = preparePaymenteWayModel(payment, cardNumber, expiryDate, cvv2);
    const client = new eWayProvider(config.payment.eWay);
    return client.send(paymentModel);
  } else {
    const paymentModel = preparePaymentAuthorizeNetModel(payment, cardNumber, expiryDate, cvv2);
    const client = new authorizenetProvider(config.payment.authorizeNet);
    return client.send(paymentModel);
  }
}
function sendPaymentEmail(payment) {
  const template = 'make-payment-sucess';
  const mailModel = prepareMailModel(payment);
  renderTemplate(template, mailModel, (error, htmlBody) => {
    if (error) {
      logger.log('error', '%s', error.stack || error.toString());
    } else {
      const message = {
        to: mailModel.contactEmail,
        from: config.emailFrom,
        body: htmlBody,
        subject: `Payment Number #${mailModel.paymentId} paid Successfully`,
        files: []
      };
      sendMail(message, (err, rawEmail) => {
        if (err) {
          logger.log('error', '%s', err.stack || err.toString());
        } else {
          const action = 'performPayment';
          storeBackupMail(payment, action, rawEmail, htmlBody);
        }
      });
    }
  });
};
function prepareMailModel(payment) {
  const model = _.cloneDeep(payment);
  model.paymentDate = moment(new Date(payment.transactionDate)).format('LLL');
  return model;
}
function preparePaymenteWayModel(payment, cardNumber, expiryDate, cvv2) {
  const model = {
    Customer: {
      Email: payment.contactEmail,
      CardDetails: {
        Number: cardNumber,
        ExpiryMonth: expiryDate.substr(0, 2),
        ExpiryYear: expiryDate.substr(2),
        CVN: cvv2
      }
    },
    Items: [
      {
        SKU: '1',
        Description: `Pay for ${payment.paymentFor}`,
        Quantity: 1,
        UnitCost: payment.amount,
        Tax: 0,
        Total: payment.amount
      }
    ],
    Payment: {
      TotalAmount: payment.amount,
      InvoiceNumber: String(payment.paymentId),
      InvoiceDescription: `Pay for Payment #${payment.paymentId}`,
      CurrencyCode: "USD"
    },
    Method: "ProcessPayment",
    TransactionType: "Purchase",
    CustomerIP: payment.ipClient
  }
  return model;
}
function preparePaymentAuthorizeNetModel(payment, cardNumber, expiryDate, cvv2) {
  // card info
  const creditCard = new APIContracts.CreditCardType();
  creditCard.setCardNumber(cardNumber);
  creditCard.setExpirationDate(expiryDate);
  creditCard.setCardCode(cvv2);

  // card type
  const paymentType = new APIContracts.PaymentType();
  paymentType.setCreditCard(creditCard);

  // payment detail
  const paymentDetails = new APIContracts.OrderType();
  paymentDetails.setInvoiceNumber(String(payment.paymentId));
  paymentDetails.setDescription(payment.visaType);

  const visaFee = new APIContracts.LineItemType();
  visaFee.setItemId('1');
  visaFee.setName(`Pay for ${payment.paymentFor}`);
  visaFee.setDescription(`Pay for ${payment.paymentFor}`);
  visaFee.setQuantity(1);
  visaFee.setUnitPrice(payment.amount.toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 }));

  const lineItemList = [];
  lineItemList.push(visaFee);

  const lineItems = new APIContracts.ArrayOfLineItem();
  lineItems.setLineItem(lineItemList);

  // transaction request type
  const transactionRequestType = new APIContracts.TransactionRequestType();
  transactionRequestType.setTransactionType(APIContracts.TransactionTypeEnum.AUTHCAPTURETRANSACTION);
  transactionRequestType.setPayment(paymentType);
  transactionRequestType.setOrder(paymentDetails);
  transactionRequestType.setLineItems(lineItems);
  transactionRequestType.setAmount(payment.amount.toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 }));
  return transactionRequestType;
}
function handleError(error, next) {
  logger.log('error', error.stack || error.toString());
  next(error);
}
function storeBackupMail(payment, type, rawEmail, html, callback) {
  const data = {
    type,
    userEmail: payment.contactEmail,
    dateSend: new Date(payment.transactionDate),
    paymentId: payment.paymentId,
    html,
    emailFile: `${payment.paymentId}-${Date.now()}.email`
  };
  const MAIL_DIR = `${__dirname}/../../mails/`;
  if (!fs.existsSync(MAIL_DIR)) {
    fs.mkdirSync(MAIL_DIR);
  }
  const $callback = callback || Utils.noop;
  fs.writeFileSync(path.resolve(`${MAIL_DIR}${data.emailFile}`), rawEmail, (err) => {
    if (err) {
      logger.log('error', '%s', err.stack || err.toString());
      return $callback(err);
    }
    return EmailModel.insert(data, (error) => {
      if (error) {
        logger.log('error', '%s', error.stack || error.toString());
        $callback(err);
      } else {
        $callback();
      }
    });
  });
}