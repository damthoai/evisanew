import * as controller from './controller';
import { Router } from 'restify-router';
import {getUserInfo} from '../auth/authService';

const router = new Router();

router.post({path: '/', version: '0.0.1'}, getUserInfo, controller.createPayment);
router.get({path: '/', version: '0.0.1'}, getUserInfo, controller.getAllPayments);
router.get({path: '/:id', version: '0.0.1'}, getUserInfo, controller.getPaymentDetail);

export default router;