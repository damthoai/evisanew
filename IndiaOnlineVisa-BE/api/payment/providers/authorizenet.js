import Base from './base';
import { APIContracts, APIControllers, Constants } from 'authorizenet';
import moment from 'moment';
import _ from 'lodash';

export default class AuthorizeNet extends Base {
  constructor(credentials) {
    super();
    if (!credentials) {
      throw new ErrorConstructor('credentials is null');
    }
    if (!credentials.API_LOGIN_ID || !credentials.TRANSACTION_KEY || !credentials.END_POINT) {
      throw new ErrorConstructor('API_LOGIN_ID or TRANSACTION_KEY or END_POINT is not setup');
    }
    this.credentials = credentials;
  }
  send(transactionRequestType) {
    const createRequest = new APIContracts.CreateTransactionRequest();
    const merchantAuthenticationType = new APIContracts.MerchantAuthenticationType();
    merchantAuthenticationType.setName(this.credentials.API_LOGIN_ID);
    merchantAuthenticationType.setTransactionKey(this.credentials.TRANSACTION_KEY);
    createRequest.setMerchantAuthentication(merchantAuthenticationType);
    createRequest.setTransactionRequest(transactionRequestType);
    const request = createRequest.getJSON();
    const ctrl = new APIControllers.CreateTransactionController(request);
    ctrl.setEnvironment(this.credentials.END_POINT);
    return new Promise(resolve => {
      ctrl.execute(function () {
        const apiResponse = ctrl.getResponse();
        const response = {
          transactionDate: moment.utc().format('YYYY-MM-DD HH:mm:ss'),
          transactionId: null,
          errors: null,
          rawRequest: request,
          rawResponse: new APIContracts.CreateTransactionResponse(apiResponse)
        };
        if (!_.isEmpty(response.rawResponse)) {
          if (response.rawResponse.getMessages().getResultCode() == APIContracts.MessageTypeEnum.OK) {
            if (response.rawResponse.getTransactionResponse().getMessages() != null) {
              response.transactionId = response.rawResponse.getTransactionResponse().getTransId();
            } else {
              if (response.rawResponse.getTransactionResponse().getErrors() != null) {
                response.errors = [{
                  code: response.rawResponse.getTransactionResponse().getErrors().getError()[0].getErrorCode(),
                  message: response.rawResponse.getTransactionResponse().getErrors().getError()[0].getErrorText()
                }];
              }
            }
          } else {
            if (response.rawResponse.getTransactionResponse() != null && response.rawResponse.getTransactionResponse().getErrors() != null) {
              response.errors = [{
                code: response.rawResponse.getTransactionResponse().getErrors().getError()[0].getErrorCode(),
                message: response.rawResponse.getTransactionResponse().getErrors().getError()[0].getErrorText()
              }];
            }
            else {
              response.errors = [{
                code: response.rawResponse.getMessages().getMessage()[0].getCode(),
                message: response.rawResponse.getMessages().getMessage()[0].getText()
              }];
            }
          }
        }
        resolve(response);
      });
    })
  }
}