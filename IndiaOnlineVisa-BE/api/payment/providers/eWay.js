import Base from './base';
import eWayProvider from 'eway-rapid';

export default class eWay extends Base {
  constructor(credentials) {
    super();
    if (!credentials) {
      throw new ErrorConstructor('credentials is null');
    }
    if (!credentials.API_KEY || !credentials.Password || !credentials.END_POINT) {
      throw new ErrorConstructor('API_KEY or Password or END_POINT is not setup');
    }
    this.client = eWayProvider.createClient(credentials.API_KEY, credentials.Password, credentials.END_POINT);
  }
  send(payment) {
    const response = {
      transactionDate: moment.utc().format('YYYY-MM-DD HH:mm:ss'),
      transactionId: null,
      errors: null,
      rawRequest: payment,
      rawResponse: null
    };
    return new Promise((resolve) => {
      this.client.createTransaction(eWayProvider.Enum.Method.DIRECT, model)
        .then((responseTransaction) => {
          response.rawResponse = responseTransaction;
          response.transactionId = responseTransaction.get('TransactionID');
          if (responseTransaction.get('TransactionStatus') === false) {
            response.errors = [responseTransaction.get('ResponseMessage')];
          }
          resolve(response);
        }).catch(error => {
          response.errors = extractPaymentErrors(error);
          resolve(response);
        });
    });
  }
}
function extractPaymentErrors(reasonFail) {
  return reasonFail.getErrors().map((error) => eWayProvider.getMessage(error, "en"));
}
