import config from '../config/enviroment';
import OrderRouter from './order';
import UserRouter from './user';
import AuthRouter from './auth';
import MasterRouter from './masterList';
import SettingRouter from './setting';
import EmailRouter from './email';
import ContentRouter from './content';
import PaymentRouter from './payment';
import ValidvisaRouter from './valid_visa';

export function createRouters(app) {
  AuthRouter.applyRoutes(app, `${config.namespace}/auth`);
  OrderRouter.applyRoutes(app, `${config.namespace}/order`);
  UserRouter.applyRoutes(app, `${config.namespace}/user`);
  MasterRouter.applyRoutes(app, `${config.namespace}/master-list`);
  SettingRouter.applyRoutes(app, `${config.namespace}/setting`);
  EmailRouter.applyRoutes(app, `${config.namespace}/email`);
  ContentRouter.applyRoutes(app, `${config.namespace}/content`);
  PaymentRouter.applyRoutes(app, `${config.namespace}/payment`);
  ValidvisaRouter.applyRoutes(app, `${config.namespace}/valid_visa`);
};