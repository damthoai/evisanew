import * as controller from './controller';
import { Router } from 'restify-router';

const router = new Router();

router.get({path: '/', version: '0.0.1'}, controller.SettingList);
router.put({path: '/:id', version: '0.0.1'}, controller.EditSettingItem);

export default router;