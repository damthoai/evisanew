import * as MasterList from '../masterList/model';
import { KEYS } from '../../constants';

export function SettingList(req, res, next) {
  MasterList.getMasterListByEnumKey(KEYS.SOCIAL, function (err, result) {
    if (err) {
      return next(err);
    }
    res.send(200, result);
  });
}

export function EditSettingItem(req, res, next) {
  const id = req.params.id;
  MasterList.updateItem('', req.body.url, req.params.id, function (err, result) {
    if (err) {
      return next(err);
    }
    res.send(200);
  });
}
