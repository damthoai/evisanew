import restify from 'restify';
import restifylogger from 'restify-logger';
import passport from 'passport';
import config from './config/enviroment';
import logger from './logger';
import { createRouters } from './api';

const app = restify.createServer({
  name: config.appName
});

app.use(restifylogger('dev'));
app.use(restify.queryParser());
app.use(restify.bodyParser());
app.use(restify.CORS());
app.use(passport.initialize());
app.use(passport.session());


app.listen(config.port, config.ip, function () {
  logger.log('info', '%s listening at %s ', app.name, app.url);
});

createRouters(app);