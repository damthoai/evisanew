const STEP_STATUS = {
  INFO: 1,
  DETAIL_APPLICANT: 2,
  PAYMENT: 3,
  THANKYOU: 4
}
const RELIOUS = [
  'Bahai', 'Buddhism', 'Chirstian', 'Hindu', 'Islam', 'Jainism', 'Judaism', 'Parsi', 'Sikh', 'Zoroastrian', 'Other'
];
const EDUCATIONS = [
  'High school', 'Graduated - bachelor', 'Post graduated', 'Below matriculation', 'Others'
];
const PAYMENT_TYPES = {
  VISA: 'VISA',
  MASTERCARD: 'MASTERCARD'
}
const KEYS = {
  SOCIAL: 'SOCIAL',
  ORDER_STATUS: 'ORDER_STATUS',
  GENDER: 'GENDER',
  TYPE_OF_CARDS: 'TYPE_OF_CARDS',
  ARRIVAL_PORT_TYPE: 'ARRIVAL_PORT_TYPE',
  PORT_OF_AIRPORT: 'PORT_OF_AIRPORT',
  PORT_OF_SEAPORT: 'PORT_OF_SEAPORT',
  GOVERNMENT_TYPE: 'GOVERNMENT_TYPE',
  PROCESS_TYPES: 'PROCESS_TYPE',
  VISA_TYPE: 'VISA_TYPE'
}
const PROCESS_TYPE = {
  NORMAL: '1',
  URGENT: '2',
  SUPER_URGENT: '3'
}
const PORT_TYPES = {
  Airport: '1',
  Seaport: '2'
}
const GlobalDateFormat = 'yyyy-mm-dd';
const VISA_ACTIONS = {
  PAYNOW: 'PAYNOW',
  CHANGETOPROCESSING: 'CHANGETOPROCESSING',
  CHANGETOFINISH: 'CHANGETOFINISH'
}
const ORDER_STATUS = {
  PENDING: "PENDING",
  PAID: "PAID",
  CANCELED_PAYMENT: "CANCELED_PAYMENT",
  PROCESSING: "PROCESSING",
  FINISH: "FINISH"
}
const ORDER_STATUS_TEXT = {
  PENDING: "PENDING",
  PAID: "PAID",
  CANCELED_PAYMENT: "CANCELED PAYMENT",
  PROCESSING: "PROCESSING",
  FINISH: "FINISH"
}
const ROLES = {
  ADMIN: 'ADMIN',
  MEMBER: 'MEMBER',
  GUEST: 'GUEST'
}

const BACKSPACE = 8;
const DELETE = 46;
const CLEAR = 12;
const TAB = 9;
const ESC = 27;

const NUMBER0 = 48;
const NUMBER1 = 49;
const NUMBER2 = 50;
const NUMBER3 = 51;
const NUMBER4 = 52;
const NUMBER5 = 53;
const NUMBER6 = 54;
const NUMBER7 = 55;
const NUMBER8 = 56;
const NUMBER9 = 57;
const NUMBERS = [NUMBER0, NUMBER1, NUMBER2, NUMBER3, NUMBER4, NUMBER5, NUMBER6, NUMBER7, NUMBER8, NUMBER9];

const leftArrow = 37;
const upArrow = 37;
const rightArrow = 39;
const downArrow = 40;

const KEY_BOARDS = {
  BACKSPACE,
  DELETE,
  CLEAR,
  TAB,
  ESC,
  NUMBER0,
  NUMBER1,
  NUMBER2,
  NUMBER3,
  NUMBER4,
  NUMBER5,
  NUMBER6,
  NUMBER7,
  NUMBER8,
  NUMBER9,
  NUMBERS,
  leftArrow,
  upArrow,
  rightArrow,
  downArrow
}
const PAYMENT_MODE = {
  VISA_CREDIT: 'Visa/Credit',
  BANK_ACCOUNT: 'Bank account'
}
export {
  STEP_STATUS,
  RELIOUS,
  EDUCATIONS,
  PAYMENT_TYPES,
  PORT_TYPES,
  PROCESS_TYPE,
  KEYS,
  VISA_ACTIONS,
  ORDER_STATUS,
  ORDER_STATUS_TEXT,
  GlobalDateFormat,
  ROLES,
  KEY_BOARDS,
  PAYMENT_MODE
};