import Ember from 'ember';
import DS from 'ember-data';
import {GlobalDateFormat} from '../constants';

const {inject}=Ember;

export default DS.Transform.extend({
  culture: inject.service(),
  deserialize(serialized) {
    let type = typeof serialized;

    if (type === "string") {
      const culture = this.get('culture');
      const date = culture.parseDate(serialized);
      return culture.formatDate(date);
    } else if (type === "number") {
      return new Date(serialized);
    } else if (serialized === null || serialized === undefined) {
      return serialized;
    } else {
      return null;
    }
  },

  serialize(date) {
    let type = typeof date;
    const culture = this.get('culture');
    if (type === "string") {
      date = culture.parseDate(date, $.fn.datepicker.defaults.format);
    }
    if (date instanceof Date && !isNaN(date)) {
      return culture.formatDate(date, GlobalDateFormat);
    } else {
      return null;
    }
  }
});