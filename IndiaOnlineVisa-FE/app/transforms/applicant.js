import Ember from 'ember';
import DS from 'ember-data';
import {GlobalDateFormat} from '../constants';

const {inject, get, set}=Ember;

export default DS.Transform.extend({
  culture: inject.service(),
  deserialize(applicants) {
    const culture = this.get('culture');
    let $applicants = applicants;
    if ($applicants instanceof String) {
      $applicants = JSON.parse($applicants);
    }
    _.each($applicants || [], (applicant) => {
      const date = culture.parseDate(get(applicant, "dateOfBirth"));
      if (date instanceof Date) {
        set(applicant, 'dateOfBirth', culture.formatDate(date));
      }
    });

    return $applicants;
  },

  serialize(applicants) {
    const culture = this.get('culture');
    let $applicants = applicants;
    if ($applicants instanceof String) {
      $applicants = JSON.parse($applicants);
    }
    _.each($applicants || [], (applicant) => {
      const d = culture.parseDate(get(applicant, "dateOfBirth"), $.fn.datepicker.defaults.format);
      if (d instanceof Date) {
        set(applicant, 'dateOfBirth', culture.formatDate(d, GlobalDateFormat));
      }
    });
    return $applicants;
  }
});