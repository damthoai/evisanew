import Ember from 'ember';

export default Ember.Component.extend({
  store: Ember.inject.service(),
  actions: {
    submit() {
      const { store, user } = this.getProperties('store', 'user');
      store.adapterFor('user').save(user)
        .then(()=>{
          Ember.set(this, 'done', true);
        })
        .catch(error=>{
          Ember.set(this, 'error', error);
        });
    }
  }
});
