import Ember from 'ember';

const { Component, Object, inject, computed } = Ember;

export default Component.extend({
  store: inject.service(),
  validationRules: {
    contactEmail: {
      email: true
    }
  },
  status: Object.create({}),
  sercurityCode: null,

  rawCode: computed(function() {
    return this.generateCode();
  }),

  isUpdate: computed('sercurityCode', 'rawCode', function() {
    return this.get('sercurityCode') === this.get('rawCode');
  }),

  actions: {
    checkStatus(status) {
      const isValid = this.$('.form-check-status').valid();
      if (isValid) {
        const store = this.get('store');
        store.adapterFor('order').checkStatus(status).then((orders) => {
          if (Ember.isArray(orders)) {
            const $orders = [];
            _.forEach(orders, order => {
              const $order = store.push(store.normalize('order', order));
              $orders.push($order);
            });
            this.set('table', $orders);
          }
        });
      }
    },
    refreshCode() {
      this.set('rawCode', this.generateCode());
    }
  },

  generateCode() {
    return Math.random().toString(36).slice(-8);
  },
});
