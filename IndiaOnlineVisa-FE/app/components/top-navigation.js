import Ember from 'ember';

const { Component, inject } = Ember;

export default Component.extend({
  session: inject.service(),
  actions: {
    logout() {
      const session = this.get('session');
      session.invalidate();
    }
  },
  didInsertElement() {
    this.openMenuHandler = this.openMenu.bind(this);
    this.$('button.navbar-toggle').on('click', this.openMenuHandler);
  },
  willDestroyElement() {
    this.$('button.navbar-toggle').off('click', this.openMenuHandler);
  },
  openMenu() {
    const $button = this.$('button.navbar-toggle');
    if ($button.hasClass('open')) {
      this.$('.navbar-collapse').removeClass('in');
      $button.html('<span class="sr-only">Toggle navigation</span><i class="fa fa-bars fa-2x"></i>').removeClass('open');
    } else {
      this.$('.navbar-collapse').addClass('in');
      $button.html('<span class="sr-only">Toggle navigation</span><img src="/assets/images/close.jpg" />').addClass('open');
    }
  }
});
