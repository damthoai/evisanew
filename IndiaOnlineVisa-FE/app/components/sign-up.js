import Ember from 'ember';

const {set, inject, isNone}=Ember;
export default Ember.Component.extend({
  store: inject.service(),
  session: inject.service(),
  routing: inject.service('-routing'),
  validationRules: {
    confirmPassword: {
      equalTo: '.signup-form [name=password]'
    }
  },
  didReceiveAttrs() {
    const store = this.get('store');
    set(this, 'user', store.createRecord('user'));
  },
  willDestroyElement() {
    const user = this.get('user');
    if(!isNone(user)) {
      user.unloadRecord();
    }
    set(this, 'user', null);
  },
  actions: {
    signup() { 
      const isValid = this.$('.signup-form').valid();
      if(isValid) {
        const { user, routing, session } = this.getProperties('user', 'routing', 'session');
        user.save()
        .then(()=> session.authenticate('authenticator:oauth2', user.get('email'), user.get('password')))
        .then(() => routing.router.transitionTo('account'))
        .catch(error => set(this,'error', error));
      }
    }
  }
});
