import Ember from 'ember';

const { inject, get, set } = Ember;

export default Ember.Component.extend({
  applyVisaService: inject.service('apply-visa'),
  didReceiveAttrs() {
    this._super(...arguments);
    const applyVisaService = get(this, 'applyVisaService');
    set(this, 'visaInfo', applyVisaService.visaInfo);
  }
});
