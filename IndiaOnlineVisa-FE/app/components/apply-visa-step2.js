import Ember from 'ember';
import { STEP_STATUS, KEY_BOARDS } from '../constants';

const { Component, computed, inject, set, isNone, get } = Ember;
const SUPER_URGENT_BUSINESS_DATE_ARRIVAL = 2;
const URGENT_BUSINESS_DATE_ARRIVAL = 5;
const BUSINESS_DATE_DEPARTURE = 60;

export default Component.extend({
  store: inject.service(),
  applyVisaService: inject.service('apply-visa'),
  routing: inject.service('-routing'),
  masterDataService: inject.service('master-data'),
  culture: inject.service(),
  validationRules: {
    contactEmail:{
      email: true
    },
    confirmEmail: {
      equalTo: '.apply-visa-form-step2 [name=contactEmail]'
    },
    dateOfArrival: {
      required: true,
      arrival: SUPER_URGENT_BUSINESS_DATE_ARRIVAL
    },
    dateOfDeparture: {
      required: true,
      departure: [BUSINESS_DATE_DEPARTURE, '[name=dateOfArrival]']
    }
  },
  showFlightNumber: computed('visaInfo.dateOfArrival', function() {
    const dateOfArrival = this.get('visaInfo.dateOfArrival');
    const culture = this.get('culture');
    const applyVisaService = this.get('applyVisaService');
    if (_.trim(dateOfArrival).length === 10) {
      let arrivalDate = culture.parseDate(dateOfArrival, $.fn.datepicker.defaults.format);
      arrivalDate = new Date(`${arrivalDate.getFullYear()}-${arrivalDate.getMonth() + 1}-${arrivalDate.getDate()}`);
      let today = new Date();
      today = new Date(`${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`);
      const isSuperUrgent = today <= arrivalDate && arrivalDate <= today.setDate(today.getDate() + SUPER_URGENT_BUSINESS_DATE_ARRIVAL);
      if (isSuperUrgent) {
        applyVisaService.changeToSuperUrgent();
      } else {
        today = new Date();
        const start = new Date(`${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`);
        const end = new Date(`${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`);
        const isUrgent = start.setDate(start.getDate() + SUPER_URGENT_BUSINESS_DATE_ARRIVAL) < arrivalDate && arrivalDate <= end.setDate(end.getDate() + URGENT_BUSINESS_DATE_ARRIVAL);
        if (isUrgent) {
          applyVisaService.changeToUrgent();
        } else {
          applyVisaService.changeToNormal();
        }
      }
      return isSuperUrgent;
    }
    applyVisaService.changeToNormal();
    return false;
  }),
  visaInfo: computed.alias('applyVisaService.visaInfo'),
  didReceiveAttrs() {
    this._super(...arguments);
    set(this, 'formatDate', _.toUpper($.fn.datepicker.defaults.format));
    const date = new Date();
    set(this, 'today', moment(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0)).format('LL'));
    jQuery.validator.addMethod('arrival', (value, element) => {
      let arrivalDate = $(element).data('datepicker').getDate();
      arrivalDate = new Date(`${arrivalDate.getFullYear()}-${arrivalDate.getMonth() + 1}-${arrivalDate.getDate()}`);
      let today = new Date();
      today = new Date(`${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`);
      const result = arrivalDate >= today;
      if (result === false) {
        this.$('.alert.dateOfArrival.arrival').show();
      } else {
        this.$('.alert.dateOfArrival.arrival').hide();
      }
      return result;
    }, SUPER_URGENT_BUSINESS_DATE_ARRIVAL);
    jQuery.validator.addMethod('departure', function(value, element, params) {
      let departureDate = $(element).data('datepicker').getDate();
      departureDate = new Date(`${departureDate.getFullYear()}-${departureDate.getMonth() + 1}-${departureDate.getDate()}`);
      let arrivalDate = $(params[1]).data('datepicker').getDate();
      arrivalDate = new Date(`${arrivalDate.getFullYear()}-${arrivalDate.getMonth() + 1}-${arrivalDate.getDate()}`);
      const dates = Math.round((departureDate-arrivalDate)/(1000*60*60*24));
      return dates >= 0 && dates <= params[0];
    }, [BUSINESS_DATE_DEPARTURE, '[name=dateOfArrival]']);
  },
  didInsertElement() {
    this.$('.thetooltip').tooltip();
  },
  willDestroyElement() {
    this.$('.thetooltip').tooltip('destroy');
  },
  GENDER: computed.alias('masterDataService.GENDER'),
  actions: {
    next() {
      const isValid = this.$('.apply-visa-form-step2').valid();
      if (isValid) {
        const visaInfo = this.get('visaInfo');
        visaInfo.save().then(() => {
          if (!isNone(get(visaInfo, 'orderId'))) {
            const routing = this.get('routing');
            routing.router.transitionTo('apply-visa', STEP_STATUS.PAYMENT);
          } else {
            set(this, 'error', {errors: [{detail: 'Cannot create Order. Please try again one more.'}]});
          }
        }).catch(errors => set(this, 'error', {errors}));
      }
    },
    prev() {
      const routing = this.get('routing');
      routing.router.transitionTo('apply-visa', STEP_STATUS.INFO);
    },
    keyDown(e) {
      if (e.keyCode !== KEY_BOARDS.BACKSPACE &&
        e.keyCode !== KEY_BOARDS.DELETE &&
        e.keyCode !== KEY_BOARDS.CLEAR &&
        e.keyCode !== KEY_BOARDS.TAB &&
        e.keyCode !== KEY_BOARDS.ESC &&
        e.keyCode !== KEY_BOARDS.leftArrow &&
        e.keyCode !== KEY_BOARDS.upArrow &&
        e.keyCode !== KEY_BOARDS.rightArrow &&
        e.keyCode !== KEY_BOARDS.downArrow) {
        if (KEY_BOARDS.NUMBERS.indexOf(e.keyCode) < 0) {
          e.preventDefault();
        }
      }
    },
    keyUp(e) {
      if (e.keyCode !== KEY_BOARDS.BACKSPACE &&
        e.keyCode !== KEY_BOARDS.DELETE &&
        e.keyCode !== KEY_BOARDS.CLEAR &&
        e.keyCode !== KEY_BOARDS.TAB &&
        e.keyCode !== KEY_BOARDS.ESC &&
        e.keyCode !== KEY_BOARDS.leftArrow &&
        e.keyCode !== KEY_BOARDS.upArrow &&
        e.keyCode !== KEY_BOARDS.rightArrow &&
        e.keyCode !== KEY_BOARDS.downArrow) {
        const val = $(e.target).val() || '';
        if (val.length === 2){
          $(e.target).val(val + "/");
        }else if (val.length === 5){
          $(e.target).val(val + "/");
        }
      }
    }
  }
});
