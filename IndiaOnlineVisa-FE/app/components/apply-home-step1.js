import Ember from 'ember';
import { STEP_STATUS, PORT_TYPES } from '../constants';

const { Component, computed, inject, get, set, A, isEmpty } = Ember;


export default Component.extend({
  applyVisaService: inject.service('apply-visa'),
  masterDataService: inject.service('master-data'),
  culture: inject.service('culture'),
  routing: inject.service('-routing'),
  visaInfo: computed.alias('applyVisaService.visaInfo'),
  didInsertElement() {
    this.$('.thetooltip').tooltip();
  },
  willDestroyElement() {
    this.$('.thetooltip').tooltip('destroy');
  },
  TYPE_OF_CARDS: computed.alias('masterDataService.TYPE_OF_CARDS'),
  ARRIVAL_PORT_TYPE: computed.alias('masterDataService.ARRIVAL_PORT_TYPE'),
  PORT_OF_AIRPORT: computed.alias('masterDataService.PORT_OF_AIRPORT'),
  PORT_OF_SEAPORT: computed.alias('masterDataService.PORT_OF_SEAPORT'),
  PortOfArrivals: computed('visaInfo.arrivalPortType', function() {
    const arrivalPortType = this.get('visaInfo.arrivalPortType');
    const PORT_OF_AIRPORT = this.get('PORT_OF_AIRPORT');
    const PORT_OF_SEAPORT = this.get('PORT_OF_SEAPORT');
    if (arrivalPortType === PORT_TYPES.Seaport) return PORT_OF_SEAPORT;
    return PORT_OF_AIRPORT;
  }),
  PROCESS_TYPES: computed.alias('masterDataService.PROCESS_TYPES'),
  actions: {
    apply() {
      const { routing, applyVisaService } = this.getProperties('routing', 'applyVisaService');
      const properties = applyVisaService.validate(STEP_STATUS.INFO);
      if (!isEmpty(properties)) {
        applyVisaService.createNewVisa(1)
      }
      const numberOfVisa = get(this, 'visaInfo.numberOfVisa');
      const applicants = get(this, 'visaInfo.applicants') || A([]);
      const length = get(applicants, 'length');
      if (length < numberOfVisa) {
        for(let index = length; index < numberOfVisa; index++) {
          applicants.pushObject(applyVisaService.createApplicant());
        }
      } else if (length > numberOfVisa) {
        for(let index = length - 1; index >= numberOfVisa; index--) {
          applicants.removeObject(applicants.objectAt(index));
        }
      }
      routing.router.transitionTo('apply-visa', STEP_STATUS.DETAIL_APPLICANT);
    },
    onProcessTypeChange(value) {
      set(this, 'visaInfo.processType', value);
      const PROCESS_TYPES = this.get('PROCESS_TYPES');
      const type = PROCESS_TYPES.find(item => get(item, 'key') === value) || {};
      set(this, 'visaInfo.processFee', get(type, 'value'));
    },
    onNumberOfVisaChange(value) {
      set(this, 'visaInfo.numberOfVisa', value);
      if (+value >= 10) {
        set(this, 'visaInfo.visaFee', 59); // FIX ME: hard code in here, will update later
      }
    }
  }
});
