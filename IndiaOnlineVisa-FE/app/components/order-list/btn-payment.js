import Ember from 'ember';
import { STEP_STATUS } from '../../constants';

const { Component, inject, set, computed } = Ember;

export default Component.extend({
  applyVisaService: inject.service('apply-visa'),
  masterDataService: inject.service('master-data'),
  routing: inject.service('-routing'),
  session: inject.service(),
  canUpdateStatus: computed.alias('session.isAdmin'),
  actions: {
    paymentNow() {
      const routing = this.get('routing');
      const applyVisaService = this.get('applyVisaService');
      const masterDataService = this.get('masterDataService');
      const visaInfo = this.get('row.content');
      set(visaInfo, 'dateOfArrival', undefined);
      set(visaInfo, 'dateOfDeparture', undefined);
      set(visaInfo, 'cardNumber', undefined);
      set(visaInfo, 'paymentType', undefined);
      set(visaInfo, 'expiryDate', undefined);
      set(visaInfo, 'cvv2', undefined);
      set(visaInfo, 'rate', masterDataService.get('RATE'));
      applyVisaService.setCurrentVisa(visaInfo);
      routing.router.transitionTo('apply-visa', STEP_STATUS.DETAIL_APPLICANT);
    }
  }
});
