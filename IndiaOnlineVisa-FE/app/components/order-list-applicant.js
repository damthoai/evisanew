import Ember from 'ember';
import Table from 'ember-light-table';

const { computed, inject } = Ember;

export default Ember.Component.extend({
  store: inject.service(),
  columns: computed(function() {
    return [{
      width: '40px',
      sortable: false,
      cellComponent: 'order-list/row-toggle',
      breakpoints: ['mobile', 'tablet']
    },{
      label: 'Surname',
      valuePath: 'surName',
      width: '150px',
    }, {
      label: 'Given Name',
      valuePath: 'givenName',
      width: '150px'
    }, {
      label: 'Gender',
      valuePath: 'gender',
      width: '200px',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }, {
      label: 'Birthday',
      valuePath: 'dateOfBirth',
      width: '150px',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }, {
      label: 'Nationality',
      valuePath: 'nationality',
      width: '150px',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    },
    {
      label: 'Passport',
      valuePath: 'passportNumber',
      width: '150px',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }];
  }),

  init() {
    this._super(...arguments);
    let data = this.get('data');

    //FIXME: please check again your model
    if(Ember.typeOf(this.get('data')) === 'string'){
      data = JSON.parse(`[${data}]`);
    }
    let table = new Table(this.get('columns'), data);
    this.set('table', table);
  },

  actions: {
    onAfterResponsiveChange(matches) {
      if (matches.indexOf('jumbo') > -1) {
        this.get('table.expandedRows').setEach('expanded', false);
      }
    }
  },


});
