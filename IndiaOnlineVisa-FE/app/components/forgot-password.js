import Ember from 'ember';

export default Ember.Component.extend({
  store: Ember.inject.service(),
  email: null,
  actions: {
    recoverPassword(){
      const isValid = this.$('.recover-password-form').valid();
      Ember.set(this, 'error', null);
      if (isValid) {
        const email = this.get('email');
        const store = this.get('store');
        store.adapterFor('user').recoverPassword(email).then(()=>{
          Ember.set(this, 'done', true);
        }).catch(error => {
          Ember.set(this, 'error', error);
        });
      }
    }
  }
});
