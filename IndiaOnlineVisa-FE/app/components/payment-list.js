import Ember from 'ember';
import Table from 'ember-light-table';
import { task } from 'ember-concurrency';

const { computed, isEmpty, inject } = Ember;

export default Ember.Component.extend({
  store: inject.service(),
  routing: inject.service('-routing'),
  columns: computed(function() {
    return [{
      width: '40px',
      sortable: false,
      cellComponent: 'payment-list/row-toggle',
      breakpoints: ['mobile', 'tablet']
    }, {
      label: 'ID',
      valuePath: 'paymentId',
      width: '150px',

    }, {
      label: 'Amount (USD)',
      valuePath: 'amount',
      width: '150px',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }, {
      label: 'Note',
      valuePath: 'note',
      width: '200px',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }, {
      label: 'Method',
      valuePath: 'paymentType',
      width: '150px',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }, {
      label: 'Insert Time',
      valuePath: 'createdDate',
      width: '150px',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }, {
      label: 'Status',
      valuePath: 'paymentStatus',
      width: '150px',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }];
  }),

  init() {
    this._super(...arguments);
    let table = new Table(this.get('columns'), this.get('data'), { enableSync: this.get('enableSync') });
    this.set('table', table);
  },

  page: 0,
  limit: 10,
  dir: 'asc',
  sort: 'orderId',

  isLoading: computed.oneWay('fetchRecords.isRunning'),
  canLoadMore: true,
  enableSync: true,

  meta: null,
  table: null,

  fetchRecords: task(function*() {
    let orders = yield this.get('store').query('payment', this.getProperties(['limit', 'page']));
    const orderLast = orders.content.get('lastObject');
    const dataLast = this.get('data').content.get('lastObject')

    if (orderLast === dataLast) {
      this.set('canLoadMore', true);
      return;
    }
    this.get('data').pushObject(orderLast);
    this.set('meta', orders.get('meta'));
    this.set('canLoadMore', !isEmpty(orders));
  }).restartable(),

  actions: {
    onAfterResponsiveChange(matches) {
      if (matches.indexOf('jumbo') > -1) {
        this.get('table.expandedRows').setEach('expanded', false);
      }
    },
    onColumnClick(column) {
      if (column.sorted) {
        this.setProperties({
          dir: column.ascending ? 'asc' : 'desc',
          sort: column.get('valuePath'),
          canLoadMore: true,
          page: 0
        });
        this.get('data').clear();
      }
    },
    onScrolledToBottom() {
      if (this.get('canLoadMore')) {
        this.incrementProperty('page');
        this.get('fetchRecords').perform();
      }
    },
    onRowDoubleClick(row) {
      const paymentId = row.get('id');
      const router = this.get('routing.router');
      const routePath = this.get('routePath');
      router.transitionTo(routePath, paymentId);
    }
  },
});
