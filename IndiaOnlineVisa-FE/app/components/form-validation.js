import Ember from 'ember';

const {computed}=Ember;

export default Ember.Component.extend({
  tagName: 'form',
  attributeBindings: ['autocomplete', 'name'],
  autocomplete: 'off',
  name: computed.alias('elementId'),
  didInsertElement() {
    const that = this;
    const options = {
      submitHandler(form, evt) {
        evt.preventDefault();
      },
      invalidHandler(evt, validator) {
        const errors = validator.numberOfInvalids();
        that.$('.alert').hide();
        if (errors) {
          const selectorErrors = _.map(validator.errorList, error => `.alert.${$(error.element).prop('name')}.${error.method}`).join(', ');
          that.$(selectorErrors).show();
        }
      },
      errorPlacement() {}
    };
    const { rules, messages } = this.getProperties('rules', 'messages');
    if (!Ember.isNone(rules)) {
      options.rules = rules;
    }
    if (!Ember.isNone(messages)) {
      options.messages = messages;
    }
    that.formValidator = that.$().validate(options);
  },
  willDestroyElement() {
    if (!Ember.isNone(this.formValidator)) {
      this.formValidator.destroy();
      this.formValidator = null;
    }
  }
});
