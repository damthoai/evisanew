import Ember from 'ember';

const { inject, computed } = Ember;

export default Ember.Component.extend({
  tagName: '',
  masterDataService: inject.service('master-data'),
  nationalities: computed.alias('masterDataService.GOVERMENT_FEES')
});
