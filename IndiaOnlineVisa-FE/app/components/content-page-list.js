import Ember from 'ember';
import Table from 'ember-light-table';

const { computed, isEmpty, inject } = Ember;
export default Ember.Component.extend({
  store: inject.service(),
  notify: inject.service(),
  init() {
    this._super(...arguments);
    let table = new Table(this.get('columns'), this.get('data'));
    this.set('table', table);
  },

  columns: computed(function() {
    return [{
      width: '40px',
      sortable: false,
      cellComponent: 'order-list/row-toggle',
      breakpoints: ['mobile', 'tablet']
    }, {
      label: 'Path to HTML',
      valuePath: 'path',
      width: '200px'
    }, {
      label: 'Title',
      valuePath: 'title',
      width: '200px',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }]
  }),

  actions: {
    onAfterResponsiveChange(matches) {
      if (matches.indexOf('jumbo') > -1) {
        this.get('table.expandedRows').setEach('expanded', false);
      }
    },
    onRowClick(row) {
      const item = this.get('store').peekRecord('content-page', row.get('id'));
      this.set('item', item);
    },
    update() {
      this.get('item').save().then(() => {
        this.get('notify').info('Update Complete');
      });
    }
  },

  isUpdated: computed('item.{path,title}', function() {
    const item = this.get('item');
    if (!isEmpty(item)) {
      return item.get('url') && item.get('title');
    }
    return false;
  })
});
