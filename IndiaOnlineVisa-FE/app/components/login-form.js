import Ember from 'ember';

const {inject, set}=Ember;

export default Ember.Component.extend({
  session: inject.service(),
  routing: inject.service('-routing'),
  ajax: inject.service(),
  email: null,
  password: null,
  actions: {
    login() {
      const isValid = this.$('.login-form').valid();
      if(isValid) {
        const { routing, session } = this.getProperties('routing', 'session');
        session.authenticate('authenticator:oauth2', this.get('email'), this.get('password'))
        .then(() => routing.router.transitionTo('account'))
        .catch(error => set(this,'error', error));
      }
    },
    loginFacebook() {
      const {session, routing} = this.getProperties('session', 'routing');
      session.authenticate('authenticator:facebook-torii', 'facebook-oauth2-custom').then(()=>routing.router.transitionTo('account'));
    },
    loginGoogle() {
      const {session, routing} = this.getProperties('session', 'routing');
      //session.authenticate('authenticator:google-torii', 'google-oauth2-custom').then(()=>routing.router.transitionTo('account'));
      this.get('session').authenticate('authenticator:google-torii', 'google-oauth2');
    }
  }
});
