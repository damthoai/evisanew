import Ember from 'ember';
import Table from 'ember-light-table';

const { computed, isEmpty, inject } = Ember;

export default Ember.Component.extend({
  store: inject.service(),
  notify: inject.service(),
  routing: inject.service('-routing'),
  table: null,
  init() {
    this._super(...arguments);
    let table = new Table(this.get('columnsFormat'), this.get('data'));
    this.set('table', table);
  },
  actions: {
    onAfterResponsiveChange(matches) {
      if (matches.indexOf('jumbo') > -1) {
        this.get('table.expandedRows').setEach('expanded', false);
      }
    },
    onRowClick(row) {
      const item = this.get('store').peekRecord('master-list', row.get('id'));
      this.set('item', item);
    },
    update() {
      const isValid = this.$('.form').valid();
      if (isValid) {
        this.get('item').save().then(() => {
          this.get('notify').info('Update Complete');
        });
      }
    }
  },

  isUpdated: computed('item.{key,detail,value}', function() {
    const item = this.get('item');
    if (!isEmpty(item)) {
      return item.get('key') && item.get('detail') && item.get('value');
    }
    return false;
  })
});
