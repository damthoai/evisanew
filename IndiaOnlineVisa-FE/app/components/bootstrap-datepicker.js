import Ember from 'ember';
import { KEY_BOARDS } from '../constants';

export default Ember.TextField.extend({
  didInsertElement() {
    this.keyDownHandler = this.keyDown.bind(this);
    this.keyUpHandler = this.keyUp.bind(this);
    this.$().on('keydown', this.keyDownHandler).on('keyup', this.keyUpHandler);
    this.$().datepicker({
      clearBtn: true,
      zIndexOffset: 100000
    });
  },
  willDestroyElement() {
    this.$().off('keydown', this.keyDownHandler).off('keyup', this.keyUpHandler);
    this.$().datepicker('destroy');
  },
  keyDown(e) {
    if (e.keyCode !== KEY_BOARDS.BACKSPACE &&
      e.keyCode !== KEY_BOARDS.DELETE &&
      e.keyCode !== KEY_BOARDS.CLEAR &&
      e.keyCode !== KEY_BOARDS.TAB &&
      e.keyCode !== KEY_BOARDS.ESC &&
      e.keyCode !== KEY_BOARDS.leftArrow &&
      e.keyCode !== KEY_BOARDS.upArrow &&
      e.keyCode !== KEY_BOARDS.rightArrow &&
      e.keyCode !== KEY_BOARDS.downArrow) {
      if (KEY_BOARDS.NUMBERS.indexOf(e.keyCode) < 0) {
        e.preventDefault();
      }
    }
  },
  keyUp(e) {
    if (e.keyCode !== KEY_BOARDS.BACKSPACE &&
      e.keyCode !== KEY_BOARDS.DELETE &&
      e.keyCode !== KEY_BOARDS.CLEAR &&
      e.keyCode !== KEY_BOARDS.TAB &&
      e.keyCode !== KEY_BOARDS.ESC &&
      e.keyCode !== KEY_BOARDS.leftArrow &&
      e.keyCode !== KEY_BOARDS.upArrow &&
      e.keyCode !== KEY_BOARDS.rightArrow &&
      e.keyCode !== KEY_BOARDS.downArrow) {
      const val = $(e.target).val() || '';
      if (val.length === 2){
        $(e.target).val(val + "/");
      }else if (val.length === 5){
        $(e.target).val(val + "/");
      }
    }
  }
});
