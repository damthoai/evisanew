import Ember from 'ember';
import { STEP_STATUS, VISA_ACTIONS, PAYMENT_MODE } from '../constants';

const { get, set, inject, computed } = Ember;

export default Ember.Component.extend({
  applyVisaService: inject.service('apply-visa'),
  routing: inject.service('-routing'),
  visaInfo: computed.alias('applyVisaService.visaInfo'),
  validationRules: {
    cardNumber: {
      required: true,
      cardnumber: [13,16]
    }
  },
  mode: PAYMENT_MODE.VISA_CREDIT,
  didReceiveAttrs() {
    this._super(...arguments);
    jQuery.validator.addMethod('cardnumber', (value, element, params) => {
      const $value = value || '';
      const num = parseInt($value, 10);
      if (!_.isNumber(num)) {
        return false;
      }
      return params[0] <= $value.length && $value.length <= params[1];
    }, [13,16]);    
    const currentYear = new Date().getFullYear();
    set(this, 'expiryYear', null);
    set(this, 'expirytMonth', null);
    set(this, 'expiryYears', [
      currentYear,
      currentYear + 1,
      currentYear + 2,
      currentYear + 3,
      currentYear + 4,
      currentYear + 5,
      currentYear + 6,
      currentYear + 7,
      currentYear + 8,
      currentYear + 9,
      currentYear + 10
    ]);
  },
  actions: {
    prev() {
      const routing = this.get('routing');
      routing.router.transitionTo('apply-visa', STEP_STATUS.DETAIL_APPLICANT);
    },
    payNow() {
      const isValid = this.$('.apply-visa-form-step3').valid();
      if (isValid) {
        const visaInfo = this.get('visaInfo');
        set(visaInfo, 'expiryDate', `${get(this, 'expirytMonth')}${(get(this, 'expiryYear')).substr(2)}`);
        const routing = this.get('routing');
        const adapterOptions = {
          action: VISA_ACTIONS.PAYNOW
        }
        visaInfo.save({adapterOptions})
        .then(() => routing.router.transitionTo('apply-visa', STEP_STATUS.THANKYOU))
        .catch(error => {
          // keep errors to show in thank you page
          set(visaInfo, 'errorMessage', error);
          routing.router.transitionTo('apply-visa', STEP_STATUS.THANKYOU);
        });
      }
    }
  }
});
