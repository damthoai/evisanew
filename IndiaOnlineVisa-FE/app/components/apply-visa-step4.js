import Ember from 'ember';

const { get, set, inject, isNone, computed } = Ember;

export default Ember.Component.extend({
  applyVisaService: inject.service('apply-visa'),
  visaInfo: computed.alias('applyVisaService.visaInfo'),
  didReceiveAttrs() {
    this._super(...arguments);
    const applyVisaService = get(this, 'applyVisaService');
    const visaInfo = get(applyVisaService, 'visaInfo');
    if(!isNone(visaInfo.paymentErrors)) {
      const errors = get(visaInfo, 'paymentErrors');
      set(this, 'errorMessage', errors);
    }
    set(this, 'contactName', get(visaInfo, 'contactName'));
    applyVisaService.createNewVisa();
  }
});
