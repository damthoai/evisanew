import Ember from 'ember';
import Table from 'ember-light-table';
import { task } from 'ember-concurrency';

const { Component, inject, computed, isEmpty } = Ember;

export default Component.extend({
  store: inject.service(),
  notify: inject.service(),
  routing: inject.service('-routing'),
  columns: computed(function() {
    return [{
      width: '40px',
      sortable: false,
      cellComponent: 'order-list/row-toggle',
      breakpoints: ['mobile', 'tablet']
    }, {
      label: 'Email',
      valuePath: 'email',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }, {
      label: 'Role',
      valuePath: 'role',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }, {
      label: 'Active',
      valuePath: 'isActived',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }];
  }),
  validationRules: {
    userEmail: {
      email: true
    }
  },
  init() {
    this._super(...arguments);
    let table = new Table(this.get('columns'), this.get('data'), { enableSync: this.get('enableSync') });
    this.set('table', table);
  },

  page: 0,
  limit: 10,
  dir: 'asc',
  sort: 'value',

  isLoading: computed.oneWay('fetchRecords.isRunning'),
  canLoadMore: true,
  enableSync: true,

  meta: null,
  table: null,

  fetchRecords: task(function*() {
    let orders = yield this.get('store').query('user', this.getProperties(['limit', 'page']));
    const orderLast = orders.content.get('lastObject');
    const dataLast = this.get('data').content.get('lastObject')

    if (orderLast === dataLast) {
      this.set('canLoadMore', true);
      return;
    }
    this.get('data').pushObject(orderLast);
    this.set('meta', orders.get('meta'));
    this.set('canLoadMore', !isEmpty(orders));
  }).restartable(),


  actions: {
    onAfterResponsiveChange(matches) {
      if (matches.indexOf('jumbo') > -1) {
        this.get('table.expandedRows').setEach('expanded', false);
      }
    },
    onScrolledToBottom() {
      if (this.get('canLoadMore')) {
        this.incrementProperty('page');
        this.get('fetchRecords').perform();
      }
    },
    onRowClick(row) {
      const item = this.get('store').peekRecord('user', row.get('id'));
      this.set('item', item);
    },

    update() {
      const isValid = this.$('.form').valid();
      if (isValid) {
        this.get('item').save().then(() => {
          this.get('notify').info('Update Complete');
        });
      }
    },

    active() {
      const isValid = this.$('.form').valid();
      if (isValid) {
        this.get('store').adapterFor('user').activeUser(this.get('item')).then(() => {
          this.get('notify').info('Update Complete');
        });
      }
    },

    deleted() {
      const isValid = this.$('.form').valid();
      if (isValid) {
        this.get('store').adapterFor('user').removeUser(this.get('item')).then(() => {
          this.get('notify').info('Update Complete');
        });
      }
    },
  },

  isDisabled: computed('item.{username,email,role}', function() {
    const item = this.get('item');
    if (!isEmpty(item)) {
      return item.get('username') && item.get('email') && item.get('role');
    }
    return false;
  })
});
