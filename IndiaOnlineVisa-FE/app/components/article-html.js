import Ember from 'ember';

const { isNone } = Ember;

export default Ember.Component.extend({
  didReceiveAttrs() {
    this._super(...arguments);
    this.loadArticle();
  },
  didInsertElement(){
    this.loadArticle();
  },
  loadArticle() {
    const article = this.get('article');
    const $element = this.$();
    if (!isNone($element)) {
      $element.load(`/assets/html/${article}.html`);
    }
  }
});
