import Ember from 'ember';
import Table from 'ember-light-table';

const { Component, inject, computed } = Ember;

export default Component.extend({
  store: inject.service(),
  routing: inject.service('-routing'),
  columns: computed(function() {
    return [{
      width: '40px',
      sortable: false,
      cellComponent: 'order-list/row-toggle',
      breakpoints: ['mobile', 'tablet']
    },{
      label: 'Type',
      valuePath: 'type',
      width: '150px',
    },{
      label: 'Order ID',
      valuePath: 'orderId',
      width: '100px',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }, {
      label: 'Client Email',
      valuePath: 'userEmail',
      width: '150px',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }, {
      label: 'Date of Sending',
      valuePath: 'dateSendText',
      width: '200px',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }];
  }),

  init() {
    this._super(...arguments);
    let table = new Table(this.get('columns'), this.get('data'));
    this.set('table', table);
  },
  table: null,
  query: '',
 selectedFilter: computed.oneWay('possibleFilters.1'),
  possibleFilters: computed('table.columns', function() {
    return this.get('table.columns').filterBy('sortable', true);
  }).readOnly(),

  actions: {
    onAfterResponsiveChange(matches) {
      if (matches.indexOf('jumbo') > -1) {
        this.get('table.expandedRows').setEach('expanded', false);
      }
    },
    onRowClick(row) {
      const html = row.get('html') || '';
      this.set('html', html);
    },
    search(){
      const valuePath = this.get('selectedFilter.valuePath');
      const query = this.get('query');
      const data = this.get('data');
      const rows = data.filter((m) => {
        return m.get(valuePath).toLowerCase().includes(query.toLowerCase());
      });
      this.get('table').setRows([]);
      this.get('table').setRows(rows);
    },
    onSearchChange(){
      this.set('query', '');
      this.get('table').setRows([]);
      this.get('table').setRows(this.get('data'));
    }
  }
});
