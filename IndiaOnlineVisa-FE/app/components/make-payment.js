import Ember from 'ember';

const { Component, inject, set, computed, isNone } = Ember;

export default Component.extend({
  store: inject.service(),
  masterDataService: inject.service('master-data'),
  validVisaService: inject.service('valid-visa'),
  routing: inject.service('-routing'),
  validationRules: {
    cardNumber: {
      required: true,
      cardnumber: [13, 16]
    }
  },
  validVisa: computed.alias('validVisaService.visaValid'),
  didReceiveAttrs() {
    this._super(...arguments);
    const store = this.get('store');
    set(this, 'payment', store.createRecord('payment', {
      rate: this.get('masterDataService.RATE')
    }));
    const validVisa = this.get('validVisa');
    if (!isNone(validVisa)) {
        set(this, 'payment.amount', validVisa.payment_box);
        set(this, 'payment.paymentFor' , validVisa.type + ' - ' + validVisa.id_number);
        set(this, 'payment.note', validVisa.id);
        set(this, 'payment.contactEmail', validVisa.email);
        
    };
    jQuery.validator.addMethod('cardnumber', (value, element, params) => {
      const $value = value || '';
      const num = parseInt($value, 10);
      if (!_.isNumber(num)) {
        return false;
      }
      return params[0] <= $value.length && $value.length <= params[1];
    }, [13, 16]);
    const currentYear = new Date().getFullYear();
    set(this, 'expiryYear', null);
    set(this, 'expirytMonth', null);
    set(this, 'expiryYears', [
      currentYear,
      currentYear + 1,
      currentYear + 2,
      currentYear + 3,
      currentYear + 4,
      currentYear + 5,
      currentYear + 6,
      currentYear + 7,
      currentYear + 8,
      currentYear + 9,
      currentYear + 10
    ]);
    set(this, 'paymentSuccess', false);
    set(this, 'errors', null);
  },
  actions: {
    processToPayment() {
      const isValid = this.$('.make-payment-form').valid();
      if (isValid) {
        const payment = this.get('payment');
        payment.set('expiryDate', `${this.get('expirytMonth')}${(this.get('expiryYear')).substr(2)}`);
        payment.save()
          .then(() => {
            set(this, 'paymentSuccess', true);
          })
          .catch(errors => {
            set(this, 'errors', errors);
          });
      }
    }
  }
});
