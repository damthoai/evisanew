import Ember from 'ember';
import { STEP_STATUS, ORDER_STATUS, VISA_ACTIONS } from '../constants';

const { inject, get, set, computed } = Ember;

export default Ember.Component.extend({
  applyVisaService: inject.service('apply-visa'),
  masterDataService: inject.service('master-data'),
  session: inject.service(),
  routing: inject.service('-routing'),
  canUpdateStatus: computed.alias('session.isAdmin'),
  actions: {
    paymentNow() {
      const routing = this.get('routing');
      const applyVisaService = this.get('applyVisaService');
      const masterDataService = this.get('masterDataService');
      const visaInfo = this.get('model');
      set(visaInfo, 'dateOfArrival', undefined);
      set(visaInfo, 'dateOfDeparture', undefined);
      set(visaInfo, 'cardNumber', undefined);
      set(visaInfo, 'paymentType', undefined);
      set(visaInfo, 'expiryDate', undefined);
      set(visaInfo, 'cvv2', undefined);
      set(visaInfo, 'rate', masterDataService.get('RATE'));
      applyVisaService.setCurrentVisa(visaInfo);
      routing.router.transitionTo('apply-visa', STEP_STATUS.DETAIL_APPLICANT);
    },
    changeToProcessing() {
      const visaInfo = this.get('model');
      const adapterOptions = {
        action: VISA_ACTIONS.CHANGETOPROCESSING
      };
      const orderStatus = get(visaInfo, 'orderStatus');
      set(visaInfo, 'orderStatus', ORDER_STATUS.PROCESSING);
      visaInfo.save({adapterOptions}).then(() => {
        alert('Update order status successful');
      }).catch(() => {
        alert('Update order status un-successful');
        set(visaInfo, 'orderStatus', orderStatus);
      });
    },
    changeToFinish() {
      const visaInfo = this.get('model');
      const adapterOptions = {
        action: VISA_ACTIONS.CHANGETOFINISH
      };
      const orderStatus = get(visaInfo, 'orderStatus');
      set(visaInfo, 'orderStatus', ORDER_STATUS.FINISH);
      visaInfo.save({adapterOptions}).then(() => {
        alert('Update order status successful');
      }).catch(() => {
        alert('Update order status un-successful');
        set(visaInfo, 'orderStatus', orderStatus);
      });
    }
  }
});
