import ApplyVisaStep1 from './apply-home-step1';
import { PROCESS_TYPE } from '../constants';

const { computed, get } = Ember;

export default ApplyVisaStep1.extend({
  PROCESS_TYPES: computed('masterDataService.PROCESS_TYPES', function() {
    const PROCESS_TYPES = this.get('masterDataService.PROCESS_TYPES');
    const isRushVisa = this.get('isRushVisa');
    if (isRushVisa) {
      return PROCESS_TYPES.filter(item=> get(item, 'key') === PROCESS_TYPE.SUPER_URGENT);
    }
    return PROCESS_TYPES;
  }),
});
