import Ember from 'ember';
const {set, Object, computed,isNone, inject } = Ember;

export default Ember.Component.extend({
  store: inject.service(),
  routing: inject.service('-routing'),
  validVisaService : inject.service('valid-visa'),
  validationRules: {
    Email: {
      email: true
    }
  },
  validationPickupRule:{
      Email:{
        email: true
      }
  },
  visaValid: Object.create({}),
  pickupVisa: Object.create({}),
  sercurityCode: null,
  sercurityCode1: null,
  isUpdate: computed('sercurityCode', 'rawCode1', function() {
    return (this.get('sercurityCode') === this.get('rawCode1') 
          && this.get('Email') !== ''
    );

  }),
  isUpdatePickup: computed('sercurityCode1', 'rawCode', function() {
    return ( this.get('sercurityCode1') === this.get('rawCode')
      && this.get('pickupEmail') !== ''  
  );
  }),

  actions: {
    checkStatus(visaValid) {
      const isValid = this.$('.form-check-valid-visa').valid();
      if (isValid) {
        const store = this.get('store');
        const validVisaService = this.get('validVisaService');
        visaValid.type = 'check-visa';
        visaValid.payment_box = '39';
        store.adapterFor('valid-visa').createRecord(visaValid).then((validvisa) => {
          if (!isNone(validvisa.id)) {
            validVisaService.createNewValidVisa(validvisa);
            this.resetForm(visaValid);
            const routing = this.get('routing');
            routing.router.transitionTo('make-payment');
          } else {
            set(this, 'error', {errors: [{detail: 'Cannot create valid visa. Please try again one more.'}]});
          }
        });
      }
    },
    pickupVisa(pickupVisa) {
        const isValid = this.$('.form-check-pickup-visa').valid();
          if (isValid) {
             const store = this.get('store');
            const validVisaService = this.get('validVisaService');
            pickupVisa.type = 'pickup-visa';
            pickupVisa.payment_box = '39';
            store.adapterFor('valid-visa').createRecord(pickupVisa).then((pickupVisa) => {
              if (!isNone(pickupVisa.id)) {
                validVisaService.createNewValidVisa(pickupVisa);
                this.resetForm(pickupVisa);
                const routing = this.get('routing');
                routing.router.transitionTo('make-payment');
              } else {
                set(this, 'error', {errors: [{detail: 'Cannot create valid visa. Please try again one more.'}]});
              }
            });
          
          }

    },
  
    refreshCode() {
      this.set('rawCode', this.generateCode());
    },
    refreshCode1() {
      this.set('rawCode1', this.generateCode1());
    },

  },


  rawCode: computed(function () {
    return this.generateCode();
  }),
  rawCode1: computed(function () {
    return this.generateCode1();
  }),

  resetForm(validVisa){
    validVisa.Email = '';
    validVisa.idNumber = '';
    validVisa.passportNumber = '';
  },

  generateCode() {
    return Math.random().toString(36).slice(-8);
  },
  generateCode1() {
    return Math.random().toString(24).slice(-8);
  }


});
