import Ember from 'ember';

export default Ember.Component.extend({
  store: Ember.inject.service(),
  validationRules: {
    confirmPassword: {
      equalTo: '.changePWD-form [name=newPassword]'
    }
  },
  currentPassword: null,
  newPassword: null,
  confirmPassword: null,

  actions: {
    changePassword() {
      const store = this.get('store');
      const currentPassword = this.get('currentPassword');
      const newPassword = this.get('newPassword');
      const isValid = this.$('.changePWD-form').valid();
      Ember.set(this, 'error', null);
      if (isValid) {
        store.adapterFor('user').changePassword(currentPassword, newPassword)
          .then(()=>{
            Ember.set(this, 'done', true);
          })
          .catch(error=>{
            Ember.set(this, 'error', error);
          });
      }
    }
  }
});
