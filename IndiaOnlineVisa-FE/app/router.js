import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('signin');
  this.route('forgot-password');
  this.route('how-to-apply');
  this.route('apply-visa', { path: 'apply-visa/step/:step' });
  this.route('rush-visa');
  this.route('article', { path: 'article/:article' });
  this.route('applying-for-india-visa', { path: 'article/applying-for-india-visa/:article' });
  this.route('embassies',{ path: 'embassies' });
  this.route('embassy',{ path: '/embassy/:article'});
  this.route('make-payment', { path: '/make-payment' });
  this.route('check-status', { path: '/status' },  function() {
    this.route('status-order', { path: '/order/detail/:order_id' });
  });
  this.route('status-order', { path: '/status/order/:order_id' });
  this.route('proceed-to-payment');
  this.route('check-visa-valid');
  this.route('check-requirements', { path: 'check-requirements' });
  this.route('check-requirement', { path: '/check-requirement/:article' });


  this.route('account', { path: '/account' }, function() {
    this.route('order', { path: '/order' });
    this.route('order-detail', { path: '/order/:order_id' });
    this.route('profile', { path: '/profile' });
    this.route('change-password', { path: '/change-password' });
    this.route('promotion-policy', { path: '/promotion-policy' });
    this.route('make-payment', { path: '/make-payment' });
    this.route('payment-detail', { path: 'payment/:paymentId' });
  });

  this.route('admin', { path: '/admin' }, function() {
    this.route('manage-user', { path: '/user' });
    this.route('manage-order', { path: '/order' });
    this.route('order-detail', { path: 'order/detail/:order_id' });
    this.route('government-fee', { path: '/government' });
    this.route('process-fee', { path: '/process' });
    this.route('social-link', { path: '/social' });
    this.route('content-page', { path: '/content' });
    this.route('email-notification', { path: '/email-notify' });
    this.route('payments', { path: '/payments' });
    this.route('payment-detail', { path: '/payments/:paymentId' });
     this.route('change-password', { path: '/change-password' });
     this.route('promotion-code', { path: '/promotion-code' });
  });

  this.route('auth_callback', { path: '/auth' }, function() {
    this.route('google', { path: '/google/callback' });
  });

  this.route('not-found', { path: '*:' });

});

export default Router;
