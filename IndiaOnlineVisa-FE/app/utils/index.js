export function appendCacheBusterParam(url, method = 'GET') {
  let newURL = url;
  if (newURL && method && method.toUpperCase() === 'GET') {
    newURL += `${newURL.indexOf('?') > 0 ? '&' : '?'}_=${Date.now()}`;
  }
  return newURL;
}

function crypt(string) {
  var len=string.length
  var intCarlu
  var carlu
  var newString="e"
  if ((string.charCodeAt(i)!=101)&&(len>0)) {
    for (var i=0; i<len; i++) {
      intCarlu=string.charCodeAt(i)
      const rnd=Math.floor(Math.random()*7)
      let newIntCarlu=30+10*rnd+intCarlu+i-48
      if (newIntCarlu<48) { newIntCarlu+=50 }
      if (newIntCarlu>=58 && newIntCarlu<=64) { newIntCarlu+=10 }
      if (newIntCarlu>=90 && newIntCarlu<=96) { newIntCarlu+=10 }
      carlu=String.fromCharCode(newIntCarlu)
      newString=newString.concat(carlu)
    }
    return newString
  } else {
    return string
  }
}
function decrypt(string) {
  var len=string.length
  var intCarlu
  var carlu
  var newString=""
  if (string.charCodeAt(i)==101) { 
    for (var i=1; i<len; i++) {
      intCarlu=string.charCodeAt(i)
      carlu=String.fromCharCode(48+(intCarlu-i+1)%10) 
      newString=newString.concat(carlu)
    }
    return newString
  } else {
    return string
  }
}

export const Base64 = {
  encode: crypt,
  decode: decrypt
};