import OAuth2PasswordGrant from 'ember-simple-auth/authenticators/oauth2-password-grant';
import Ember from 'ember';

const {inject}=Ember;

export default OAuth2PasswordGrant.extend({
  ajax: inject.service(),
  serverTokenEndpoint: `/api/v1/auth`,
  rejectWithResponse: true,
  makeRequest(url, data) {
  
    const ajax = this.get('ajax');
    const requestOptions = {  
      data: JSON.stringify(data),
      contentType: 'application/json',
      dataType: 'json'
    };
    return ajax.post(url, requestOptions);
  }
});
