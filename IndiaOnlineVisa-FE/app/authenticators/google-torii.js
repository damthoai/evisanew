import Ember from 'ember';
import Torii from 'ember-simple-auth/authenticators/torii';

const { service } = Ember.inject;

export default Torii.extend({
  torii: service('torii'),
  ajax: service(),
  
 authenticate() {
      const ajax = this.get('ajax');
  
      return this._super(...arguments).then((data) => {
        return ajax.request("/api/v1/auth/google/callback", {
          contentType: 'application/json',
          dataType: 'json',
          //data: JSON.stringify(data)
         data:     { 'grant_type': 'assertion', 'auth_code': data.authorizationCode }
        }).then((response) => {
          return {
            expireIn: response.expireIn,
            role: response.role,
            access_token: response.access_token,
            provider: data.provider,
            data: data
          };
        }).catch((error) => {
          //console.log(error);
        });
      });
    }
});