import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
  query() {
    return this.ajax('/master-list', 'GET');
  },
  updateRecord(store, type, snapshot) {
    const data = this.serialize(snapshot);
    const url = `/master-list/${snapshot.id}`;
    return this.ajax(url, 'PUT', { data });
  },
  getExchangeRate() {
    const url = `/master-list/exchange-rate`;
    return this.ajax(url, 'GET');
  }
});
