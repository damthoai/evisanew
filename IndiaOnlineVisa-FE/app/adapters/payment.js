import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
  createRecord(store, type, snapshot) {
    const order = this.serialize(snapshot);
    return this.ajax(`/payment`, 'POST', { data: order });
  },
  updateRecord(store, type, snapshot) {
    const payment = this.serialize(snapshot);
    return this.ajax(`/payment/${payment.paymentId}`, 'PUT', { data: payment });
  },

  findAll() {
    return this.ajax(`/payment`, 'GET');
  },

  findRecord(store, type, param) {
    return this.ajax(`/payment/${param}`, 'GET');
  },

  query(store, type, query) {
    const {page, limit} = query;
    return this.ajax(`/payment?limit=${limit}&page=${page}`, 'GET');
  }
});
