import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({

    createRecord( snapshot) {
       // const validvisa = this.serialize(snapshot);
        return this.ajax(`/valid_visa`, 'POST', { data: snapshot });
      } ,
    updateRecords( snapshot){
       // const validvisa = this.serialize(snapshot);
       snapshot.type = 'pickup-visa';
       snapshot.payment_box = '39';
        return this.ajax(`/valid_visa`, 'PUT', { data: snapshot });
    }

});
