import Ember from 'ember';
import RESTAdapter from 'ember-data/adapters/rest';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';
import { appendCacheBusterParam } from '../utils/index';
import { AjaxError } from 'ember-ajax/errors';

const { isNone } = Ember;

export default RESTAdapter.extend(DataAdapterMixin, {
  namespace: 'api/v1',
  authorizer: 'authorizer:application',
  ajax(url, type, hash) { // IE11 caching issue
    const ajaxType = isNone(type) ? 'GET' : type;
    const newURL = appendCacheBusterParam(url, ajaxType);
    return this._super(`/${this.namespace}/${_.trim(newURL, '/')}`, ajaxType, hash);
  },
  handleResponse(status, headers, payload) {
    const response = this._super(...arguments);
    if (!isNone(response) && response.isAdapterError === true) {
      return new AjaxError([{ code: payload.code , detail: payload.message }]|| response.errors || [response]);
    }
    return response;
  },
});