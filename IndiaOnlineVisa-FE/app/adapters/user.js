import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
  query(store, type, query) {
    const { page, limit } = query;
    return this.ajax(`/user?limit=${limit}&page=${page}`, 'GET');
  },
  createRecord(store, type, snapshot) {
    const data = this.serialize(snapshot);
    return this.ajax('/user', 'POST', { data });
  },
  updateRecord(store, type, snapshot) {
    const data = this.serialize(snapshot);
    const url = `/user/${snapshot.id}`;
    return this.ajax(url, 'PUT', { data });
  },
  // deleteRecord(store, type, snapshot) {
  //   const data = this.serialize(snapshot);
  //   const url = `/user/${data.id}/removeUser`;
  //   return this.ajax(url, 'PUT', { data });
  // },
  removeUser(snapshot) {
    const data = this.serialize(snapshot);
    const url = `/user/${snapshot.id}/removeUser`;
    return this.ajax(url, 'POST', { data });
  },
  activeUser(snapshot) {
    const data = this.serialize(snapshot);
    const url = `/user/${snapshot.id}/activeUser`;
    return this.ajax(url, 'POST', { data });
  },
  recoverPassword(email){
    const url = `/user/recoverPassword`;
    return this.ajax(url, 'PUT', { data: { email } });
  },
  login(email, password) {
    return this.ajax('/auth', 'POST', { data: { email, password } });
  },
  changePassword(currentPassword, newPassword) {
    return this.ajax('/user/changePWD', 'PUT', { data: { currentPassword, newPassword } });
  },
  userDetail(){
    return this.ajax('user/detail', 'GET');
  },
  save(user) {
    const url = `/user/${user.id}`;
    return this.ajax(url, 'PUT', { data: _.assign({}, user) });
  }
});
