import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
  findAll() {
    return this.ajax(`/content`, 'GET');
  },
  updateRecord(store, type, snapshot) {
    const data = this.serialize(snapshot);
    const url = `/content/${data.setId}`;
    return this.ajax(url, 'PUT', { data });
  }
});
