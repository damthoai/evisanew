import ApplicationAdapter from './application';
import { VISA_ACTIONS } from '../constants';

export default ApplicationAdapter.extend({
  createRecord(store, type, snapshot) {
    const order = this.serialize(snapshot);
    return this.ajax(`/order`, 'POST', { data: order });
  },
  updateRecord(store, type, snapshot) {
    const { action } = snapshot.adapterOptions || {};
    const order = this.serialize(snapshot);
    if (action === VISA_ACTIONS.PAYNOW) {
      return this.ajax('/order/paynow', 'PUT', { data: order });
    } else if (action === VISA_ACTIONS.CHANGETOFINISH || action === VISA_ACTIONS.CHANGETOPROCESSING) {
      return this.ajax('/order/order-status', 'PUT', { data: order });
    }
    return this.ajax('/order', 'PUT', { data: order });
  },

  findRecord(store, type, param) {
    return this.ajax(`/order/${param}`, 'GET');
  },

  query(store, type, query) {
    const { page, limit } = query;
    let { isAdmin } = query;
    if (Ember.isEmpty(isAdmin)) {
      isAdmin = "";
    }
    return this.ajax(`/order?limit=${limit}&page=${page}&isAdmin=${isAdmin}`, 'GET');
  },
  queryRecord(store, type, query) {
    const { orderId, userId } = query;
    if(userId){
      return this.ajax(`/order/${orderId}?userId=${userId}`, 'GET');
    }
    return this.ajax(`/order/${orderId}`, 'GET');
  },

  checkStatus(snapshot) {
    return this.ajax('/order/checkStatus', 'POST', { data: snapshot })
  }
});
