import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
  findAll() {
    return this.ajax(`/email`, 'GET');
  },

  query(store, type, query) {
    const {name, value} = query;
    return this.ajax(`/email/filter?name=${name}&value=${value}`, 'GET');
  }
});
