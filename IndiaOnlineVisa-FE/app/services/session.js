import ESASession from 'ember-simple-auth/services/session';
import { ROLES } from '../constants';

export default ESASession.extend({
  isAdmin: Ember.computed('isAuthenticated', function () {
    const isAuthenticated = Ember.get(this, 'isAuthenticated');
    const role = Ember.get(this, 'role');
    return isAuthenticated && role === ROLES.ADMIN;
  }),
  role: Ember.computed.alias('data.authenticated.role')
});
