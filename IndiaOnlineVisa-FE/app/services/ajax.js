import Ember from 'ember';
import AjaxService from 'ember-ajax/services/ajax';

export default AjaxService.extend({
  session: Ember.inject.service(),
  headers: Ember.computed('session.data.authenticated.access_token', {
    get() {
      const headers = {};
      const authToken = this.get('session.data.authenticated.access_token');
      if (authToken) {
        headers.Authorization = `Bearer ${authToken}`;
        headers['Content-Type'] = 'application/json';
      }
      return headers;
    }
  }),
});