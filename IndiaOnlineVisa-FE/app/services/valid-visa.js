import Ember from 'ember';

const { set } = Ember;

export default Ember.Service.extend({
  visaValid: null,
  createNewValidVisa(validVisa){
    set(this, 'visaValid', validVisa);
  }
});


