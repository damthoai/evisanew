import Ember from 'ember';
import { KEYS } from '../constants';

const { Service, computed, get } = Ember;

export default Service.extend({
  GENDER: computed('MasterData', function () {
    const masterData = this.get('MasterData') || [];
    return masterData.filter(item => get(item, 'enumKey') === KEYS.GENDER);
  }),
  TYPE_OF_CARDS: computed('MasterData', function () {
    const masterData = this.get('MasterData') || [];
    return masterData.filter(item => get(item, 'enumKey') === KEYS.TYPE_OF_CARDS);
  }),
  PROCESS_TYPES: computed('MasterData', function () {
    const masterData = this.get('MasterData') || [];
    return masterData.filter(item => get(item, 'enumKey') === KEYS.PROCESS_TYPES);
  }),
  ARRIVAL_PORT_TYPE: computed('MasterData', function () {
    const masterData = this.get('MasterData') || [];
    return masterData.filter(item => get(item, 'enumKey') === KEYS.ARRIVAL_PORT_TYPE);
  }),
  PORT_OF_AIRPORT: computed('MasterData', function () {
    const masterData = this.get('MasterData') || [];
    return masterData.filter(item => get(item, 'enumKey') === KEYS.PORT_OF_AIRPORT);
  }),
  PORT_OF_SEAPORT: computed('MasterData', function () {
    const masterData = this.get('MasterData') || [];
    return masterData.filter(item => get(item, 'enumKey') === KEYS.PORT_OF_SEAPORT);
  }),
  GOVERMENT_FEES: computed('MasterData', function () {
    const masterData = this.get('MasterData') || [];
    return masterData.filter(item => get(item, 'enumKey') === KEYS.GOVERNMENT_TYPE);
  }),
  GOVERMENT_FEES_MAP: computed('GOVERMENT_FEES.length', function() {
    const GOVERMENT_FEES = this.get('GOVERMENT_FEES');
    return new Map(_.map(GOVERMENT_FEES, item => [get(item, 'key'), item]));
  }),
  VISA_FEE: computed('MasterData', function() {
    const masterData = this.get('MasterData');
    return get(masterData.find(item => get(item, 'enumKey') === KEYS.VISA_TYPE) || {}, 'value'); 
  }),
  SOCIAL: computed('MasterData', function() {
    const masterData = this.get('MasterData');
    return masterData.filter(item => get(item, 'enumKey') === KEYS.SOCIAL);
  }),
  RATE: computed('ExchangeRate', function() {
    const ExchangeRate = this.get('ExchangeRate') || {};
    const rates = ExchangeRate.rates || {};
    return rates.AUD || 1;
  })
});