import Ember from 'ember';
import { PORT_TYPES, PROCESS_TYPE, STEP_STATUS } from '../constants';

const { isBlank, isEmpty, get, set, inject, isNone } = Ember;

function validateStep1(visaInfo) {
  const properties = [];
  if (isBlank(get(visaInfo, 'numberOfVisa')) || (+get(visaInfo, 'numberOfVisa') <= 0)) {
    properties.push('numberOfVisa');
  }
  if (isBlank(get(visaInfo, 'typeOfVisa'))) {
    properties.push('typeOfVisa');
  }
  if (isBlank(get(visaInfo, 'processType'))) {
    properties.push('processType');
  }
  if (isBlank(get(visaInfo, 'portOfArrival'))) {
    properties.push('portOfArrival');
  }
  if (isBlank(get(visaInfo, 'arrivalPortType'))) {
    properties.push('arrivalPortType');
  }
  
  return properties;
}

function validateStep2(visaInfo) {
  const properties = [];
  const applicants = get(visaInfo, 'applicants');
  if (isEmpty(applicants)) {
    properties.push('applicants');
  } else {
    _.each(applicants, (applicant, index) => {
      if (isBlank(get(applicant, 'surName'))) {
        properties.push(`surName${index+1}`);
      }
      if (isBlank(get(applicant, 'givenName'))) {
        properties.push(`givenName${index+1}`);
      }
      if (isBlank(get(applicant, 'gender'))) {
        properties.push(`gender${index+1}`);
      }
      if (isBlank(get(applicant, 'dateOfBirth'))) {
        properties.push(`dateOfBirth${index+1}`);
      }
      if (isBlank(get(applicant, 'nationality'))) {
        properties.push(`nationality${index+1}`);
      }
      if (isBlank(get(applicant, 'passportNumber'))) {
        properties.push(`passportNumber${index+1}`);
      }
    });
  }
  if (isBlank(get(visaInfo, 'dateOfArrival'))) {
    properties.push('dateOfArrival');
  }
  if (isBlank(get(visaInfo, 'dateOfDeparture'))) {
    properties.push('dateOfDeparture');
  }
  if (isBlank(get(visaInfo, 'contactName'))) {
    properties.push('contactName');
  }
  if (isBlank(get(visaInfo, 'contactEmail'))) {
    properties.push('contactEmail');
  }
  if (isBlank(get(visaInfo, 'contactPhone'))) {
    properties.push('contactPhone');
  }
  if (isBlank(get(visaInfo, 'contactAddress'))) {
    properties.push('contactAddress');
  }
  if (isBlank(get(visaInfo, 'contactCountry'))) {
    properties.push('contactCountry');
  }
  return properties;
}

function validateStep3(visaInfo) {
  const properties = [];
  if (isBlank(get(visaInfo, 'firstName'))) {
    properties.push('firstName');
  }
  if (isBlank(get(visaInfo, 'lastName'))) {
    properties.push('lastName');
  }
  if (isBlank(get(visaInfo, 'country'))) {
    properties.push('country');
  }
  if (isBlank(get(visaInfo, 'address'))) {
    properties.push('address');
  }
  if (isBlank(get(visaInfo, 'city'))) {
    properties.push('city');
  }
  if (isBlank(get(visaInfo, 'state'))) {
    properties.push('state');
  }
  if (isBlank(get(visaInfo, 'postCode'))) {
    properties.push('postCode');
  }
  if (isBlank(get(visaInfo, 'cardNumber'))) {
    properties.push('cardNumber');
  }
  if (isBlank(get(visaInfo, 'expiryDate'))) {
    properties.push('expiryDate');
  }
  if (isBlank(get(visaInfo, 'cvv2'))) {
    properties.push('cvv2');
  }
  
  return properties;
}

export default Ember.Service.extend({
  store: inject.service(),
  masterDataService: inject.service('master-data'),
  visaInfo: null,
  empty() {
    this.visaInfo = this.createNewVisa();
  },
  validate(step) {
    if (step === STEP_STATUS.INFO) {
      return validateStep1(this.visaInfo);
    } else if (step === STEP_STATUS.DETAIL_APPLICANT) {
      return validateStep2(this.visaInfo);
    } else if (step === STEP_STATUS.PAYMENT) {
      return validateStep3(this.visaInfo);
    }
    return false;
  },
  setCurrentVisa(visaInfo) {
    const oldVisa = get(this, 'visaInfo');
    if (oldVisa !== visaInfo){
      set(this, 'visaInfo', visaInfo);
      if (!isNone(oldVisa)){
        oldVisa.unloadRecord();
      }
    }
  },
  createNewVisa(numberOfVisa) {
    const { store, masterDataService } = this.getProperties('store', 'masterDataService');
    const TYPE_OF_CARDS = masterDataService.get('TYPE_OF_CARDS') || [];
    const ARRIVAL_PORT_TYPE = masterDataService.get('ARRIVAL_PORT_TYPE') || [];
    const PORT_OF_AIRPORT = masterDataService.get('PORT_OF_AIRPORT') || [];
    const PORT_OF_SEAPORT = masterDataService.get('PORT_OF_SEAPORT') || [];
    const port = _.first(ARRIVAL_PORT_TYPE) || {};
    const portOfArrival = get(port, 'key') === PORT_TYPES.Airport ? PORT_OF_AIRPORT : PORT_OF_SEAPORT;
    const PROCESS_TYPES = masterDataService.get('PROCESS_TYPES') || [];
    const processType = _.first(PROCESS_TYPES) || {};
    const rate = masterDataService.get('RATE');
    const oldVisa = get(this, 'visaInfo');
    set(this, 'visaInfo', store.createRecord('order', {
      numberOfVisa: numberOfVisa || 0,
      typeOfVisa: get(_.first(TYPE_OF_CARDS) || {}, 'key'),
      arrivalPortType: get(port, 'key'),
      portOfArrival: portOfArrival.get('firstObject.key'),
      processType: get(processType, 'key'),
      applicants: Ember.A([]),
      visaFee: masterDataService.get('VISA_FEE'),
      govermentVisaFee: 0,
      processFee: get(processType, 'value'),
      rate
    }));
    if (!isNone(oldVisa)){
      oldVisa.unloadRecord();
    }
  },
  createRushVisa() {
    this.createNewVisa(1);
    this.changeToSuperUrgent();
  },
  createApplicant() {
    return {
      id: get(this, 'visaInfo.applicants.length') + 1,
      surName: null,
      givenName: null,
      gender: '',
      dateOfBirth: null,
      nationality: null,
      passportNumber: null,
      relious: '',
      education: '',
      fatherName: null,
      fatherNationality: null,
      motherName: null,
      motherNationality: null
    };
  },
  changeToSuperUrgent(){
    const { masterDataService } = this.getProperties('masterDataService');
    const PROCESS_TYPES = masterDataService.get('PROCESS_TYPES') || [];
    const processType = _.find(PROCESS_TYPES, item => Ember.get(item, 'key') === PROCESS_TYPE.SUPER_URGENT);
    set(this, 'visaInfo.processType', Ember.get(processType, 'key'));
    set(this, 'visaInfo.processFee', Ember.get(processType, 'value'));
  },
  changeToUrgent(){
    const { masterDataService } = this.getProperties('masterDataService');
    const PROCESS_TYPES = masterDataService.get('PROCESS_TYPES') || [];
    const processType = _.find(PROCESS_TYPES, item => Ember.get(item, 'key') === PROCESS_TYPE.URGENT);
    set(this, 'visaInfo.processType', Ember.get(processType, 'key'));
    set(this, 'visaInfo.processFee', Ember.get(processType, 'value'));
  },
  changeToNormal(){
    set(this, 'visaInfo.processType', PROCESS_TYPE.NORMAL);
    set(this, 'visaInfo.processFee', 0);
  }
});
