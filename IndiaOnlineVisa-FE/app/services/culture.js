import Ember from 'ember';
import { GlobalDateFormat } from '../constants';

const { Service } = Ember;

export default Service.extend({
  parseDate(date, format) {
    return $.fn.datepicker.DPGlobal.parseDate(date, format || GlobalDateFormat, $.fn.datepicker.defaults.language, $.fn.datepicker.defaults.assumeNearbyYear);
  },
  formatDate(date, format) {
    return $.fn.datepicker.DPGlobal.formatDate(date, format || $.fn.datepicker.defaults.format, $.fn.datepicker.defaults.language);
  }
});

