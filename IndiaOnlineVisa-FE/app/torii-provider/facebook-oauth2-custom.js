import Oauth2 from 'torii/providers/facebook-oauth2';

export default Oauth2.extend({
  responseParams: ['code', 'state', 'expireIn', 'access_token', 'role'],
  open: function (options) {
    var name = this.get('name'),
      url = this.buildUrl(),
      redirectUri = this.get('redirectUri'),
      responseParams = this.get('responseParams'),
      state = this.get('state'),
      shouldCheckState = responseParams.indexOf('state') !== -1;

    return this.get('popup').open(url, responseParams, options).then(function (authData) {
      var missingResponseParams = [];

      responseParams.forEach(function (param) {
        if (authData[param] === undefined) {
          missingResponseParams.push(param);
        }
      });

      if (missingResponseParams.length) {
        throw new Error("The response from the provider is missing " +
          "these required response params: " + missingResponseParams.join(', '));
      }

      if (shouldCheckState && authData.state !== state) {
        throw new Error('The response from the provider has an incorrect ' +
          'session state param: should be "' + state + '", ' +
          'but is "' + authData.state + '"');
      }

      return {
        access_token: authData.access_token,
        expireIn: authData.expireIn,
        role: authData.role,
        authorizationCode: authData.code,
        provider: name,
        redirectUri: redirectUri
      };
    });
  }
});