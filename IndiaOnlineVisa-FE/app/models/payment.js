import Model from 'ember-data/model';
import attr from 'ember-data/attr';

export default Model.extend({
  paymentStatus: attr('string'),
  createdDate: attr('customdate'),
  paymentId: attr('number'),
  amount: attr('number'),
  rate: attr('number'),
  paymentFor: attr('string'),
  note: attr('string'),
  contactEmail: attr('string'),
  // billing address
  firstName: attr('string'),
  lastName: attr('string'),
  country: attr('string'),
  address: attr('string'),
  city: attr('string'),
  state: attr('string'),
  postCode: attr('string'),
  // card info
  cardNumber: attr('string'),
  paymentType: attr('string'),
  expiryDate: attr('string'),
  cvv2: attr('string'),
  paymentErrors: attr()
})