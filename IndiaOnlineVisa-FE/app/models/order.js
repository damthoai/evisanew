import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { PORT_TYPES } from '../constants';

const { computed, get, set, isNone, inject } = Ember;

export default Model.extend({
  masterDataService: inject.service('master-data'),

  orderId: attr('number'),
  orderDate: attr('customdate'),
  lastUpdateDate: attr('customdate'),
  orderStatus: attr('string'),

  numverOfTravelInYear: attr('string'),
  question2: attr('string', { defaultValue: 'No' }),
  question3: attr('string', { defaultValue: 'No' }),
  question4: attr('string', { defaultValue: 'No' }),
  question5: attr('string', { defaultValue: 'No' }),
  numberOfVisa: attr('number'),
  typeOfVisa: attr('string'),
  arrivalPortType: attr('string'),
  portOfArrival: attr('string'),
  processType: attr('string'),

  applicants: attr('applicant'),
  dateOfArrival: attr('customdate'),
  dateOfDeparture: attr('customdate'),

  flightNumber: attr('string'),
  arrivalTime: attr('string'),
  contactName: attr('string'),
  contactEmail: attr('string'),
  contactPhone: attr('string'),
  contactAddress: attr('string'),
  contactCountry: attr('string'),
  contactNote: attr('string'),
  // billing address
  firstName: attr('string'),
  lastName: attr('string'),
  country: attr('string'),
  address: attr('string'),
  city: attr('string'),
  state: attr('string'),
  postCode: attr('string'),
  // card info
  cardNumber: attr('string'),
  paymentType: attr('string'),
  expiryDate: attr('string'),
  cvv2: attr('string'),
  paymentErrors: attr(),
  visaFee: attr('number'),
  processFee: attr('number'),
  govermentVisaFee: attr('number'),
  rate: attr('number'),
  totalVisaFee: computed('visaFee', 'numberOfVisa', function(){
    const { visaFee, numberOfVisa } = this.getProperties('visaFee', 'numberOfVisa');
    return visaFee * (+numberOfVisa < 1 ? 1 : numberOfVisa);
  }),
  totalFees: computed('totalVisaFee', 'processFee', 'govermentVisaFee', function() {
    const { totalVisaFee, processFee, govermentVisaFee } = this.getProperties('totalVisaFee', 'processFee', 'govermentVisaFee');
    return totalVisaFee + processFee + govermentVisaFee;
  }),
  numberOfVisaText: computed('numberOfVisa', function() {
    const numberOfVisa = this.get('numberOfVisa');
    if (numberOfVisa < 1) return '1 Applicant';
    if (numberOfVisa < 2) return `${numberOfVisa} Applicant`;
    return `${numberOfVisa} Applicants`;
  }),
  TYPE_OF_CARDS: computed.alias('masterDataService.TYPE_OF_CARDS'),
  typeOfVisaText: computed('typeOfVisa', function() {
    const typeOfVisa = this.get('typeOfVisa');
    const TYPE_OF_CARDS = this.get('TYPE_OF_CARDS');
    const type = TYPE_OF_CARDS.find(item => get(item, 'key') === typeOfVisa) || {};
    return get(type, 'detail');
  }),
  ARRIVAL_PORT_TYPE: computed.alias('masterDataService.ARRIVAL_PORT_TYPE'),
  arrivalPortTypeText: computed('arrivalPortType', function() {
    const arrivalPortType = this.get('arrivalPortType');
    const ARRIVAL_PORT_TYPE = this.get('ARRIVAL_PORT_TYPE');
    return ARRIVAL_PORT_TYPE.find(item => get(item, 'key') === arrivalPortType).get('detail');
  }),
  PORT_OF_AIRPORT: computed.alias('masterDataService.PORT_OF_AIRPORT'),
  PORT_OF_SEAPORT: computed.alias('masterDataService.PORT_OF_SEAPORT'),
  portOfArrivalText: computed('portOfArrival', 'arrivalPortType', function() {
    const portOfArrival = this.get('portOfArrival');
    const arrivalPortType = this.get('arrivalPortType');
    let portOfArrivals = this.get('PORT_OF_AIRPORT');
    if (PORT_TYPES.Seaport === arrivalPortType) portOfArrivals = this.get('PORT_OF_SEAPORT');
    let item = portOfArrivals.find($item => get($item, 'key') === portOfArrival);
    if (isNone(item)) {
      item = portOfArrivals.get('firstObject');
    }
    return get(item, 'detail');
  }),
  PROCESS_TYPES: computed.alias('masterDataService.PROCESS_TYPES'),
  processingTimeText: computed('processType', function() {
    const processType = this.get('processType');
    const PROCESS_TYPES = this.get('PROCESS_TYPES');
    return PROCESS_TYPES.find(item => get(item, 'key') === processType).get('detail');
  }),
  totalFee: computed('totalVisaFee', 'processFee', 'govermentVisaFee', function() {
    const { totalVisaFee, processFee, govermentVisaFee } = this.getProperties('totalVisaFee', 'processFee', 'govermentVisaFee');
    return totalVisaFee + processFee + govermentVisaFee;
  }),
  totalFeeAUD: computed('totalFee', function(){
    const totalFee = this.get('totalFee');
    const rate = this.get('rate');
    return Math.floor(totalFee * rate);
  }),
  totalFeeAUDText: computed('totalFeeAUD', function(){
    const totalFeeAUD = this.get('totalFeeAUD');
    return `(AUD)${totalFeeAUD}`;
  }),
  totalFeeText: computed('totalFee', function() {
    return '$'+this.get('totalFee');
  }),
  applyDate: computed('orderDate', function() {
    return moment(this.get('orderDate')).format('LLL');
  }),
  GOVERMENT_FEES_MAP: computed.alias('masterDataService.GOVERMENT_FEES_MAP'),
  applicantFees: computed('applicants.@each.nationality', function() {
    const applicants = this.get('applicants') || [];
    const GOVERMENT_FEES_MAP = this.get('GOVERMENT_FEES_MAP');
    const fees = _.map(applicants, applicant => GOVERMENT_FEES_MAP.get(get(applicant, 'nationality'))).filter(applicant => !isNone(applicant)).map((item, index) => ({ ...item.toJSON(), index: index + 1 }));
    set(this, 'govermentVisaFee', _.sumBy(fees, 'value'));
    return fees;
  })
});
