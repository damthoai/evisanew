import Model from 'ember-data/model';
import attr from 'ember-data/attr';

export default Model.extend({
  surName: attr('string'),
  givenName: attr('string'),
  dateOfBirth: attr('date'),
  gender: attr('string'),
  nationality: attr('string'),
  passportNumber: attr('string')
})
