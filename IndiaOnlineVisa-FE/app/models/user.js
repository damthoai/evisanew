import Model from 'ember-data/model';
import attr from 'ember-data/attr';

export default Model.extend({
  email: attr('string'),
  username: attr('string'),
  password: attr('string'),
  fullname: attr('string'),
  phone: attr('string'),
  country: attr('string'),
  address: attr('string'),
  isActived: attr('string'),
  role: attr('string')
});
