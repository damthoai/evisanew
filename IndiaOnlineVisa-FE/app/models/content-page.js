import Model from 'ember-data/model';
import attr from 'ember-data/attr';

export default Model.extend({
  path: attr('string'),
  title: attr('string'),
  enumKey: attr('string')
})
