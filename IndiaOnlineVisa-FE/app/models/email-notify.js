import Model from 'ember-data/model';
import attr from 'ember-data/attr';

export default Model.extend({
  type: attr('string'),
  userEmail: attr('string'),
  dateSend: attr('string'),
  orderId: attr('string'),
  html: attr('string'),
  raw: attr('string'),
  dateSendText: Ember.computed('dateSend', function() {
    return moment(this.get('dateSend')).format('LLL');
  })
});
