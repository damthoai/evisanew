import Ember from 'ember';

export default Ember.Helper.helper(params => _.reduce(params, (flag, param) => flag || param, false));
