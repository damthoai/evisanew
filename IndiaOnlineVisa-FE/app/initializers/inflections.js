import * as CONSTANTS from '../constants';
import CustomGoogleOAuth2Provider from '../torii-provider/google-oauth2-custom';
import CustomFacebookOAuth2Provider from '../torii-provider/facebook-oauth2-custom';

export function initialize(application) {
  // CONSTANTS
  application.register('indiaonlinevisa:constants', CONSTANTS, { instantiate: false, singleton: true });
  ['component', 'controller', 'route'].forEach(one => application.inject(one, 'CONSTANTS', 'indiaonlinevisa:constants'));

  application.register('torii-provider:google-oauth2-custom', CustomGoogleOAuth2Provider);
  application.register('torii-provider:facebook-oauth2-custom', CustomFacebookOAuth2Provider);
}

export default {
  name: 'inflections',
  initialize
};
