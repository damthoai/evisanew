import Ember from 'ember';
import { STEP_STATUS } from '../constants';

const { Route, inject } = Ember;

export default Route.extend({
  applyVisaService: inject.service('apply-visa'),
  model() {
    const applyVisaService = this.get('applyVisaService');
    applyVisaService.createRushVisa();
    return STEP_STATUS.INFO;
  }
});
