import Ember from 'ember';

export default Ember.Route.extend({
  routing: Ember.inject.service('-routing'),
  actions: {
    checkEmbassy() {
      const route = Ember.$( "select#cbCountry" ).val();
      const routing = this.get('routing');
       
      routing.router.transitionTo('embassy', route);
    }
  }
});
