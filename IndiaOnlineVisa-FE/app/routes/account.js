import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { ROLES } from '../constants';
const { inject } = Ember;
export default Ember.Route.extend(AuthenticatedRouteMixin, {
  store: inject.service(),
  session: inject.service(),
  beforeModel(transition) {
    const session = this.get('session');
    const isAuthenticated = Ember.get(session, 'isAuthenticated');
    if (isAuthenticated) {
      const role = Ember.get(session, 'role');
      if (role === ROLES.ADMIN) {
        return this.transitionTo('admin.manage-order');
      }
      if (role !== ROLES.MEMBER) {
        return session.invalidate();
      }
      if (transition.targetName === 'account.index') {
        return this.transitionTo('account.order');
      }
    } else {
      return this.transitionTo('signin');
    }
  }
});
