import Ember from 'ember';
const { inject } = Ember;
export default Ember.Route.extend({
  store: inject.service(),
  model(params) {
    return this.get('store').findRecord('payment', params.paymentId, { backgroundReload: false });
  }
});
