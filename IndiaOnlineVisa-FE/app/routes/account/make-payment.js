import Ember from 'ember';

export default Ember.Route.extend({
  store: Ember.inject.service(),
  limit: 10,
  page: 0,
  queryParams: {
    limit: {
      refreshModel: true
    },
    page: {
      refreshModel: true
    }
  },
  model(){
    return this.get('store').query('payment', this.getProperties(['limit', 'page']))
  }
});