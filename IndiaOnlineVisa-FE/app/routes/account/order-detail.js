import Ember from 'ember';
const { inject } = Ember;
export default Ember.Route.extend({
  store: inject.service(),
  model(params) {
    return this.get('store').findRecord('order', params.order_id, { backgroundReload: false });
  },
  actions: {
    willTransition: function(transition) {
      const targetRoute = transition.targetName;
      if (targetRoute !== 'apply-visa') {
        const model = this.get('controller.model');
        model.unloadRecord();
      }
    }
  }
});
