import Ember from 'ember';
import * as UiServiceMixin from 'torii/mixins/ui-service-mixin';



export default Ember.Route.extend({
  ajax: Ember.inject.service(),
  
  model({ code, access_token, expireIn }) {
    const CURRENT_REQUEST_KEY = UiServiceMixin.CURRENT_REQUEST_KEY;
    const pendingRequestKey = window.localStorage.getItem(CURRENT_REQUEST_KEY);

    if (pendingRequestKey) {
      window.localStorage.removeItem(CURRENT_REQUEST_KEY);
      window.localStorage.setItem(pendingRequestKey, JSON.stringify({ code, access_token, expireIn }));
    }
    window.close();
  }
});
