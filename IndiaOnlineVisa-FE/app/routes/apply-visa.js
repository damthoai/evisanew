import Ember from 'ember';
import { STEP_STATUS, ORDER_STATUS } from '../constants';

const { Route, inject, isEmpty, RSVP } = Ember;
function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

export default Route.extend({
  applyVisaService: inject.service('apply-visa'),
  store: inject.service(),
  model(params) {
    const { step } = params;
    return new RSVP.Promise((resolve) => {
      const token = getParameterByName('token', window.location.search);
      if (!isEmpty(token)) {
        const applyVisaService = this.get('applyVisaService');
        const store = this.get('store');
        let paymentToken = window.atob(token);
        if (!Ember.isEmpty(paymentToken)) {
          try {
            paymentToken = JSON.parse(paymentToken);
            if (paymentToken.orderId){
              store.queryRecord('order', paymentToken).then(order => {
                applyVisaService.setCurrentVisa(order);
                resolve(+(step || 1));
              }).catch(error => resolve({error}));
            } else {
              resolve({error: { errors: [{message: 'Order does not exist'}]}});
            }
          } catch (error) {
            resolve({error: { errors: [{message: 'Order does not exist'}]}});
          }
        }
      } else {
        resolve(+(step || 1));
      }
    });
  },
  afterModel(step) {
    const applyVisaService = this.get('applyVisaService');
    if (step < STEP_STATUS.INFO || step > STEP_STATUS.THANKYOU) {
      this.transitionTo('apply-visa', STEP_STATUS.INFO);
    } else {
      if (step === STEP_STATUS.DETAIL_APPLICANT) {
        const properties = applyVisaService.validate(STEP_STATUS.INFO);
        if (!isEmpty(properties)) {
          this.transitionTo('apply-visa', STEP_STATUS.INFO);
        }
      } else if (step === STEP_STATUS.PAYMENT) {
        const properties = applyVisaService.validate(STEP_STATUS.DETAIL_APPLICANT);
        if (!isEmpty(properties)) {
          this.transitionTo('apply-visa', STEP_STATUS.DETAIL_APPLICANT);
        }
      } else if (step === STEP_STATUS.THANKYOU) {
        const visaInfo = Ember.get(applyVisaService, 'visaInfo');
        const orderStatus = Ember.get(visaInfo, 'orderStatus');
        if (orderStatus !== ORDER_STATUS.CANCELED_PAYMENT && orderStatus !== ORDER_STATUS.PAID) {
          this.transitionTo('apply-visa', STEP_STATUS.INFO);
        }
      }
    }
  },
  actions: {
    willTransition(transition) {
      const targetRoute = transition.targetName;
      if (targetRoute !== 'apply-visa') {
        const applyVisaService = this.get('applyVisaService');
        applyVisaService.createNewVisa();
      }
    }
  }
});
