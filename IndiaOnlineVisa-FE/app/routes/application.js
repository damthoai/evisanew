import Ember from 'ember';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';

const { inject, set, RSVP } = Ember;

export default Ember.Route.extend(ApplicationRouteMixin, {
  store: inject.service(),
  masterData: inject.service('master-data'),
  applyVisaService: inject.service('apply-visa'),
  model() {
    $.fn.datepicker.defaults.format = 'dd/mm/yyyy';
    const { store, masterData, applyVisaService } = this.getProperties('store', 'masterData', 'applyVisaService');
    return RSVP.hash({
      masterData: store.query('master-list', {}),
      exchangeRate: store.adapterFor('master-list').getExchangeRate()
    }).then(data => {
      set(masterData, 'MasterData', data.masterData);
      set(masterData, 'ExchangeRate', data.exchangeRate);
      applyVisaService.createNewVisa();
      return data;
    });
  }
});
