import Ember from 'ember';

export default Ember.Route.extend({
  routing: Ember.inject.service('-routing'),
  actions: {
    checkRequirement() {
      const route = Ember.$( "select#cbCountry" ).val();
      const routing = this.get('routing');
       
      routing.router.transitionTo('check-requirement', route);
    }
  }
});
