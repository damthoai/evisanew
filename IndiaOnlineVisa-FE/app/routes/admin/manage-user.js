import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin,{
  store: Ember.inject.service(),
  limit: 10,
  page: 0,
  queryParams: {
    limit: {
      refreshModel: true
    },
    page: {
      refreshModel: true
    }
  },
  model(){
    return this.get('store').query('user', this.getProperties(['limit', 'page']))
  }
});




