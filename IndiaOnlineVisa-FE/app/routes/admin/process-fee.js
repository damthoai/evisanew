import Ember from 'ember';
import RSVP from 'rsvp';

export default Ember.Route.extend({
  masterData: Ember.inject.service("master-data"),
    model() {
    const columns = [{
      width: '40px',
      sortable: false,
      cellComponent: 'order-list/row-toggle',
      breakpoints: ['mobile', 'tablet']
    }, {
      label: 'No.',
      valuePath: 'key',
      width: '200px'
    }, {
      label: 'Detail',
      valuePath: 'detail',
      width: '200px',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }, {
      label: 'Price',
      valuePath: 'value',
      width: '150px',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }];
    return RSVP.hash({
      data: this.get('masterData.PROCESS_TYPES'),
      columns
    });
  },
});
