import Ember from 'ember';
import RSVP from 'rsvp';

export default Ember.Route.extend({
  masterData: Ember.inject.service("master-data"),
    model() {
    const columns = [{
      width: '40px',
      sortable: false,
      cellComponent: 'order-list/row-toggle',
      breakpoints: ['mobile', 'tablet']
    }, {
      label: 'Name',
      valuePath: 'key',
      width: '200px'
    }, {
      label: 'Link',
      valuePath: 'detail',
      width: '300px',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }];
    return RSVP.hash({
      data: this.get('masterData.SOCIAL'),
      columns
    });
  },
});
