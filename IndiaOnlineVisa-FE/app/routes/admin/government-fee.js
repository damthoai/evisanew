import Ember from 'ember';
import RSVP from 'rsvp';

export default Ember.Route.extend({
  masterData: Ember.inject.service("master-data"),
  model() {
    const columns = [{
      width: '40px',
      sortable: false,
      cellComponent: 'order-list/row-toggle',
      breakpoints: ['mobile', 'tablet']
    }, {
      label: 'Code',
      valuePath: 'key',
      width: '200px'
    }, {
      label: 'Country',
      valuePath: 'detail',
      width: '200px',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }, {
      label: 'Price',
      valuePath: 'value',
      width: '150px',
      breakpoints: ['tablet', 'desktop', 'jumbo']
    }];
    return RSVP.hash({
      data: this.get('masterData.GOVERMENT_FEES'),
      columns
    });
  },
});
