import Ember from 'ember';

export default Ember.Route.extend({
  store: Ember.inject.service(),
  isAdmin: true,
  limit: 10,
  page: 0,
  queryParams: {
    limit: {
      refreshModel: true
    },
    page: {
      refreshModel: true
    }
  },
  model() {
    return this.get('store').query('order', this.getProperties(['limit', 'page','isAdmin']))
  }
});
