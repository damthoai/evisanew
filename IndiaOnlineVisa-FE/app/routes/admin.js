import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { ROLES } from '../constants';
const { inject } = Ember;
export default Ember.Route.extend(AuthenticatedRouteMixin, {
  store: inject.service(),
  session: inject.service(),
  beforeModel() {
    const session = this.get('session');
    const isAuthenticated = session.get('isAuthenticated');
    if (isAuthenticated) {
      const role = session.get('role')
      if (role === ROLES.MEMBER) {
        return this.transitionTo('account');
      } else if (role !== ROLES.ADMIN){
        return session.invalidate();
      }
    } else {
      return this.transitionTo('signin');
    }
  }
});
