import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
    const { article } = params;
    return article;
  }
});
