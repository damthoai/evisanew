import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { ROLES } from '../constants';
const { isEmpty, inject } = Ember;
export default Ember.Route.extend(AuthenticatedRouteMixin, {
  store: inject.service(),
  session: inject.service(),
  beforeModel() {
    const isAuthenticated = this.get('session.isAuthenticated');
    const role = this.get('session.data.authenticated.role')
    if (isAuthenticated && !isEmpty(role)) {
      if (role === ROLES.ADMIN) {
        return this.transitionTo('admin.manage-order');
      }
      if (role === ROLES.MEMBER) {
        return this.transitionTo('account');
      }
    }
    return this.transitionTo('signin');
  },

});
