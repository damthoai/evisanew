/* eslint-env node */
const EmberApp = require('ember-cli/lib/broccoli/ember-app');
const mergeTrees = require('broccoli-merge-trees');
const pickFiles = require('broccoli-static-compiler');

const fontawesome = pickFiles('bower_components/font-awesome/fonts', {
  srcDir: '/',
  destDir: '/fonts'
});
const fontboostrap = pickFiles('bower_components/bootstrap-sass/assets/fonts/bootstrap', {
  srcDir: '/',
  destDir: '/fonts'
});

module.exports = function(defaults) {
  var app = new EmberApp(defaults, {
    'ember-cli-bootstrap-sassy': {
      'js': ['dropdown', 'modal', 'collapse', 'transition', 'tab', 'tooltip', 'alert']
    },
    storeConfigInMeta: false,
    SRI: {
      enabled: false
    }
  });
  // lodash
  app.import('bower_components/lodash/dist/lodash.js');
  // bootstrap-datepicker
  app.import('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.standalone.css');
  app.import('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js');
  // jquery-validation
  app.import('bower_components/jquery-validation/dist/jquery.validate.js');
  // moment
  app.import('bower_components/moment/min/moment.min.js'); 

  const merged = mergeTrees([app.toTree(), fontawesome, fontboostrap], {
    overwrite: true
  });

  return app.toTree(merged);
};
