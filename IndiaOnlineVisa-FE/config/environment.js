/* eslint-env node */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'india-online-visa-fe',
    environment: environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    },
    torii: {
      allowUnsafeRedirect: true,
      sessionServiceName: 'session',
      providers: {
        'google-oauth2': {
          apiKey: '880133518781-bdlm6tkojes34t1s17q1t6fv01hitdu7.apps.googleusercontent.com',
          redirectUri: 'http://localhost:4200/api/v1/auth/google/callback'
        },
        'facebook-oauth2': {
          apiKey: '468858830115945',
          redirectUri: 'http://localhost:4200/api/v1/auth/facebook/callback'
        }
      }
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {
    const config = require('./production.json');
    ENV = Object.assign(ENV, config);
  }

  return ENV;
};
